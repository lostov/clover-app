<?php
date_default_timezone_set('Asia/Tashkent');
/**
 * Точка входа
 */

require __DIR__.'/../bootstrap/autoload.php';

$app = require __DIR__.'/../bootstrap/app.php';

$app->run();