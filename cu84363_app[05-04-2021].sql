-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 04 2021 г., 13:51
-- Версия сервера: 8.0.19
-- Версия PHP: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cu84363_app`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int NOT NULL,
  `title` text NOT NULL,
  `text` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category` int NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `text`, `user`, `date`, `category`, `status`) VALUES
(1, 'Some title 1', 'Lorem1 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:41', 1, 1),
(2, 'Some title 2', 'Lorem2 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:47', 1, 1),
(3, 'Some title 3', 'Lorem3 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:51', 1, 1),
(4, 'Some title 4', 'Lorem4 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:54', 1, 1),
(5, 'Some title 5', 'Lorem5 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:57', 1, 1),
(6, 'Some title 6', 'Lorem6 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:59', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int NOT NULL,
  `title` text NOT NULL,
  `parent` int NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `configs`
--

CREATE TABLE `configs` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = default.null\r\n1 = string\r\n2 = int\r\n3 = bool\r\n4 = json.array\r\n5 = json.object',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sort` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `configs`
--

INSERT INTO `configs` (`id`, `user`, `name`, `value`, `date`, `type`, `title`, `sort`) VALUES
(1, 1, 'slides.count', '5', '2020-12-06 01:06:46', 2, 'How Many Slides for a use on home page', 1),
(2, 1, 'slides.total', '10', '2020-12-06 01:47:58', 2, 'Totaly', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `items`
--

CREATE TABLE `items` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `type` int NOT NULL DEFAULT '1',
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `icon` text,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `items`
--

INSERT INTO `items` (`id`, `user`, `type`, `code`, `title`, `icon`, `text`) VALUES
(1, 1, 1, 'listening', 'Listening', 'some-icon', 'Extend your listening ability with question based exercises and b...'),
(2, 1, 1, 'reading', 'Reading', 'some-icon', 'Upgrade your reading skills with customized passages ...'),
(3, 1, 1, 'writing', 'Writing', 'some-icon', 'Become an excellent writer via master class sessions in-a...'),
(4, 1, 1, 'speaking', 'Speaking', 'some-icon', 'Transform your speaking skills using our \"Record\" feature and r...'),
(5, 1, 1, 'vocabulary', 'Vocabulary', 'some-icon', 'Turn into a vocabulary guru by just playing quizzes');

-- --------------------------------------------------------

--
-- Структура таблицы `listening`
--

CREATE TABLE `listening` (
  `id` int NOT NULL,
  `day` tinyint NOT NULL,
  `tips` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `listening`
--

INSERT INTO `listening` (`id`, `day`, `tips`, `title`, `text`, `user`, `date`) VALUES
(1, 1, 1, 'Lorem ipsum practice first', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-07 02:20:11'),
(2, 2, 1, 'Lorem ipsum practice second', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-07 02:20:28');

-- --------------------------------------------------------

--
-- Структура таблицы `listening_weeks`
--

CREATE TABLE `listening_weeks` (
  `id` int NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` int DEFAULT NULL,
  `text` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `listening_weeks`
--

INSERT INTO `listening_weeks` (`id`, `title`, `icon`, `text`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 'Week 1', NULL, 'Some text', '2021-04-04 13:32:59', 1, NULL, NULL, 1),
(2, 'Week 2', NULL, 'Some text', '2021-04-04 13:33:06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `logs`
--

CREATE TABLE `logs` (
  `id` int NOT NULL,
  `type` int NOT NULL,
  `action` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `logs`
--

INSERT INTO `logs` (`id`, `type`, `action`, `user`, `date`) VALUES
(1, 1, 'home.show', 1, '2020-12-03 11:11:38'),
(2, 1, 'auth.logout', 1, '2020-12-03 11:12:07'),
(3, 1, 'home.show', 1, '2020-12-03 11:12:28'),
(4, 1, 'auth.logout', 1, '2020-12-03 11:14:51'),
(5, 1, 'auth.login', 1, '2020-12-03 11:15:10'),
(6, 1, 'home.show', 1, '2020-12-03 11:15:10'),
(7, 1, 'home.show', 1, '2020-12-04 03:48:02'),
(8, 1, 'home.show', 1, '2020-12-04 03:48:19'),
(9, 1, 'home.show', 1, '2020-12-04 03:48:32'),
(10, 1, 'home.show', 1, '2020-12-04 03:51:13'),
(11, 1, 'home.show', 1, '2020-12-04 03:52:19'),
(12, 1, 'home.show', 1, '2020-12-04 03:53:17'),
(13, 1, 'home.show', 1, '2020-12-04 03:54:11'),
(14, 1, 'home.show', 1, '2020-12-04 03:56:41'),
(15, 1, 'home.show', 1, '2020-12-04 03:56:52'),
(16, 1, 'home.show', 1, '2020-12-04 03:57:31'),
(17, 1, 'home.show', 1, '2020-12-04 03:58:01'),
(18, 1, 'home.show', 1, '2020-12-04 03:58:08'),
(19, 1, 'home.show', 1, '2020-12-04 03:59:34'),
(20, 1, 'home.show', 1, '2020-12-04 04:00:18'),
(21, 1, 'home.show', 1, '2020-12-04 04:00:40'),
(22, 1, 'home.show', 1, '2020-12-04 04:01:52'),
(23, 1, 'home.show', 1, '2020-12-04 04:03:15'),
(24, 1, 'home.show', 1, '2020-12-04 04:03:32'),
(25, 1, 'home.show', 1, '2020-12-04 04:04:46'),
(26, 1, 'home.show', 1, '2020-12-04 04:05:03'),
(27, 1, 'home.show', 1, '2020-12-04 04:05:09'),
(28, 1, 'home.show', 1, '2020-12-04 04:06:36'),
(29, 1, 'home.show', 1, '2020-12-04 04:09:26'),
(30, 1, 'home.show', 1, '2020-12-04 04:09:37'),
(31, 1, 'home.show', 1, '2020-12-04 04:09:53'),
(32, 1, 'home.show', 1, '2020-12-04 04:10:27'),
(33, 1, 'home.show', 1, '2020-12-04 04:10:39'),
(34, 1, 'home.show', 1, '2020-12-04 04:11:07'),
(35, 1, 'home.show', 1, '2020-12-04 04:13:37'),
(36, 1, 'home.show', 1, '2020-12-04 04:13:53'),
(37, 1, 'home.show', 1, '2020-12-04 04:14:03'),
(38, 1, 'home.show', 1, '2020-12-04 04:14:49'),
(39, 1, 'home.show', 1, '2020-12-04 04:15:15'),
(40, 1, 'home.show', 1, '2020-12-04 04:17:12'),
(41, 1, 'home.show', 1, '2020-12-04 04:17:53'),
(42, 1, 'home.show', 1, '2020-12-04 04:18:21'),
(43, 1, 'home.show', 1, '2020-12-04 04:19:02'),
(44, 1, 'home.show', 1, '2020-12-04 04:19:15'),
(45, 1, 'home.show', 1, '2020-12-04 04:19:21'),
(46, 1, 'home.show', 1, '2020-12-04 04:19:51'),
(47, 1, 'home.show', 1, '2020-12-04 04:20:45'),
(48, 1, 'home.show', 1, '2020-12-04 04:21:57'),
(49, 1, 'home.show', 1, '2020-12-04 04:22:04'),
(50, 1, 'home.show', 1, '2020-12-04 04:22:13'),
(51, 1, 'home.exportword', 1, '2020-12-04 04:25:25'),
(52, 1, 'home.exportword', 1, '2020-12-04 04:25:28'),
(53, 1, 'home.exportword', 1, '2020-12-04 04:25:44'),
(54, 1, 'home.exportword', 1, '2020-12-04 04:26:34'),
(55, 1, 'home.exportword', 1, '2020-12-04 04:26:59'),
(56, 1, 'home.exportword', 1, '2020-12-04 04:31:12'),
(57, 1, 'home.exportword', 1, '2020-12-04 04:31:40'),
(58, 1, 'home.exportword', 1, '2020-12-04 04:34:34'),
(59, 1, 'home.exportword', 1, '2020-12-04 04:35:44'),
(60, 1, 'home.exportword', 1, '2020-12-04 04:36:00'),
(61, 1, 'home.exportword', 1, '2020-12-04 04:37:36'),
(62, 1, 'home.exportword', 1, '2020-12-04 04:38:25'),
(63, 1, 'home.exportword', 1, '2020-12-04 04:46:46'),
(64, 1, 'home.exportword', 1, '2020-12-04 04:57:39'),
(65, 1, 'home.exportword', 1, '2020-12-04 04:58:00'),
(66, 1, 'home.exportword', 1, '2020-12-04 04:58:09'),
(67, 1, 'home.exportword', 1, '2020-12-04 04:58:25'),
(68, 1, 'home.exportword', 1, '2020-12-04 04:58:35'),
(69, 1, 'home.exportword', 1, '2020-12-04 04:58:42'),
(70, 1, 'home.exportword', 1, '2020-12-04 04:59:25'),
(71, 1, 'home.exportword', 1, '2020-12-04 05:02:38'),
(72, 1, 'home.exportword', 1, '2020-12-04 05:04:42'),
(73, 1, 'home.exportword', 1, '2020-12-04 05:04:57'),
(74, 1, 'home.exportword', 1, '2020-12-04 05:05:10'),
(75, 1, 'home.exportword', 1, '2020-12-04 05:05:21'),
(76, 1, 'home.exportword', 1, '2020-12-04 05:07:27'),
(77, 1, 'home.exportword', 1, '2020-12-04 05:07:33'),
(78, 1, 'home.exportword', 1, '2020-12-04 05:08:18'),
(79, 1, 'home.exportword', 1, '2020-12-04 05:16:20'),
(80, 1, 'home.exportword', 1, '2020-12-04 05:17:18'),
(81, 1, 'home.exportword', 1, '2020-12-04 05:17:54'),
(82, 1, 'home.exportword', 1, '2020-12-04 05:18:07'),
(83, 1, 'home.exportword', 1, '2020-12-04 05:18:10'),
(84, 1, 'home.exportword', 1, '2020-12-04 05:18:15'),
(85, 1, 'home.exportword', 1, '2020-12-04 05:18:19'),
(86, 1, 'home.exportword', 1, '2020-12-04 05:18:21'),
(87, 1, 'home.exportword', 1, '2020-12-04 05:18:30'),
(88, 1, 'home.exportword', 1, '2020-12-04 05:18:55'),
(89, 1, 'home.exportword', 1, '2020-12-04 05:19:41'),
(90, 1, 'home.exportword', 1, '2020-12-04 05:20:01'),
(91, 1, 'home.exportword', 1, '2020-12-04 05:20:11'),
(92, 1, 'home.exportword', 1, '2020-12-04 05:20:28'),
(93, 1, 'home.exportword', 1, '2020-12-04 05:20:54'),
(94, 1, 'home.exportword', 1, '2020-12-04 05:23:31'),
(95, 1, 'home.exportword', 1, '2020-12-04 05:23:36'),
(96, 1, 'home.exportword', 1, '2020-12-04 05:24:01'),
(97, 1, 'home.exportword', 1, '2020-12-04 05:24:10'),
(98, 1, 'home.exportword', 1, '2020-12-04 05:24:21'),
(99, 1, 'home.exportword', 1, '2020-12-04 05:25:40'),
(100, 1, 'home.exportword', 1, '2020-12-04 05:28:52'),
(101, 1, 'home.exportword', 1, '2020-12-04 05:29:34'),
(102, 1, 'home.exportword', 1, '2020-12-04 05:30:27'),
(103, 1, 'home.exportword', 1, '2020-12-04 05:30:39'),
(104, 1, 'home.exportword', 1, '2020-12-04 05:31:36'),
(105, 1, 'home.exportword', 1, '2020-12-04 05:31:55'),
(106, 1, 'home.exportword', 1, '2020-12-04 05:35:21'),
(107, 1, 'home.exportword', 1, '2020-12-04 05:35:49'),
(108, 1, 'home.exportword', 1, '2020-12-04 05:36:20'),
(109, 1, 'home.exportword', 1, '2020-12-04 05:36:24'),
(110, 1, 'home.exportword', 1, '2020-12-04 05:40:18'),
(111, 1, 'home.exportword', 1, '2020-12-04 05:43:54'),
(112, 1, 'home.exportword', 1, '2020-12-04 05:44:35'),
(113, 1, 'home.show', 1, '2020-12-04 05:52:31'),
(114, 1, 'home.show', 1, '2020-12-04 05:52:31'),
(115, 1, 'home.show', 1, '2020-12-04 05:53:00'),
(116, 1, 'home.show', 1, '2020-12-04 05:53:39'),
(117, 1, 'home.show', 1, '2020-12-04 05:54:21'),
(118, 1, 'home.show', 1, '2020-12-04 05:54:36'),
(119, 1, 'home.show', 1, '2020-12-04 05:54:42'),
(120, 1, 'home.show', 1, '2020-12-04 05:55:22'),
(121, 1, 'home.show', 1, '2020-12-04 05:56:32'),
(122, 1, 'home.show', 1, '2020-12-04 05:56:50'),
(123, 1, 'home.show', 1, '2020-12-04 05:57:19'),
(124, 1, 'home.show', 1, '2020-12-04 05:57:44'),
(125, 1, 'home.show', 1, '2020-12-04 05:58:04'),
(126, 1, 'home.show', 1, '2020-12-04 05:59:36'),
(127, 1, 'home.show', 1, '2020-12-04 05:59:49'),
(128, 1, 'home.show', 1, '2020-12-04 05:59:59'),
(129, 1, 'home.show', 1, '2020-12-04 06:00:30'),
(130, 1, 'home.show', 1, '2020-12-04 06:00:44'),
(131, 1, 'home.show', 1, '2020-12-04 06:00:57'),
(132, 1, 'home.show', 1, '2020-12-04 06:02:24'),
(133, 1, 'home.show', 1, '2020-12-04 06:02:30'),
(134, 1, 'home.show', 1, '2020-12-04 06:03:07'),
(135, 1, 'home.show', 1, '2020-12-04 06:03:26'),
(136, 1, 'home.show', 1, '2020-12-04 06:04:21'),
(137, 1, 'home.show', 1, '2020-12-04 06:05:05'),
(138, 1, 'home.show', 1, '2020-12-04 06:06:00'),
(139, 1, 'home.show', 1, '2020-12-04 06:07:21'),
(140, 1, 'home.show', 1, '2020-12-04 06:08:16'),
(141, 1, 'home.show', 1, '2020-12-04 06:09:27'),
(142, 1, 'home.show', 1, '2020-12-04 06:10:07'),
(143, 1, 'home.show', 1, '2020-12-04 06:11:11'),
(144, 1, 'home.show', 1, '2020-12-04 06:11:20'),
(145, 1, 'home.show', 1, '2020-12-04 06:12:21'),
(146, 1, 'home.show', 1, '2020-12-04 06:13:15'),
(147, 1, 'home.show', 1, '2020-12-04 06:16:11'),
(148, 1, 'home.show', 1, '2020-12-04 06:22:53'),
(149, 1, 'home.show', 1, '2020-12-04 06:23:04'),
(150, 1, 'home.show', 1, '2020-12-04 06:23:13'),
(151, 1, 'home.exportword', 1, '2020-12-04 06:23:17'),
(152, 1, 'home.exportword', 1, '2020-12-04 06:23:39'),
(153, 1, 'home.exportword', 1, '2020-12-04 06:23:47'),
(154, 1, 'home.exportword', 1, '2020-12-04 06:23:54'),
(155, 1, 'home.exportword', 1, '2020-12-04 06:23:57'),
(156, 1, 'home.exportword', 1, '2020-12-04 06:24:05'),
(157, 1, 'home.exportword', 1, '2020-12-04 06:24:11'),
(158, 1, 'home.show', 1, '2020-12-04 06:25:47'),
(159, 1, 'home.exportword', 1, '2020-12-04 06:25:51'),
(160, 1, 'home.exportword', 1, '2020-12-04 06:28:10'),
(161, 1, 'home.exportword', 1, '2020-12-04 06:33:44'),
(162, 1, 'home.show', 1, '2020-12-04 06:35:37'),
(163, 1, 'home.show', 1, '2020-12-04 06:35:53'),
(164, 1, 'home.show', 1, '2020-12-04 06:40:18'),
(165, 1, 'home.show', 1, '2020-12-04 06:41:06'),
(166, 1, 'home.show', 1, '2020-12-04 06:41:18'),
(167, 1, 'home.show', 1, '2020-12-04 06:41:27'),
(168, 1, 'home.show', 1, '2020-12-04 06:43:10'),
(169, 1, 'home.show', 1, '2020-12-04 06:44:51'),
(170, 1, 'home.exportword', 1, '2020-12-04 06:45:10'),
(171, 1, 'home.show', 1, '2020-12-04 06:46:05'),
(172, 1, 'home.show', 1, '2020-12-04 06:47:48'),
(173, 1, 'home.exportword', 1, '2020-12-04 06:47:54'),
(174, 1, 'home.show', 1, '2020-12-04 06:48:24'),
(175, 1, 'home.show', 1, '2020-12-04 06:48:35'),
(176, 1, 'home.exportword', 1, '2020-12-04 06:48:41'),
(177, 1, 'home.show', 1, '2020-12-04 06:55:18'),
(178, 1, 'auth.logout', 1, '2020-12-04 06:55:24'),
(179, 1, 'auth.login', 1, '2020-12-04 06:55:29'),
(180, 1, 'home.show', 1, '2020-12-04 06:55:29'),
(181, 1, 'home.show', 1, '2020-12-04 06:58:46'),
(182, 1, 'home.show', 1, '2020-12-04 06:58:51'),
(183, 1, 'home.show', 1, '2020-12-04 06:59:06'),
(184, 1, 'home.show', 1, '2020-12-04 07:01:04'),
(185, 1, 'home.show', 1, '2020-12-04 07:03:04'),
(186, 1, 'home.exportword', 1, '2020-12-04 07:03:21'),
(187, 1, 'home.show', 1, '2020-12-04 07:04:48'),
(188, 1, 'home.show', 1, '2020-12-04 07:05:16'),
(189, 1, 'home.exportword', 1, '2020-12-04 07:05:21'),
(190, 1, 'home.exportword', 1, '2020-12-04 07:06:49'),
(191, 1, 'home.show', 1, '2020-12-04 07:06:50'),
(192, 1, 'home.exportword', 1, '2020-12-04 07:06:55'),
(193, 1, 'auth.login', 1, '2020-12-04 07:07:28'),
(194, 1, 'home.show', 1, '2020-12-04 07:07:28'),
(195, 1, 'home.show', 1, '2020-12-04 07:15:21'),
(196, 1, 'home.exportword', 1, '2020-12-04 07:15:28'),
(197, 1, 'home.show', 1, '2020-12-04 07:15:57'),
(198, 1, 'home.exportword', 1, '2020-12-04 07:16:02'),
(199, 1, 'home.show', 1, '2020-12-04 07:17:04'),
(200, 1, 'home.show', 1, '2020-12-04 07:18:01'),
(201, 1, 'home.exportword', 1, '2020-12-04 07:18:04'),
(202, 1, 'home.exportword', 1, '2020-12-04 07:20:55'),
(203, 1, 'home.exportword', 1, '2020-12-04 07:21:14'),
(204, 1, 'home.exportword', 1, '2020-12-04 07:21:17'),
(205, 1, 'home.exportword', 1, '2020-12-04 07:21:45'),
(206, 1, 'home.show', 1, '2020-12-04 07:21:51'),
(207, 1, 'home.exportword', 1, '2020-12-04 07:21:56'),
(208, 1, 'home.show', 1, '2020-12-04 07:22:31'),
(209, 1, 'home.show', 1, '2020-12-04 07:22:39'),
(210, 1, 'home.exportword', 1, '2020-12-04 07:22:43'),
(211, 1, 'auth.login', 1, '2020-12-04 07:30:05'),
(212, 1, 'home.show', 1, '2020-12-04 07:30:05'),
(213, 1, 'home.show', 1, '2020-12-04 07:31:47'),
(214, 1, 'home.exportword', 1, '2020-12-04 07:33:24'),
(215, 1, 'home.exportword', 1, '2020-12-04 07:36:31'),
(216, 1, 'home.exportword', 1, '2020-12-04 07:36:33'),
(217, 1, 'home.exportword', 1, '2020-12-04 07:37:16'),
(218, 1, 'home.exportword', 1, '2020-12-04 07:37:22'),
(219, 1, 'home.exportword', 1, '2020-12-04 07:37:47'),
(220, 1, 'home.show', 1, '2020-12-04 07:38:15'),
(221, 1, 'home.show', 1, '2020-12-04 07:38:25'),
(222, 1, 'home.exportword', 1, '2020-12-04 07:38:29'),
(223, 1, 'home.show', 1, '2020-12-04 07:38:57'),
(224, 1, 'home.show', 1, '2020-12-04 07:39:04'),
(225, 1, 'home.show', 1, '2020-12-04 07:39:11'),
(226, 1, 'home.show', 1, '2020-12-04 07:42:51'),
(227, 1, 'home.show', 1, '2020-12-04 07:47:03'),
(228, 1, 'auth.logout', 1, '2020-12-04 07:49:04'),
(229, 1, 'auth.login', 1, '2020-12-04 07:51:53'),
(230, 1, 'home.show', 1, '2020-12-04 07:51:53'),
(231, 1, 'home.exportword', 1, '2020-12-04 07:52:04'),
(232, 1, 'home.exportword', 1, '2020-12-04 07:52:31'),
(233, 1, 'home.exportword', 1, '2020-12-04 07:52:41'),
(234, 1, 'home.exportword', 1, '2020-12-04 07:53:03'),
(235, 1, 'home.exportword', 1, '2020-12-04 07:53:18'),
(236, 1, 'home.exportword', 1, '2020-12-04 16:10:38'),
(237, 1, 'home.exportword', 1, '2020-12-04 16:11:31'),
(238, 1, 'home.show', 1, '2020-12-04 16:12:10'),
(239, 1, 'home.exportword', 1, '2020-12-04 16:12:15'),
(240, 1, 'home.show', 1, '2020-12-04 16:15:39'),
(241, 1, 'home.exportword', 1, '2020-12-04 16:15:51'),
(242, 1, 'home.exportword', 1, '2020-12-04 16:15:52'),
(243, 1, 'home.show', 1, '2020-12-04 16:16:05'),
(244, 1, 'home.exportword', 1, '2020-12-04 16:16:10'),
(245, 1, 'home.exportword', 1, '2020-12-04 16:16:11'),
(246, 1, 'home.show', 1, '2020-12-04 16:16:32'),
(247, 1, 'home.show', 1, '2020-12-04 16:16:41'),
(248, 1, 'auth.login', 1, '2020-12-23 19:55:17'),
(249, 1, 'home.show', 1, '2020-12-23 19:55:17'),
(250, 1, 'home.show', 1, '2020-12-23 19:55:24'),
(251, 1, 'mainapi.items', 1, '2020-12-23 19:55:56'),
(252, 1, 'api.listening', 1, '2020-12-23 19:57:12'),
(253, 1, 'api.listening', 1, '2020-12-23 20:00:31'),
(254, 1, 'api.listening', 1, '2020-12-23 20:01:49'),
(255, 1, 'api.listening', 1, '2020-12-23 20:04:18'),
(256, 1, 'api.listening', 1, '2020-12-23 20:05:00'),
(257, 1, 'api.listening', 1, '2020-12-23 20:05:54'),
(258, 1, 'api.listening', 1, '2020-12-23 20:09:34'),
(259, 1, 'home.show', 1, '2020-12-23 20:11:01'),
(260, 1, 'auth.logout', 1, '2020-12-23 20:11:06'),
(261, 1, 'auth.login', 1, '2020-12-23 20:12:14'),
(262, 1, 'home.show', 1, '2020-12-23 20:12:14'),
(263, 1, 'auth.logout', 1, '2020-12-23 20:12:30'),
(264, 1, 'auth.login', 1, '2020-12-23 20:12:41'),
(265, 1, 'home.show', 1, '2020-12-23 20:12:41'),
(266, 1, 'auth.logout', 1, '2020-12-23 20:13:49'),
(267, 1, 'auth.login', 1, '2020-12-23 20:14:01'),
(268, 1, 'home.show', 1, '2020-12-23 20:14:01'),
(269, 1, 'auth.logout', 1, '2020-12-23 20:14:03'),
(270, 1, 'auth.login', 1, '2020-12-23 20:14:16'),
(271, 1, 'home.show', 1, '2020-12-23 20:14:16'),
(272, 1, 'auth.logout', 1, '2020-12-23 20:14:30'),
(273, 1, 'auth.login', 1, '2021-04-04 06:59:55'),
(274, 1, 'home.show', 1, '2021-04-04 06:59:55'),
(275, 1, 'home.show', 1, '2021-04-04 07:00:43'),
(276, 1, 'home.show', 1, '2021-04-04 07:00:58'),
(277, 1, 'home.show', 1, '2021-04-04 07:02:32'),
(278, 1, 'home.show', 1, '2021-04-04 07:02:38'),
(279, 1, 'home.show', 1, '2021-04-04 07:03:07'),
(280, 1, 'home.show', 1, '2021-04-04 07:05:08'),
(281, 1, 'home.show', 1, '2021-04-04 07:05:21'),
(282, 1, 'home.show', 1, '2021-04-04 07:05:44'),
(283, 1, 'home.show', 1, '2021-04-04 07:06:27'),
(284, 1, 'home.show', 1, '2021-04-04 07:06:51'),
(285, 1, 'home.show', 1, '2021-04-04 07:07:45'),
(286, 1, 'home.show', 1, '2021-04-04 07:28:37'),
(287, 1, 'home.show', 1, '2021-04-04 07:28:45'),
(288, 1, 'home.show', 1, '2021-04-04 07:28:55'),
(289, 1, 'home.show', 1, '2021-04-04 07:31:22'),
(290, 1, 'home.show', 1, '2021-04-04 07:32:58'),
(291, 1, 'home.show', 1, '2021-04-04 07:34:26'),
(292, 1, 'home.show', 1, '2021-04-04 07:34:44'),
(293, 1, 'home.show', 1, '2021-04-04 07:36:18'),
(294, 1, 'home.show', 1, '2021-04-04 07:36:59'),
(295, 1, 'home.show', 1, '2021-04-04 07:37:06'),
(296, 1, 'home.show', 1, '2021-04-04 07:38:45'),
(297, 1, 'home.show', 1, '2021-04-04 07:40:08'),
(298, 1, 'home.show', 1, '2021-04-04 07:41:06'),
(299, 1, 'home.show', 1, '2021-04-04 07:41:36'),
(300, 1, 'listening.dashboard', 1, '2021-04-04 07:50:57'),
(301, 1, 'listening.dashboard', 1, '2021-04-04 07:51:56'),
(302, 1, 'listening.dashboard', 1, '2021-04-04 07:52:08'),
(303, 1, 'listening.dashboard', 1, '2021-04-04 07:52:20'),
(304, 1, 'home.show', 1, '2021-04-04 07:56:43'),
(305, 1, 'listening.dashboard', 1, '2021-04-04 07:56:48'),
(306, 1, 'listening.dashboard', 1, '2021-04-04 07:58:44'),
(307, 1, 'listening.dashboard', 1, '2021-04-04 07:58:48'),
(308, 1, 'listening.dashboard', 1, '2021-04-04 07:59:16'),
(309, 1, 'listening.dashboard', 1, '2021-04-04 07:59:41'),
(310, 1, 'auth.login', 1, '2021-04-04 09:39:52'),
(311, 1, 'home.show', 1, '2021-04-04 09:39:52'),
(312, 1, 'listening.dashboard', 1, '2021-04-04 09:39:56'),
(313, 1, 'listening.dashboard', 1, '2021-04-04 09:46:12'),
(314, 1, 'listening.dashboard', 1, '2021-04-04 09:48:09'),
(315, 1, 'listening.dashboard', 1, '2021-04-04 09:49:06'),
(316, 1, 'listening.dashboard', 1, '2021-04-04 09:49:30'),
(317, 1, 'listening.dashboard', 1, '2021-04-04 09:50:31'),
(318, 1, 'listening.dashboard', 1, '2021-04-04 12:49:51'),
(319, 1, 'listening.dashboard', 1, '2021-04-04 12:50:15'),
(320, 1, 'listening.dashboard', 1, '2021-04-04 12:55:49'),
(321, 1, 'listening.dashboard', 1, '2021-04-04 12:56:05'),
(322, 1, 'listening.dashboard', 1, '2021-04-04 12:56:30'),
(323, 1, 'listening.dashboard', 1, '2021-04-04 12:56:40'),
(324, 1, 'listening.dashboard', 1, '2021-04-04 13:04:16'),
(325, 1, 'listening.dashboard', 1, '2021-04-04 13:05:38'),
(326, 1, 'listening.dashboard', 1, '2021-04-04 13:06:53'),
(327, 1, 'listening.dashboard', 1, '2021-04-04 13:07:41'),
(328, 1, 'listening.dashboard', 1, '2021-04-04 13:08:32'),
(329, 1, 'listening.dashboard', 1, '2021-04-04 13:21:58'),
(330, 1, 'listening.dashboard', 1, '2021-04-04 13:22:25'),
(331, 1, 'listening.dashboard', 1, '2021-04-04 13:33:50'),
(332, 1, 'listening.dashboard', 1, '2021-04-04 13:37:46'),
(333, 1, 'listening.dashboard', 1, '2021-04-04 13:38:00'),
(334, 1, 'listening.dashboard', 1, '2021-04-04 13:38:09'),
(335, 1, 'listening.dashboard', 1, '2021-04-04 13:38:42'),
(336, 1, 'home.show', 1, '2021-04-04 13:39:13'),
(337, 1, 'listening.dashboard', 1, '2021-04-04 13:42:02'),
(338, 1, 'home.show', 1, '2021-04-04 14:59:09'),
(339, 1, 'home.show', 1, '2021-04-04 15:01:03'),
(340, 1, 'listening.dashboard', 1, '2021-04-04 15:02:52'),
(341, 1, 'home.show', 1, '2021-04-04 15:02:53'),
(342, 1, 'home.show', 1, '2021-04-04 15:03:19'),
(343, 1, 'home.show', 1, '2021-04-04 15:04:16'),
(344, 1, 'home.show', 1, '2021-04-04 15:04:33'),
(345, 1, 'home.show', 1, '2021-04-04 15:06:59'),
(346, 1, 'home.show', 1, '2021-04-04 15:07:19'),
(347, 1, 'home.show', 1, '2021-04-04 15:07:50'),
(348, 1, 'listening.dashboard', 1, '2021-04-04 15:09:02'),
(349, 1, 'listening.dashboard', 1, '2021-04-04 15:09:16'),
(350, 1, 'listening.dashboard', 1, '2021-04-04 15:09:49'),
(351, 1, 'listening.dashboard', 1, '2021-04-04 15:10:00'),
(352, 1, 'listening.dashboard', 1, '2021-04-04 15:10:03'),
(353, 1, 'listening.dashboard', 1, '2021-04-04 15:11:02'),
(354, 1, 'listening.dashboard', 1, '2021-04-04 15:11:21'),
(355, 1, 'listening.dashboard', 1, '2021-04-04 15:11:33'),
(356, 1, 'listening.dashboard', 1, '2021-04-04 15:11:52'),
(357, 1, 'listening.dashboard', 1, '2021-04-04 15:12:16'),
(358, 1, 'listening.dashboard', 1, '2021-04-04 15:13:07'),
(359, 1, 'listening.dashboard', 1, '2021-04-04 15:14:50'),
(360, 1, 'listening.dashboard', 1, '2021-04-04 15:15:14'),
(361, 1, 'listening.dashboard', 1, '2021-04-04 15:15:19'),
(362, 1, 'listening.dashboard', 1, '2021-04-04 15:15:55'),
(363, 1, 'listening.dashboard', 1, '2021-04-04 15:19:49'),
(364, 1, 'listening.dashboard', 1, '2021-04-04 15:20:37'),
(365, 1, 'listening.dashboard', 1, '2021-04-04 15:20:51'),
(366, 1, 'listening.dashboard', 1, '2021-04-04 15:21:28'),
(367, 1, 'listening.dashboard', 1, '2021-04-04 15:22:07'),
(368, 1, 'listening.dashboard', 1, '2021-04-04 15:25:05'),
(369, 1, 'listening.dashboard', 1, '2021-04-04 15:25:12'),
(370, 1, 'listening.dashboard', 1, '2021-04-04 15:26:18'),
(371, 1, 'listening.dashboard', 1, '2021-04-04 15:26:26'),
(372, 1, 'listening.dashboard', 1, '2021-04-04 15:27:12'),
(373, 1, 'listening.dashboard', 1, '2021-04-04 15:28:04'),
(374, 1, 'listening.dashboard', 1, '2021-04-04 15:28:25'),
(375, 1, 'listening.dashboard', 1, '2021-04-04 15:28:43'),
(376, 1, 'listening.dashboard', 1, '2021-04-04 15:28:48'),
(377, 1, 'listening.dashboard', 1, '2021-04-04 15:28:53'),
(378, 1, 'listening.dashboard', 1, '2021-04-04 15:29:19'),
(379, 1, 'listening.dashboard', 1, '2021-04-04 15:29:34'),
(380, 1, 'listening.dashboard', 1, '2021-04-04 15:30:02'),
(381, 1, 'listening.dashboard', 1, '2021-04-04 15:30:22'),
(382, 1, 'listening.dashboard', 1, '2021-04-04 15:31:10'),
(383, 1, 'listening.dashboard', 1, '2021-04-04 15:31:22'),
(384, 1, 'listening.dashboard', 1, '2021-04-04 15:32:12'),
(385, 1, 'listening.dashboard', 1, '2021-04-04 15:37:41'),
(386, 1, 'listening.weekform', 1, '2021-04-04 15:42:18'),
(387, 1, 'listening.weekform', 1, '2021-04-04 15:42:43'),
(388, 1, 'listening.weekform', 1, '2021-04-04 15:43:42'),
(389, 1, 'listening.weekform', 1, '2021-04-04 15:45:11'),
(390, 1, 'listening.weekform', 1, '2021-04-04 15:46:10'),
(391, 1, 'listening.dashboard', 1, '2021-04-04 15:46:32'),
(392, 1, 'listening.weekform', 1, '2021-04-04 15:47:30'),
(393, 1, 'listening.dashboard', 1, '2021-04-04 15:48:26'),
(394, 1, 'listening.weekform', 1, '2021-04-04 15:48:30'),
(395, 1, 'listening.weekform', 1, '2021-04-04 15:48:42'),
(396, 1, 'listening.weekform', 1, '2021-04-04 15:49:25');

-- --------------------------------------------------------

--
-- Структура таблицы `slides`
--

CREATE TABLE `slides` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `url` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category` int NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `slides`
--

INSERT INTO `slides` (`id`, `user`, `url`, `date`, `category`, `status`) VALUES
(1, 1, 'https://i.picsum.photos/id/809/400/250.jpg?hmac=2f-6tfCMn0Xqf1ekbU_3yjVTiOttcDGgBw8me_BFdDw', '2020-12-06 00:55:42', 0, 1),
(2, 1, 'https://i.picsum.photos/id/545/400/250.jpg?hmac=dwV06WncX9F-q2Yw37l-ffGz1iH3Lt9aFX4ya5RSYiw', '2020-12-06 00:56:51', 0, 1),
(3, 1, 'https://i.picsum.photos/id/2/400/250.jpg?hmac=xQarGFhPP7UqhRdaEoqEBcWnlHuctr99TQdkB9fv3O8', '2020-12-06 00:56:51', 0, 1),
(4, 1, 'https://i.picsum.photos/id/1079/400/250.jpg?hmac=InqR2wOl1RhfBRCZIlImg1KQdZFCs3OUc_vzn3BQpBU', '2020-12-06 00:57:18', 0, 1),
(5, 1, 'https://i.picsum.photos/id/580/400/250.jpg?hmac=TGatCf-0jVubHnohnmf0vOr4-ySdVgiQ021ShH_L2KY', '2020-12-06 00:57:18', 0, 1),
(6, 1, 'https://i.picsum.photos/id/80/400/250.jpg?hmac=_SdiunS86ZDGpytlABJ7u16UWvd8UT-H01atgESh_D0', '2020-12-06 00:57:24', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tips`
--

CREATE TABLE `tips` (
  `id` int NOT NULL,
  `text` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `tips`
--

INSERT INTO `tips` (`id`, `text`, `user`, `date`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-07 02:38:34');

-- --------------------------------------------------------

--
-- Структура таблицы `tips_user`
--

CREATE TABLE `tips_user` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `tips` int NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `tips_user`
--

INSERT INTO `tips_user` (`id`, `user`, `tips`, `status`, `date`) VALUES
(1, 1, 1, 0, '2020-12-07 02:51:10');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `modified`, `created`) VALUES
(1, 'admin', '$2y$10$MszeSZpsEJI93y19.4EXUe78rXAkWI5loFH5j9sDrgq5pFNBzqbre', 'lostov@hi.uz', NULL, '2020-12-03 03:38:33');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `GROUP.NAME` (`name`) USING BTREE;

--
-- Индексы таблицы `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `listening`
--
ALTER TABLE `listening`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `listening_weeks`
--
ALTER TABLE `listening_weeks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tips`
--
ALTER TABLE `tips`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tips_user`
--
ALTER TABLE `tips_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `items`
--
ALTER TABLE `items`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `listening`
--
ALTER TABLE `listening`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `listening_weeks`
--
ALTER TABLE `listening_weeks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=397;

--
-- AUTO_INCREMENT для таблицы `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `tips`
--
ALTER TABLE `tips`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `tips_user`
--
ALTER TABLE `tips_user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
