<?php

/**
 * Загрузочный файл
 * Загрузите все, что нужно приложению, с помощью автозагрузчика
 * Загрузить список маршрутов
 */

require_once __DIR__.'/../vendor/autoload.php';

require_once __DIR__.'/../app/routes.php';
