<?php

use Core\Routing\Router;
use Core\Foundation\Application;

/**
 * Создайте приложение
 */

/**
 * Маршрутизатор Instancier
 */
$router = Router::getInstance();

/**
 * Вызов приложения
 */
$app = new Application($router);

/**
 * Retourner Application
 */
return $app;
