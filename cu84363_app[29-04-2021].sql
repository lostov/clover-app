-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 29 2021 г., 21:15
-- Версия сервера: 8.0.19
-- Версия PHP: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cu84363_app`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int NOT NULL,
  `title` text NOT NULL,
  `text` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category` int NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `text`, `user`, `date`, `category`, `status`) VALUES
(1, 'Some title 1', 'Lorem1 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:41', 1, 1),
(2, 'Some title 2', 'Lorem2 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:47', 1, 1),
(3, 'Some title 3', 'Lorem3 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:51', 1, 1),
(4, 'Some title 4', 'Lorem4 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:54', 1, 1),
(5, 'Some title 5', 'Lorem5 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:57', 1, 1),
(6, 'Some title 6', 'Lorem6 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:59', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int NOT NULL,
  `title` text NOT NULL,
  `parent` int NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `configs`
--

CREATE TABLE `configs` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = default.null\r\n1 = string\r\n2 = int\r\n3 = bool\r\n4 = json.array\r\n5 = json.object',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sort` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `configs`
--

INSERT INTO `configs` (`id`, `user`, `name`, `value`, `date`, `type`, `title`, `sort`) VALUES
(1, 1, 'slides.count', '5', '2020-12-06 01:06:46', 2, 'How Many Slides for a use on home page', 1),
(2, 1, 'slides.total', '10', '2020-12-06 01:47:58', 2, 'Totaly', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `config_colors`
--

CREATE TABLE `config_colors` (
  `id` int NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `config_colors`
--

INSERT INTO `config_colors` (`id`, `name`, `code`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'Black', '#000', '2021-04-06 07:24:50', 1, NULL, NULL),
(2, 'White', '#fff', '2021-04-06 07:24:55', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `contest`
--

CREATE TABLE `contest` (
  `id` int NOT NULL,
  `practice` int NOT NULL,
  `question` text COLLATE utf8mb4_general_ci NOT NULL,
  `answer` text COLLATE utf8mb4_general_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `items`
--

CREATE TABLE `items` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `type` int NOT NULL DEFAULT '1',
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `icon` text,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `items`
--

INSERT INTO `items` (`id`, `user`, `type`, `code`, `title`, `icon`, `text`) VALUES
(1, 1, 1, 'listening', 'Listening', 'some-icon', 'Extend your listening ability with question based exercises and b...'),
(2, 1, 1, 'reading', 'Reading', 'some-icon', 'Upgrade your reading skills with customized passages ...'),
(3, 1, 1, 'writing', 'Writing', 'some-icon', 'Become an excellent writer via master class sessions in-a...'),
(4, 1, 1, 'speaking', 'Speaking', 'some-icon', 'Transform your speaking skills using our \"Record\" feature and r...'),
(5, 1, 1, 'vocabulary', 'Vocabulary', 'some-icon', 'Turn into a vocabulary guru by just playing quizzes');

-- --------------------------------------------------------

--
-- Структура таблицы `listening`
--

CREATE TABLE `listening` (
  `id` int NOT NULL,
  `day` tinyint NOT NULL,
  `tips` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `listening`
--

INSERT INTO `listening` (`id`, `day`, `tips`, `title`, `text`, `user`, `date`) VALUES
(1, 1, 1, 'Lorem ipsum practice first', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-07 02:20:11'),
(2, 2, 1, 'Lorem ipsum practice second', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-07 02:20:28');

-- --------------------------------------------------------

--
-- Структура таблицы `listening_days`
--

CREATE TABLE `listening_days` (
  `id` int NOT NULL,
  `week` int NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` int DEFAULT NULL,
  `color` int NOT NULL,
  `text` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `listening_days`
--

INSERT INTO `listening_days` (`id`, `week`, `title`, `icon`, `color`, `text`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 1, 'Map', NULL, 1, 'Map - Day 1 - Week 1', '2021-04-06 08:51:27', 1, '2021-04-08 12:38:33', 1, 1),
(2, 1, 'Multiple', NULL, 1, 'Multiple - day 2 - Week 1', '2021-04-07 16:19:18', 1, '2021-04-08 12:38:10', 1, 1),
(3, 1, 'Gap', NULL, 1, 'Gap - day 3 - Week 1', '2021-04-07 20:35:03', 1, '2021-04-08 18:53:42', 1, 1),
(4, 1, '3/5', NULL, 2, '3/5 - day 4 - Week 1', '2021-04-08 18:54:24', 1, NULL, NULL, 1),
(5, 1, 'Multiple level 2', NULL, 1, 'Multiple level 2- day 5 - Week 1', '2021-04-08 18:54:51', 1, NULL, NULL, 1),
(6, 1, 'Gap level 2', NULL, 1, 'Gap level 2 - day 6 - Week 1', '2021-04-08 18:55:12', 1, NULL, NULL, 1),
(7, 2, 'Map', NULL, 1, 'Map - Day 1 - Week 2', '2021-04-08 18:58:01', 1, NULL, NULL, 1),
(8, 2, 'Multiple', NULL, 1, 'Multiple - day 2 - Week 2', '2021-04-08 18:58:01', 1, NULL, NULL, 1),
(9, 2, 'Gap', NULL, 1, 'Gap - day 3 - Week 2', '2021-04-08 18:58:01', 1, NULL, NULL, 1),
(10, 2, '3/5', NULL, 2, '3/5 - day 4 - Week 2', '2021-04-08 18:58:01', 1, NULL, NULL, 1),
(11, 2, 'Multiple level 2', NULL, 1, 'Multiple level 2- day 5 - Week 2', '2021-04-08 18:58:01', 1, NULL, NULL, 1),
(12, 2, 'Gap level 2', NULL, 1, 'Gap level 2 - day 6 - Week 2', '2021-04-08 18:58:01', 1, NULL, NULL, 1),
(13, 3, 'Map', NULL, 1, 'Map - Day 1 - Week 3', '2021-04-08 18:58:57', 1, NULL, NULL, 1),
(14, 3, 'Multiple', NULL, 1, 'Multiple - day 2 - Week 3', '2021-04-08 18:58:57', 1, NULL, NULL, 1),
(15, 3, 'Gap', NULL, 1, 'Gap - day 3 - Week 3', '2021-04-08 18:58:57', 1, NULL, NULL, 1),
(16, 3, '3/5', NULL, 2, '3/5 - day 4 - Week 3', '2021-04-08 18:58:57', 1, NULL, NULL, 1),
(17, 3, 'Multiple level 2', NULL, 1, 'Multiple level 2- day 5 - Week 3', '2021-04-08 18:58:57', 1, NULL, NULL, 1),
(18, 3, 'Gap level 2', NULL, 1, 'Gap level 2 - day 6 - Week 3', '2021-04-08 18:58:57', 1, NULL, NULL, 1),
(19, 4, 'Map', NULL, 1, 'Map - Day 1 - Week 4', '2021-04-08 18:59:12', 1, NULL, NULL, 1),
(20, 4, 'Multiple', NULL, 1, 'Multiple - day 2 - Week 4', '2021-04-08 18:59:12', 1, NULL, NULL, 1),
(21, 4, 'Gap', NULL, 1, 'Gap - day 3 - Week 4', '2021-04-08 18:59:12', 1, NULL, NULL, 1),
(22, 4, '3/5', NULL, 2, '3/5 - day 4 - Week 4', '2021-04-08 18:59:12', 1, NULL, NULL, 1),
(23, 4, 'Multiple level 2', NULL, 1, 'Multiple level 2- day 5 - Week 4', '2021-04-08 18:59:12', 1, NULL, NULL, 1),
(24, 4, 'Gap level 2', NULL, 1, 'Gap level 2 - day 6 - Week 4', '2021-04-08 18:59:12', 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `listening_practices`
--

CREATE TABLE `listening_practices` (
  `id` int NOT NULL,
  `day` int NOT NULL,
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `icon` int DEFAULT NULL,
  `color` int DEFAULT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `content` text NOT NULL,
  `media` int DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `listening_practices`
--

INSERT INTO `listening_practices` (`id`, `day`, `title`, `icon`, `color`, `text`, `content`, `media`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 1, 'Practice test 1', NULL, 1, '', '<div>Write the correct letter A-G next to questions 1-6.</div>\r\n<h3>The Neighborhood of Abbotswell   V</h3>\r\n<img src=\"_[[[SITE]]]_/media/photos/weeks/1/1/practice_1.png\" alt=\"map\"/>\r\n<ul>_[[[CONTESTS]]]_</ul>', 1, '2021-04-07 16:01:40', 1, '2021-04-08 20:19:31', 1, 1),
(2, 2, 'Some practice', NULL, 2, NULL, '', 2, '2021-04-07 16:01:47', 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `listening_weeks`
--

CREATE TABLE `listening_weeks` (
  `id` int NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` int DEFAULT NULL,
  `text` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `listening_weeks`
--

INSERT INTO `listening_weeks` (`id`, `title`, `icon`, `text`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 'Week 1', NULL, 'Description for Week 1', '2021-04-04 13:32:59', 1, '2021-04-07 19:04:59', 1, 1),
(2, 'Week 2', NULL, 'Some text', '2021-04-04 13:33:06', 1, NULL, NULL, 1),
(3, 'Week 3', NULL, 'adssadsdasadsad', '2021-04-06 05:46:28', 1, '2021-04-07 20:00:34', 1, 1),
(4, 'Week 4', NULL, 'adssadsdasadsad', '2021-04-06 05:50:29', 1, '2021-04-07 20:12:27', 1, 1),
(5, 'Week 5', NULL, 'Bbbb', '2021-04-06 06:05:38', 1, '2021-04-07 20:12:36', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `logs`
--

CREATE TABLE `logs` (
  `id` int NOT NULL,
  `type` int NOT NULL,
  `action` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `logs`
--

INSERT INTO `logs` (`id`, `type`, `action`, `user`, `date`) VALUES
(1, 1, 'home.show', 1, '2020-12-03 11:11:38'),
(2, 1, 'auth.logout', 1, '2020-12-03 11:12:07'),
(3, 1, 'home.show', 1, '2020-12-03 11:12:28'),
(4, 1, 'auth.logout', 1, '2020-12-03 11:14:51'),
(5, 1, 'auth.login', 1, '2020-12-03 11:15:10'),
(6, 1, 'home.show', 1, '2020-12-03 11:15:10'),
(7, 1, 'home.show', 1, '2020-12-04 03:48:02'),
(8, 1, 'home.show', 1, '2020-12-04 03:48:19'),
(9, 1, 'home.show', 1, '2020-12-04 03:48:32'),
(10, 1, 'home.show', 1, '2020-12-04 03:51:13'),
(11, 1, 'home.show', 1, '2020-12-04 03:52:19'),
(12, 1, 'home.show', 1, '2020-12-04 03:53:17'),
(13, 1, 'home.show', 1, '2020-12-04 03:54:11'),
(14, 1, 'home.show', 1, '2020-12-04 03:56:41'),
(15, 1, 'home.show', 1, '2020-12-04 03:56:52'),
(16, 1, 'home.show', 1, '2020-12-04 03:57:31'),
(17, 1, 'home.show', 1, '2020-12-04 03:58:01'),
(18, 1, 'home.show', 1, '2020-12-04 03:58:08'),
(19, 1, 'home.show', 1, '2020-12-04 03:59:34'),
(20, 1, 'home.show', 1, '2020-12-04 04:00:18'),
(21, 1, 'home.show', 1, '2020-12-04 04:00:40'),
(22, 1, 'home.show', 1, '2020-12-04 04:01:52'),
(23, 1, 'home.show', 1, '2020-12-04 04:03:15'),
(24, 1, 'home.show', 1, '2020-12-04 04:03:32'),
(25, 1, 'home.show', 1, '2020-12-04 04:04:46'),
(26, 1, 'home.show', 1, '2020-12-04 04:05:03'),
(27, 1, 'home.show', 1, '2020-12-04 04:05:09'),
(28, 1, 'home.show', 1, '2020-12-04 04:06:36'),
(29, 1, 'home.show', 1, '2020-12-04 04:09:26'),
(30, 1, 'home.show', 1, '2020-12-04 04:09:37'),
(31, 1, 'home.show', 1, '2020-12-04 04:09:53'),
(32, 1, 'home.show', 1, '2020-12-04 04:10:27'),
(33, 1, 'home.show', 1, '2020-12-04 04:10:39'),
(34, 1, 'home.show', 1, '2020-12-04 04:11:07'),
(35, 1, 'home.show', 1, '2020-12-04 04:13:37'),
(36, 1, 'home.show', 1, '2020-12-04 04:13:53'),
(37, 1, 'home.show', 1, '2020-12-04 04:14:03'),
(38, 1, 'home.show', 1, '2020-12-04 04:14:49'),
(39, 1, 'home.show', 1, '2020-12-04 04:15:15'),
(40, 1, 'home.show', 1, '2020-12-04 04:17:12'),
(41, 1, 'home.show', 1, '2020-12-04 04:17:53'),
(42, 1, 'home.show', 1, '2020-12-04 04:18:21'),
(43, 1, 'home.show', 1, '2020-12-04 04:19:02'),
(44, 1, 'home.show', 1, '2020-12-04 04:19:15'),
(45, 1, 'home.show', 1, '2020-12-04 04:19:21'),
(46, 1, 'home.show', 1, '2020-12-04 04:19:51'),
(47, 1, 'home.show', 1, '2020-12-04 04:20:45'),
(48, 1, 'home.show', 1, '2020-12-04 04:21:57'),
(49, 1, 'home.show', 1, '2020-12-04 04:22:04'),
(50, 1, 'home.show', 1, '2020-12-04 04:22:13'),
(51, 1, 'home.exportword', 1, '2020-12-04 04:25:25'),
(52, 1, 'home.exportword', 1, '2020-12-04 04:25:28'),
(53, 1, 'home.exportword', 1, '2020-12-04 04:25:44'),
(54, 1, 'home.exportword', 1, '2020-12-04 04:26:34'),
(55, 1, 'home.exportword', 1, '2020-12-04 04:26:59'),
(56, 1, 'home.exportword', 1, '2020-12-04 04:31:12'),
(57, 1, 'home.exportword', 1, '2020-12-04 04:31:40'),
(58, 1, 'home.exportword', 1, '2020-12-04 04:34:34'),
(59, 1, 'home.exportword', 1, '2020-12-04 04:35:44'),
(60, 1, 'home.exportword', 1, '2020-12-04 04:36:00'),
(61, 1, 'home.exportword', 1, '2020-12-04 04:37:36'),
(62, 1, 'home.exportword', 1, '2020-12-04 04:38:25'),
(63, 1, 'home.exportword', 1, '2020-12-04 04:46:46'),
(64, 1, 'home.exportword', 1, '2020-12-04 04:57:39'),
(65, 1, 'home.exportword', 1, '2020-12-04 04:58:00'),
(66, 1, 'home.exportword', 1, '2020-12-04 04:58:09'),
(67, 1, 'home.exportword', 1, '2020-12-04 04:58:25'),
(68, 1, 'home.exportword', 1, '2020-12-04 04:58:35'),
(69, 1, 'home.exportword', 1, '2020-12-04 04:58:42'),
(70, 1, 'home.exportword', 1, '2020-12-04 04:59:25'),
(71, 1, 'home.exportword', 1, '2020-12-04 05:02:38'),
(72, 1, 'home.exportword', 1, '2020-12-04 05:04:42'),
(73, 1, 'home.exportword', 1, '2020-12-04 05:04:57'),
(74, 1, 'home.exportword', 1, '2020-12-04 05:05:10'),
(75, 1, 'home.exportword', 1, '2020-12-04 05:05:21'),
(76, 1, 'home.exportword', 1, '2020-12-04 05:07:27'),
(77, 1, 'home.exportword', 1, '2020-12-04 05:07:33'),
(78, 1, 'home.exportword', 1, '2020-12-04 05:08:18'),
(79, 1, 'home.exportword', 1, '2020-12-04 05:16:20'),
(80, 1, 'home.exportword', 1, '2020-12-04 05:17:18'),
(81, 1, 'home.exportword', 1, '2020-12-04 05:17:54'),
(82, 1, 'home.exportword', 1, '2020-12-04 05:18:07'),
(83, 1, 'home.exportword', 1, '2020-12-04 05:18:10'),
(84, 1, 'home.exportword', 1, '2020-12-04 05:18:15'),
(85, 1, 'home.exportword', 1, '2020-12-04 05:18:19'),
(86, 1, 'home.exportword', 1, '2020-12-04 05:18:21'),
(87, 1, 'home.exportword', 1, '2020-12-04 05:18:30'),
(88, 1, 'home.exportword', 1, '2020-12-04 05:18:55'),
(89, 1, 'home.exportword', 1, '2020-12-04 05:19:41'),
(90, 1, 'home.exportword', 1, '2020-12-04 05:20:01'),
(91, 1, 'home.exportword', 1, '2020-12-04 05:20:11'),
(92, 1, 'home.exportword', 1, '2020-12-04 05:20:28'),
(93, 1, 'home.exportword', 1, '2020-12-04 05:20:54'),
(94, 1, 'home.exportword', 1, '2020-12-04 05:23:31'),
(95, 1, 'home.exportword', 1, '2020-12-04 05:23:36'),
(96, 1, 'home.exportword', 1, '2020-12-04 05:24:01'),
(97, 1, 'home.exportword', 1, '2020-12-04 05:24:10'),
(98, 1, 'home.exportword', 1, '2020-12-04 05:24:21'),
(99, 1, 'home.exportword', 1, '2020-12-04 05:25:40'),
(100, 1, 'home.exportword', 1, '2020-12-04 05:28:52'),
(101, 1, 'home.exportword', 1, '2020-12-04 05:29:34'),
(102, 1, 'home.exportword', 1, '2020-12-04 05:30:27'),
(103, 1, 'home.exportword', 1, '2020-12-04 05:30:39'),
(104, 1, 'home.exportword', 1, '2020-12-04 05:31:36'),
(105, 1, 'home.exportword', 1, '2020-12-04 05:31:55'),
(106, 1, 'home.exportword', 1, '2020-12-04 05:35:21'),
(107, 1, 'home.exportword', 1, '2020-12-04 05:35:49'),
(108, 1, 'home.exportword', 1, '2020-12-04 05:36:20'),
(109, 1, 'home.exportword', 1, '2020-12-04 05:36:24'),
(110, 1, 'home.exportword', 1, '2020-12-04 05:40:18'),
(111, 1, 'home.exportword', 1, '2020-12-04 05:43:54'),
(112, 1, 'home.exportword', 1, '2020-12-04 05:44:35'),
(113, 1, 'home.show', 1, '2020-12-04 05:52:31'),
(114, 1, 'home.show', 1, '2020-12-04 05:52:31'),
(115, 1, 'home.show', 1, '2020-12-04 05:53:00'),
(116, 1, 'home.show', 1, '2020-12-04 05:53:39'),
(117, 1, 'home.show', 1, '2020-12-04 05:54:21'),
(118, 1, 'home.show', 1, '2020-12-04 05:54:36'),
(119, 1, 'home.show', 1, '2020-12-04 05:54:42'),
(120, 1, 'home.show', 1, '2020-12-04 05:55:22'),
(121, 1, 'home.show', 1, '2020-12-04 05:56:32'),
(122, 1, 'home.show', 1, '2020-12-04 05:56:50'),
(123, 1, 'home.show', 1, '2020-12-04 05:57:19'),
(124, 1, 'home.show', 1, '2020-12-04 05:57:44'),
(125, 1, 'home.show', 1, '2020-12-04 05:58:04'),
(126, 1, 'home.show', 1, '2020-12-04 05:59:36'),
(127, 1, 'home.show', 1, '2020-12-04 05:59:49'),
(128, 1, 'home.show', 1, '2020-12-04 05:59:59'),
(129, 1, 'home.show', 1, '2020-12-04 06:00:30'),
(130, 1, 'home.show', 1, '2020-12-04 06:00:44'),
(131, 1, 'home.show', 1, '2020-12-04 06:00:57'),
(132, 1, 'home.show', 1, '2020-12-04 06:02:24'),
(133, 1, 'home.show', 1, '2020-12-04 06:02:30'),
(134, 1, 'home.show', 1, '2020-12-04 06:03:07'),
(135, 1, 'home.show', 1, '2020-12-04 06:03:26'),
(136, 1, 'home.show', 1, '2020-12-04 06:04:21'),
(137, 1, 'home.show', 1, '2020-12-04 06:05:05'),
(138, 1, 'home.show', 1, '2020-12-04 06:06:00'),
(139, 1, 'home.show', 1, '2020-12-04 06:07:21'),
(140, 1, 'home.show', 1, '2020-12-04 06:08:16'),
(141, 1, 'home.show', 1, '2020-12-04 06:09:27'),
(142, 1, 'home.show', 1, '2020-12-04 06:10:07'),
(143, 1, 'home.show', 1, '2020-12-04 06:11:11'),
(144, 1, 'home.show', 1, '2020-12-04 06:11:20'),
(145, 1, 'home.show', 1, '2020-12-04 06:12:21'),
(146, 1, 'home.show', 1, '2020-12-04 06:13:15'),
(147, 1, 'home.show', 1, '2020-12-04 06:16:11'),
(148, 1, 'home.show', 1, '2020-12-04 06:22:53'),
(149, 1, 'home.show', 1, '2020-12-04 06:23:04'),
(150, 1, 'home.show', 1, '2020-12-04 06:23:13'),
(151, 1, 'home.exportword', 1, '2020-12-04 06:23:17'),
(152, 1, 'home.exportword', 1, '2020-12-04 06:23:39'),
(153, 1, 'home.exportword', 1, '2020-12-04 06:23:47'),
(154, 1, 'home.exportword', 1, '2020-12-04 06:23:54'),
(155, 1, 'home.exportword', 1, '2020-12-04 06:23:57'),
(156, 1, 'home.exportword', 1, '2020-12-04 06:24:05'),
(157, 1, 'home.exportword', 1, '2020-12-04 06:24:11'),
(158, 1, 'home.show', 1, '2020-12-04 06:25:47'),
(159, 1, 'home.exportword', 1, '2020-12-04 06:25:51'),
(160, 1, 'home.exportword', 1, '2020-12-04 06:28:10'),
(161, 1, 'home.exportword', 1, '2020-12-04 06:33:44'),
(162, 1, 'home.show', 1, '2020-12-04 06:35:37'),
(163, 1, 'home.show', 1, '2020-12-04 06:35:53'),
(164, 1, 'home.show', 1, '2020-12-04 06:40:18'),
(165, 1, 'home.show', 1, '2020-12-04 06:41:06'),
(166, 1, 'home.show', 1, '2020-12-04 06:41:18'),
(167, 1, 'home.show', 1, '2020-12-04 06:41:27'),
(168, 1, 'home.show', 1, '2020-12-04 06:43:10'),
(169, 1, 'home.show', 1, '2020-12-04 06:44:51'),
(170, 1, 'home.exportword', 1, '2020-12-04 06:45:10'),
(171, 1, 'home.show', 1, '2020-12-04 06:46:05'),
(172, 1, 'home.show', 1, '2020-12-04 06:47:48'),
(173, 1, 'home.exportword', 1, '2020-12-04 06:47:54'),
(174, 1, 'home.show', 1, '2020-12-04 06:48:24'),
(175, 1, 'home.show', 1, '2020-12-04 06:48:35'),
(176, 1, 'home.exportword', 1, '2020-12-04 06:48:41'),
(177, 1, 'home.show', 1, '2020-12-04 06:55:18'),
(178, 1, 'auth.logout', 1, '2020-12-04 06:55:24'),
(179, 1, 'auth.login', 1, '2020-12-04 06:55:29'),
(180, 1, 'home.show', 1, '2020-12-04 06:55:29'),
(181, 1, 'home.show', 1, '2020-12-04 06:58:46'),
(182, 1, 'home.show', 1, '2020-12-04 06:58:51'),
(183, 1, 'home.show', 1, '2020-12-04 06:59:06'),
(184, 1, 'home.show', 1, '2020-12-04 07:01:04'),
(185, 1, 'home.show', 1, '2020-12-04 07:03:04'),
(186, 1, 'home.exportword', 1, '2020-12-04 07:03:21'),
(187, 1, 'home.show', 1, '2020-12-04 07:04:48'),
(188, 1, 'home.show', 1, '2020-12-04 07:05:16'),
(189, 1, 'home.exportword', 1, '2020-12-04 07:05:21'),
(190, 1, 'home.exportword', 1, '2020-12-04 07:06:49'),
(191, 1, 'home.show', 1, '2020-12-04 07:06:50'),
(192, 1, 'home.exportword', 1, '2020-12-04 07:06:55'),
(193, 1, 'auth.login', 1, '2020-12-04 07:07:28'),
(194, 1, 'home.show', 1, '2020-12-04 07:07:28'),
(195, 1, 'home.show', 1, '2020-12-04 07:15:21'),
(196, 1, 'home.exportword', 1, '2020-12-04 07:15:28'),
(197, 1, 'home.show', 1, '2020-12-04 07:15:57'),
(198, 1, 'home.exportword', 1, '2020-12-04 07:16:02'),
(199, 1, 'home.show', 1, '2020-12-04 07:17:04'),
(200, 1, 'home.show', 1, '2020-12-04 07:18:01'),
(201, 1, 'home.exportword', 1, '2020-12-04 07:18:04'),
(202, 1, 'home.exportword', 1, '2020-12-04 07:20:55'),
(203, 1, 'home.exportword', 1, '2020-12-04 07:21:14'),
(204, 1, 'home.exportword', 1, '2020-12-04 07:21:17'),
(205, 1, 'home.exportword', 1, '2020-12-04 07:21:45'),
(206, 1, 'home.show', 1, '2020-12-04 07:21:51'),
(207, 1, 'home.exportword', 1, '2020-12-04 07:21:56'),
(208, 1, 'home.show', 1, '2020-12-04 07:22:31'),
(209, 1, 'home.show', 1, '2020-12-04 07:22:39'),
(210, 1, 'home.exportword', 1, '2020-12-04 07:22:43'),
(211, 1, 'auth.login', 1, '2020-12-04 07:30:05'),
(212, 1, 'home.show', 1, '2020-12-04 07:30:05'),
(213, 1, 'home.show', 1, '2020-12-04 07:31:47'),
(214, 1, 'home.exportword', 1, '2020-12-04 07:33:24'),
(215, 1, 'home.exportword', 1, '2020-12-04 07:36:31'),
(216, 1, 'home.exportword', 1, '2020-12-04 07:36:33'),
(217, 1, 'home.exportword', 1, '2020-12-04 07:37:16'),
(218, 1, 'home.exportword', 1, '2020-12-04 07:37:22'),
(219, 1, 'home.exportword', 1, '2020-12-04 07:37:47'),
(220, 1, 'home.show', 1, '2020-12-04 07:38:15'),
(221, 1, 'home.show', 1, '2020-12-04 07:38:25'),
(222, 1, 'home.exportword', 1, '2020-12-04 07:38:29'),
(223, 1, 'home.show', 1, '2020-12-04 07:38:57'),
(224, 1, 'home.show', 1, '2020-12-04 07:39:04'),
(225, 1, 'home.show', 1, '2020-12-04 07:39:11'),
(226, 1, 'home.show', 1, '2020-12-04 07:42:51'),
(227, 1, 'home.show', 1, '2020-12-04 07:47:03'),
(228, 1, 'auth.logout', 1, '2020-12-04 07:49:04'),
(229, 1, 'auth.login', 1, '2020-12-04 07:51:53'),
(230, 1, 'home.show', 1, '2020-12-04 07:51:53'),
(231, 1, 'home.exportword', 1, '2020-12-04 07:52:04'),
(232, 1, 'home.exportword', 1, '2020-12-04 07:52:31'),
(233, 1, 'home.exportword', 1, '2020-12-04 07:52:41'),
(234, 1, 'home.exportword', 1, '2020-12-04 07:53:03'),
(235, 1, 'home.exportword', 1, '2020-12-04 07:53:18'),
(236, 1, 'home.exportword', 1, '2020-12-04 16:10:38'),
(237, 1, 'home.exportword', 1, '2020-12-04 16:11:31'),
(238, 1, 'home.show', 1, '2020-12-04 16:12:10'),
(239, 1, 'home.exportword', 1, '2020-12-04 16:12:15'),
(240, 1, 'home.show', 1, '2020-12-04 16:15:39'),
(241, 1, 'home.exportword', 1, '2020-12-04 16:15:51'),
(242, 1, 'home.exportword', 1, '2020-12-04 16:15:52'),
(243, 1, 'home.show', 1, '2020-12-04 16:16:05'),
(244, 1, 'home.exportword', 1, '2020-12-04 16:16:10'),
(245, 1, 'home.exportword', 1, '2020-12-04 16:16:11'),
(246, 1, 'home.show', 1, '2020-12-04 16:16:32'),
(247, 1, 'home.show', 1, '2020-12-04 16:16:41'),
(248, 1, 'auth.login', 1, '2020-12-23 19:55:17'),
(249, 1, 'home.show', 1, '2020-12-23 19:55:17'),
(250, 1, 'home.show', 1, '2020-12-23 19:55:24'),
(251, 1, 'mainapi.items', 1, '2020-12-23 19:55:56'),
(252, 1, 'api.listening', 1, '2020-12-23 19:57:12'),
(253, 1, 'api.listening', 1, '2020-12-23 20:00:31'),
(254, 1, 'api.listening', 1, '2020-12-23 20:01:49'),
(255, 1, 'api.listening', 1, '2020-12-23 20:04:18'),
(256, 1, 'api.listening', 1, '2020-12-23 20:05:00'),
(257, 1, 'api.listening', 1, '2020-12-23 20:05:54'),
(258, 1, 'api.listening', 1, '2020-12-23 20:09:34'),
(259, 1, 'home.show', 1, '2020-12-23 20:11:01'),
(260, 1, 'auth.logout', 1, '2020-12-23 20:11:06'),
(261, 1, 'auth.login', 1, '2020-12-23 20:12:14'),
(262, 1, 'home.show', 1, '2020-12-23 20:12:14'),
(263, 1, 'auth.logout', 1, '2020-12-23 20:12:30'),
(264, 1, 'auth.login', 1, '2020-12-23 20:12:41'),
(265, 1, 'home.show', 1, '2020-12-23 20:12:41'),
(266, 1, 'auth.logout', 1, '2020-12-23 20:13:49'),
(267, 1, 'auth.login', 1, '2020-12-23 20:14:01'),
(268, 1, 'home.show', 1, '2020-12-23 20:14:01'),
(269, 1, 'auth.logout', 1, '2020-12-23 20:14:03'),
(270, 1, 'auth.login', 1, '2020-12-23 20:14:16'),
(271, 1, 'home.show', 1, '2020-12-23 20:14:16'),
(272, 1, 'auth.logout', 1, '2020-12-23 20:14:30'),
(273, 1, 'auth.login', 1, '2021-04-04 06:59:55'),
(274, 1, 'home.show', 1, '2021-04-04 06:59:55'),
(275, 1, 'home.show', 1, '2021-04-04 07:00:43'),
(276, 1, 'home.show', 1, '2021-04-04 07:00:58'),
(277, 1, 'home.show', 1, '2021-04-04 07:02:32'),
(278, 1, 'home.show', 1, '2021-04-04 07:02:38'),
(279, 1, 'home.show', 1, '2021-04-04 07:03:07'),
(280, 1, 'home.show', 1, '2021-04-04 07:05:08'),
(281, 1, 'home.show', 1, '2021-04-04 07:05:21'),
(282, 1, 'home.show', 1, '2021-04-04 07:05:44'),
(283, 1, 'home.show', 1, '2021-04-04 07:06:27'),
(284, 1, 'home.show', 1, '2021-04-04 07:06:51'),
(285, 1, 'home.show', 1, '2021-04-04 07:07:45'),
(286, 1, 'home.show', 1, '2021-04-04 07:28:37'),
(287, 1, 'home.show', 1, '2021-04-04 07:28:45'),
(288, 1, 'home.show', 1, '2021-04-04 07:28:55'),
(289, 1, 'home.show', 1, '2021-04-04 07:31:22'),
(290, 1, 'home.show', 1, '2021-04-04 07:32:58'),
(291, 1, 'home.show', 1, '2021-04-04 07:34:26'),
(292, 1, 'home.show', 1, '2021-04-04 07:34:44'),
(293, 1, 'home.show', 1, '2021-04-04 07:36:18'),
(294, 1, 'home.show', 1, '2021-04-04 07:36:59'),
(295, 1, 'home.show', 1, '2021-04-04 07:37:06'),
(296, 1, 'home.show', 1, '2021-04-04 07:38:45'),
(297, 1, 'home.show', 1, '2021-04-04 07:40:08'),
(298, 1, 'home.show', 1, '2021-04-04 07:41:06'),
(299, 1, 'home.show', 1, '2021-04-04 07:41:36'),
(300, 1, 'listening.dashboard', 1, '2021-04-04 07:50:57'),
(301, 1, 'listening.dashboard', 1, '2021-04-04 07:51:56'),
(302, 1, 'listening.dashboard', 1, '2021-04-04 07:52:08'),
(303, 1, 'listening.dashboard', 1, '2021-04-04 07:52:20'),
(304, 1, 'home.show', 1, '2021-04-04 07:56:43'),
(305, 1, 'listening.dashboard', 1, '2021-04-04 07:56:48'),
(306, 1, 'listening.dashboard', 1, '2021-04-04 07:58:44'),
(307, 1, 'listening.dashboard', 1, '2021-04-04 07:58:48'),
(308, 1, 'listening.dashboard', 1, '2021-04-04 07:59:16'),
(309, 1, 'listening.dashboard', 1, '2021-04-04 07:59:41'),
(310, 1, 'auth.login', 1, '2021-04-04 09:39:52'),
(311, 1, 'home.show', 1, '2021-04-04 09:39:52'),
(312, 1, 'listening.dashboard', 1, '2021-04-04 09:39:56'),
(313, 1, 'listening.dashboard', 1, '2021-04-04 09:46:12'),
(314, 1, 'listening.dashboard', 1, '2021-04-04 09:48:09'),
(315, 1, 'listening.dashboard', 1, '2021-04-04 09:49:06'),
(316, 1, 'listening.dashboard', 1, '2021-04-04 09:49:30'),
(317, 1, 'listening.dashboard', 1, '2021-04-04 09:50:31'),
(318, 1, 'listening.dashboard', 1, '2021-04-04 12:49:51'),
(319, 1, 'listening.dashboard', 1, '2021-04-04 12:50:15'),
(320, 1, 'listening.dashboard', 1, '2021-04-04 12:55:49'),
(321, 1, 'listening.dashboard', 1, '2021-04-04 12:56:05'),
(322, 1, 'listening.dashboard', 1, '2021-04-04 12:56:30'),
(323, 1, 'listening.dashboard', 1, '2021-04-04 12:56:40'),
(324, 1, 'listening.dashboard', 1, '2021-04-04 13:04:16'),
(325, 1, 'listening.dashboard', 1, '2021-04-04 13:05:38'),
(326, 1, 'listening.dashboard', 1, '2021-04-04 13:06:53'),
(327, 1, 'listening.dashboard', 1, '2021-04-04 13:07:41'),
(328, 1, 'listening.dashboard', 1, '2021-04-04 13:08:32'),
(329, 1, 'listening.dashboard', 1, '2021-04-04 13:21:58'),
(330, 1, 'listening.dashboard', 1, '2021-04-04 13:22:25'),
(331, 1, 'listening.dashboard', 1, '2021-04-04 13:33:50'),
(332, 1, 'listening.dashboard', 1, '2021-04-04 13:37:46'),
(333, 1, 'listening.dashboard', 1, '2021-04-04 13:38:00'),
(334, 1, 'listening.dashboard', 1, '2021-04-04 13:38:09'),
(335, 1, 'listening.dashboard', 1, '2021-04-04 13:38:42'),
(336, 1, 'home.show', 1, '2021-04-04 13:39:13'),
(337, 1, 'listening.dashboard', 1, '2021-04-04 13:42:02'),
(338, 1, 'home.show', 1, '2021-04-04 14:59:09'),
(339, 1, 'home.show', 1, '2021-04-04 15:01:03'),
(340, 1, 'listening.dashboard', 1, '2021-04-04 15:02:52'),
(341, 1, 'home.show', 1, '2021-04-04 15:02:53'),
(342, 1, 'home.show', 1, '2021-04-04 15:03:19'),
(343, 1, 'home.show', 1, '2021-04-04 15:04:16'),
(344, 1, 'home.show', 1, '2021-04-04 15:04:33'),
(345, 1, 'home.show', 1, '2021-04-04 15:06:59'),
(346, 1, 'home.show', 1, '2021-04-04 15:07:19'),
(347, 1, 'home.show', 1, '2021-04-04 15:07:50'),
(348, 1, 'listening.dashboard', 1, '2021-04-04 15:09:02'),
(349, 1, 'listening.dashboard', 1, '2021-04-04 15:09:16'),
(350, 1, 'listening.dashboard', 1, '2021-04-04 15:09:49'),
(351, 1, 'listening.dashboard', 1, '2021-04-04 15:10:00'),
(352, 1, 'listening.dashboard', 1, '2021-04-04 15:10:03'),
(353, 1, 'listening.dashboard', 1, '2021-04-04 15:11:02'),
(354, 1, 'listening.dashboard', 1, '2021-04-04 15:11:21'),
(355, 1, 'listening.dashboard', 1, '2021-04-04 15:11:33'),
(356, 1, 'listening.dashboard', 1, '2021-04-04 15:11:52'),
(357, 1, 'listening.dashboard', 1, '2021-04-04 15:12:16'),
(358, 1, 'listening.dashboard', 1, '2021-04-04 15:13:07'),
(359, 1, 'listening.dashboard', 1, '2021-04-04 15:14:50'),
(360, 1, 'listening.dashboard', 1, '2021-04-04 15:15:14'),
(361, 1, 'listening.dashboard', 1, '2021-04-04 15:15:19'),
(362, 1, 'listening.dashboard', 1, '2021-04-04 15:15:55'),
(363, 1, 'listening.dashboard', 1, '2021-04-04 15:19:49'),
(364, 1, 'listening.dashboard', 1, '2021-04-04 15:20:37'),
(365, 1, 'listening.dashboard', 1, '2021-04-04 15:20:51'),
(366, 1, 'listening.dashboard', 1, '2021-04-04 15:21:28'),
(367, 1, 'listening.dashboard', 1, '2021-04-04 15:22:07'),
(368, 1, 'listening.dashboard', 1, '2021-04-04 15:25:05'),
(369, 1, 'listening.dashboard', 1, '2021-04-04 15:25:12'),
(370, 1, 'listening.dashboard', 1, '2021-04-04 15:26:18'),
(371, 1, 'listening.dashboard', 1, '2021-04-04 15:26:26'),
(372, 1, 'listening.dashboard', 1, '2021-04-04 15:27:12'),
(373, 1, 'listening.dashboard', 1, '2021-04-04 15:28:04'),
(374, 1, 'listening.dashboard', 1, '2021-04-04 15:28:25'),
(375, 1, 'listening.dashboard', 1, '2021-04-04 15:28:43'),
(376, 1, 'listening.dashboard', 1, '2021-04-04 15:28:48'),
(377, 1, 'listening.dashboard', 1, '2021-04-04 15:28:53'),
(378, 1, 'listening.dashboard', 1, '2021-04-04 15:29:19'),
(379, 1, 'listening.dashboard', 1, '2021-04-04 15:29:34'),
(380, 1, 'listening.dashboard', 1, '2021-04-04 15:30:02'),
(381, 1, 'listening.dashboard', 1, '2021-04-04 15:30:22'),
(382, 1, 'listening.dashboard', 1, '2021-04-04 15:31:10'),
(383, 1, 'listening.dashboard', 1, '2021-04-04 15:31:22'),
(384, 1, 'listening.dashboard', 1, '2021-04-04 15:32:12'),
(385, 1, 'listening.dashboard', 1, '2021-04-04 15:37:41'),
(386, 1, 'listening.weekform', 1, '2021-04-04 15:42:18'),
(387, 1, 'listening.weekform', 1, '2021-04-04 15:42:43'),
(388, 1, 'listening.weekform', 1, '2021-04-04 15:43:42'),
(389, 1, 'listening.weekform', 1, '2021-04-04 15:45:11'),
(390, 1, 'listening.weekform', 1, '2021-04-04 15:46:10'),
(391, 1, 'listening.dashboard', 1, '2021-04-04 15:46:32'),
(392, 1, 'listening.weekform', 1, '2021-04-04 15:47:30'),
(393, 1, 'listening.dashboard', 1, '2021-04-04 15:48:26'),
(394, 1, 'listening.weekform', 1, '2021-04-04 15:48:30'),
(395, 1, 'listening.weekform', 1, '2021-04-04 15:48:42'),
(396, 1, 'listening.weekform', 1, '2021-04-04 15:49:25'),
(397, 1, 'auth.login', 1, '2021-04-05 09:15:44'),
(398, 1, 'home.show', 1, '2021-04-05 09:15:44'),
(399, 1, 'home.show', 1, '2021-04-05 09:17:15'),
(400, 1, 'home.show', 1, '2021-04-05 09:17:40'),
(401, 1, 'home.show', 1, '2021-04-05 09:18:10'),
(402, 1, 'home.show', 1, '2021-04-05 09:28:11'),
(403, 1, 'listening.dashboard', 1, '2021-04-05 09:28:31'),
(404, 1, 'home.show', 1, '2021-04-05 09:28:46'),
(405, 1, 'home.show', 1, '2021-04-05 09:38:17'),
(406, 1, 'auth.login', 1, '2021-04-05 10:05:49'),
(407, 1, 'home.show', 1, '2021-04-05 10:05:49'),
(408, 1, 'home.show', 1, '2021-04-05 10:07:10'),
(409, 1, 'home.show', 1, '2021-04-05 10:11:08'),
(410, 1, 'home.show', 1, '2021-04-05 10:40:03'),
(411, 1, 'home.show', 1, '2021-04-05 10:41:09'),
(412, 1, 'home.show', 1, '2021-04-05 10:42:11'),
(413, 1, 'listening.dashboard', 1, '2021-04-05 10:48:10'),
(414, 1, 'listening.dashboard', 1, '2021-04-05 10:48:53'),
(415, 1, 'listening.dashboard', 1, '2021-04-05 10:48:57'),
(416, 1, 'listening.dashboard', 1, '2021-04-05 10:49:10'),
(417, 1, 'listening.dashboard', 1, '2021-04-05 10:49:25'),
(418, 1, 'listening.dashboard', 1, '2021-04-05 10:49:39'),
(419, 1, 'listening.dashboard', 1, '2021-04-05 10:49:47'),
(420, 1, 'listening.dashboard', 1, '2021-04-05 10:50:12'),
(421, 1, 'listening.dashboard', 1, '2021-04-05 10:50:24'),
(422, 1, 'listening.dashboard', 1, '2021-04-05 10:50:42'),
(423, 1, 'listening.dashboard', 1, '2021-04-05 11:02:00'),
(424, 1, 'listening.dashboard', 1, '2021-04-05 11:04:52'),
(425, 1, 'listening.dashboard', 1, '2021-04-05 11:05:05'),
(426, 1, 'listening.dashboard', 1, '2021-04-05 11:06:38'),
(427, 1, 'listening.dashboard', 1, '2021-04-05 11:06:47'),
(428, 1, 'listening.dashboard', 1, '2021-04-05 11:07:11'),
(429, 1, 'listening.dashboard', 1, '2021-04-05 11:07:21'),
(430, 1, 'listening.dashboard', 1, '2021-04-05 11:07:39'),
(431, 1, 'listening.dashboard', 1, '2021-04-05 11:07:40'),
(432, 1, 'listening.dashboard', 1, '2021-04-05 11:13:15'),
(433, 1, 'listening.dashboard', 1, '2021-04-05 11:13:18'),
(434, 1, 'listening.dashboard', 1, '2021-04-05 11:14:29'),
(435, 1, 'listening.dashboard', 1, '2021-04-05 11:14:50'),
(436, 1, 'listening.dashboard', 1, '2021-04-05 11:15:04'),
(437, 1, 'listening.dashboard', 1, '2021-04-05 11:15:26'),
(438, 1, 'listening.dashboard', 1, '2021-04-05 11:15:37'),
(439, 1, 'listening.dashboard', 1, '2021-04-05 11:17:01'),
(440, 1, 'listening.dashboard', 1, '2021-04-05 11:17:12'),
(441, 1, 'listening.dashboard', 1, '2021-04-05 11:18:12'),
(442, 1, 'listening.dashboard', 1, '2021-04-05 11:18:27'),
(443, 1, 'listening.dashboard', 1, '2021-04-05 11:19:20'),
(444, 1, 'listening.dashboard', 1, '2021-04-05 11:19:25'),
(445, 1, 'listening.dashboard', 1, '2021-04-05 11:19:35'),
(446, 1, 'listening.dashboard', 1, '2021-04-05 11:19:51'),
(447, 1, 'listening.dashboard', 1, '2021-04-05 11:20:12'),
(448, 1, 'listening.dashboard', 1, '2021-04-05 11:20:58'),
(449, 1, 'listening.dashboard', 1, '2021-04-05 11:21:17'),
(450, 1, 'listening.dashboard', 1, '2021-04-05 11:21:47'),
(451, 1, 'listening.dashboard', 1, '2021-04-05 11:21:56'),
(452, 1, 'listening.dashboard', 1, '2021-04-05 11:23:47'),
(453, 1, 'listening.dashboard', 1, '2021-04-05 11:24:05'),
(454, 1, 'listening.dashboard', 1, '2021-04-05 11:29:06'),
(455, 1, 'listening.dashboard', 1, '2021-04-05 11:29:21'),
(456, 1, 'listening.weekform', 1, '2021-04-05 11:29:41'),
(457, 1, 'listening.weekform', 1, '2021-04-05 11:30:16'),
(458, 1, 'listening.weekform', 1, '2021-04-05 11:30:32'),
(459, 1, 'listening.weekform', 1, '2021-04-05 11:30:52'),
(460, 1, 'listening.weekform', 1, '2021-04-05 11:31:23'),
(461, 1, 'listening.weekform', 1, '2021-04-05 11:33:02'),
(462, 1, 'listening.weekform', 1, '2021-04-05 11:33:22'),
(463, 1, 'listening.weekform', 1, '2021-04-05 11:33:31'),
(464, 1, 'listening.weekform', 1, '2021-04-05 11:33:39'),
(465, 1, 'listening.weekform', 1, '2021-04-05 11:33:48'),
(466, 1, 'listening.weekform', 1, '2021-04-05 11:36:18'),
(467, 1, 'listening.weekform', 1, '2021-04-05 11:36:28'),
(468, 1, 'listening.weekform', 1, '2021-04-05 11:36:44'),
(469, 1, 'listening.weekform', 1, '2021-04-05 11:36:54'),
(470, 1, 'listening.weekform', 1, '2021-04-05 11:37:01'),
(471, 1, 'listening.weekform', 1, '2021-04-05 11:45:05'),
(472, 1, 'listening.weekform', 1, '2021-04-05 11:45:45'),
(473, 1, 'listening.weekform', 1, '2021-04-05 11:46:07'),
(474, 1, 'listening.weekform', 1, '2021-04-05 11:46:13'),
(475, 1, 'listening.weekform', 1, '2021-04-05 11:46:16'),
(476, 1, 'listening.weekform', 1, '2021-04-05 11:47:22'),
(477, 1, 'listening.weekform', 1, '2021-04-05 11:47:29'),
(478, 1, 'listening.weekform', 1, '2021-04-05 11:47:55'),
(479, 1, 'listening.weekform', 1, '2021-04-05 11:48:20'),
(480, 1, 'listening.weekform', 1, '2021-04-05 11:48:28'),
(481, 1, 'listening.weekform', 1, '2021-04-05 11:48:38'),
(482, 1, 'listening.weekform', 1, '2021-04-05 11:48:46'),
(483, 1, 'listening.weekform', 1, '2021-04-05 11:49:01'),
(484, 1, 'listening.weekform', 1, '2021-04-05 11:49:08'),
(485, 1, 'listening.weekform', 1, '2021-04-05 11:51:03'),
(486, 1, 'listening.weekform', 1, '2021-04-05 11:51:13'),
(487, 1, 'listening.weekform', 1, '2021-04-05 11:51:21'),
(488, 1, 'listening.weekform', 1, '2021-04-05 11:51:28'),
(489, 1, 'listening.weekform', 1, '2021-04-05 11:51:33'),
(490, 1, 'listening.weekform', 1, '2021-04-05 11:51:41'),
(491, 1, 'listening.weekform', 1, '2021-04-05 11:51:52'),
(492, 1, 'listening.weekform', 1, '2021-04-05 11:52:01'),
(493, 1, 'listening.weekform', 1, '2021-04-05 11:52:06'),
(494, 1, 'listening.weekform', 1, '2021-04-05 11:53:00'),
(495, 1, 'listening.weekform', 1, '2021-04-05 11:53:10'),
(496, 1, 'listening.weekform', 1, '2021-04-05 11:53:17'),
(497, 1, 'listening.weekform', 1, '2021-04-05 11:56:06'),
(498, 1, 'listening.weekform', 1, '2021-04-05 11:56:48'),
(499, 1, 'listening.dashboard', 1, '2021-04-05 11:57:02'),
(500, 1, 'listening.weekform', 1, '2021-04-05 11:57:04'),
(501, 1, 'listening.weekform', 1, '2021-04-05 11:58:17'),
(502, 1, 'listening.weekform', 1, '2021-04-05 11:58:31'),
(503, 1, 'listening.weekform', 1, '2021-04-05 11:58:37'),
(504, 1, 'listening.weekform', 1, '2021-04-05 11:58:47'),
(505, 1, 'listening.weekform', 1, '2021-04-05 11:58:53'),
(506, 1, 'listening.weekform', 1, '2021-04-05 11:59:22'),
(507, 1, 'listening.weekform', 1, '2021-04-05 11:59:30'),
(508, 1, 'listening.weekform', 1, '2021-04-05 12:02:52'),
(509, 1, 'listening.weekform', 1, '2021-04-05 12:03:01'),
(510, 1, 'listening.weekform', 1, '2021-04-05 12:03:57'),
(511, 1, 'listening.weekform', 1, '2021-04-05 12:04:57'),
(512, 1, 'listening.weekform', 1, '2021-04-05 12:05:33'),
(513, 1, 'listening.weekform', 1, '2021-04-05 12:06:48'),
(514, 1, 'listening.weekform', 1, '2021-04-05 12:06:54'),
(515, 1, 'listening.weekform', 1, '2021-04-05 12:07:55'),
(516, 1, 'listening.weekform', 1, '2021-04-05 12:08:03'),
(517, 1, 'listening.weekform', 1, '2021-04-05 12:08:07'),
(518, 1, 'listening.weekform', 1, '2021-04-05 12:08:59'),
(519, 1, 'listening.dashboard', 1, '2021-04-05 12:09:00'),
(520, 1, 'listening.weekform', 1, '2021-04-05 12:09:02'),
(521, 1, 'listening.weekform', 1, '2021-04-05 12:09:07'),
(522, 1, 'listening.weekform', 1, '2021-04-05 12:09:40'),
(523, 1, 'listening.dashboard', 1, '2021-04-05 12:09:41'),
(524, 1, 'listening.weekform', 1, '2021-04-05 12:09:43'),
(525, 1, 'listening.weekform', 1, '2021-04-05 12:10:14'),
(526, 1, 'listening.weekform', 1, '2021-04-05 12:10:15'),
(527, 1, 'listening.weekform', 1, '2021-04-05 14:02:24'),
(528, 1, 'auth.login', 1, '2021-04-05 20:28:00'),
(529, 1, 'home.show', 1, '2021-04-05 20:28:00'),
(530, 1, 'listening.dashboard', 1, '2021-04-05 20:28:52'),
(531, 1, 'listening.dashboard', 1, '2021-04-05 20:31:51'),
(532, 1, 'listening.dashboard', 1, '2021-04-05 20:31:53'),
(533, 1, 'listening.dashboard', 1, '2021-04-05 20:33:50'),
(534, 1, 'listening.dashboard', 1, '2021-04-05 20:34:17'),
(535, 1, 'listening.weeknew', 1, '2021-04-05 20:36:33'),
(536, 1, 'listening.weeknew', 1, '2021-04-05 20:36:41'),
(537, 1, 'listening.weeknew', 1, '2021-04-05 20:41:59'),
(538, 1, 'listening.weeknew', 1, '2021-04-05 20:42:25'),
(539, 1, 'listening.weeknew', 1, '2021-04-05 20:42:26'),
(540, 1, 'listening.weeknew', 1, '2021-04-05 20:43:40'),
(541, 1, 'listening.weeknew', 1, '2021-04-05 20:44:53'),
(542, 1, 'listening.weeknew', 1, '2021-04-05 20:45:06'),
(543, 1, 'listening.weeknew', 1, '2021-04-05 20:45:12'),
(544, 1, 'listening.weeknew', 1, '2021-04-05 20:45:18'),
(545, 1, 'listening.weeknew', 1, '2021-04-05 20:45:20'),
(546, 1, 'listening.weeknew', 1, '2021-04-05 20:45:23'),
(547, 1, 'listening.weeknew', 1, '2021-04-05 20:45:24'),
(548, 1, 'listening.weeknew', 1, '2021-04-05 20:45:28'),
(549, 1, 'listening.weeknew', 1, '2021-04-05 20:45:38'),
(550, 1, 'listening.weeknew', 1, '2021-04-05 20:45:50'),
(551, 1, 'listening.weeknew', 1, '2021-04-05 20:46:07'),
(552, 1, 'listening.weeknew', 1, '2021-04-05 20:46:12'),
(553, 1, 'listening.weeknew', 1, '2021-04-05 20:46:14'),
(554, 1, 'listening.weeknew', 1, '2021-04-05 20:46:17'),
(555, 1, 'listening.weeknew', 1, '2021-04-05 20:46:21'),
(556, 1, 'listening.weeknew', 1, '2021-04-05 20:46:29'),
(557, 1, 'listening.weeknew', 1, '2021-04-05 20:46:36'),
(558, 1, 'listening.weeknew', 1, '2021-04-05 20:47:04'),
(559, 1, 'listening.weeknew', 1, '2021-04-05 20:47:10'),
(560, 1, 'listening.weeknew', 1, '2021-04-05 20:47:18'),
(561, 1, 'listening.weeknew', 1, '2021-04-05 20:47:20'),
(562, 1, 'listening.weeknew', 1, '2021-04-05 20:47:35'),
(563, 1, 'listening.weeknew', 1, '2021-04-05 20:47:40'),
(564, 1, 'listening.weeknew', 1, '2021-04-05 20:49:08'),
(565, 1, 'listening.weeknew', 1, '2021-04-05 20:49:45'),
(566, 1, 'listening.weeknew', 1, '2021-04-05 20:52:25'),
(567, 1, 'listening.weeknew', 1, '2021-04-05 20:54:09'),
(568, 1, 'listening.weeknew', 1, '2021-04-05 20:54:56'),
(569, 1, 'listening.weeknew', 1, '2021-04-05 20:54:57'),
(570, 1, 'auth.logout', 1, '2021-04-05 20:54:59'),
(571, 1, 'auth.login', 1, '2021-04-05 20:55:03'),
(572, 1, 'home.show', 1, '2021-04-05 20:55:03'),
(573, 1, 'listening.dashboard', 1, '2021-04-05 21:16:02'),
(574, 1, 'listening.weekform', 1, '2021-04-05 21:16:05'),
(575, 1, 'listening.weekform', 1, '2021-04-05 21:16:08'),
(576, 1, 'listening.weekform', 1, '2021-04-05 21:16:42'),
(577, 1, 'listening.weekform', 1, '2021-04-05 21:16:45'),
(578, 1, 'listening.weekform', 1, '2021-04-05 21:27:21'),
(579, 1, 'listening.dashboard', 1, '2021-04-05 21:27:26'),
(580, 1, 'listening.weeknew', 1, '2021-04-05 21:27:27'),
(581, 1, 'listening.weeknew', 1, '2021-04-05 21:27:42'),
(582, 1, 'listening.weeknew', 1, '2021-04-05 21:28:00'),
(583, 1, 'listening.weeknew', 1, '2021-04-05 21:28:27'),
(584, 1, 'listening.weeknew', 1, '2021-04-05 21:28:53'),
(585, 1, 'listening.weeknew', 1, '2021-04-05 21:29:31'),
(586, 1, 'listening.dashboard', 1, '2021-04-05 21:29:59'),
(587, 1, 'listening.weeknew', 1, '2021-04-05 21:33:58'),
(588, 1, 'listening.weeknew', 1, '2021-04-05 21:34:10'),
(589, 1, 'listening.weeknew', 1, '2021-04-05 21:34:24'),
(590, 1, 'listening.weeknew', 1, '2021-04-05 21:34:29'),
(591, 1, 'listening.weeknew', 1, '2021-04-06 00:27:23'),
(592, 1, 'auth.login', 1, '2021-04-06 00:27:37'),
(593, 1, 'home.show', 1, '2021-04-06 00:27:37'),
(594, 1, 'listening.dashboard', 1, '2021-04-06 00:27:48'),
(595, 1, 'listening.dashboard', 1, '2021-04-06 00:37:42'),
(596, 1, 'listening.dashboard', 1, '2021-04-06 01:48:18'),
(597, 1, 'listening.weeknew', 1, '2021-04-06 01:55:58'),
(598, 1, 'listening.weeknew', 1, '2021-04-06 01:56:22'),
(599, 1, 'listening.weeknew', 1, '2021-04-06 01:56:34'),
(600, 1, 'listening.weeknew', 1, '2021-04-06 01:56:38'),
(601, 1, 'listening.weeknew', 1, '2021-04-06 01:56:41'),
(602, 1, 'listening.weeknew', 1, '2021-04-06 01:58:01'),
(603, 1, 'listening.weeknew', 1, '2021-04-06 01:58:24'),
(604, 1, 'listening.weeknew', 1, '2021-04-06 01:58:39'),
(605, 1, 'listening.weeknew', 1, '2021-04-06 01:58:51'),
(606, 1, 'listening.weeknew', 1, '2021-04-06 01:59:32'),
(607, 1, 'listening.weeknew', 1, '2021-04-06 01:59:42'),
(608, 1, 'listening.weeknew', 1, '2021-04-06 01:59:48'),
(609, 1, 'listening.weeknew', 1, '2021-04-06 02:00:06'),
(610, 1, 'listening.weeknew', 1, '2021-04-06 02:00:25'),
(611, 1, 'listening.weeknew', 1, '2021-04-06 02:01:03'),
(612, 1, 'listening.weeknew', 1, '2021-04-06 02:01:26'),
(613, 1, 'listening.weeknew', 1, '2021-04-06 02:01:27'),
(614, 1, 'listening.weeknew', 1, '2021-04-06 02:01:31'),
(615, 1, 'listening.weeknew', 1, '2021-04-06 02:01:32'),
(616, 1, 'listening.weeknew', 1, '2021-04-06 02:02:59'),
(617, 1, 'listening.weeknew', 1, '2021-04-06 02:03:01'),
(618, 1, 'listening.weeknew', 1, '2021-04-06 02:03:10'),
(619, 1, 'listening.weeknew', 1, '2021-04-06 02:03:19'),
(620, 1, 'listening.weeknew', 1, '2021-04-06 02:03:22'),
(621, 1, 'listening.weeknew', 1, '2021-04-06 02:03:27'),
(622, 1, 'listening.weeknew', 1, '2021-04-06 02:04:00'),
(623, 1, 'listening.weeknew', 1, '2021-04-06 02:04:04'),
(624, 1, 'listening.weeknew', 1, '2021-04-06 02:04:46'),
(625, 1, 'listening.weeknew', 1, '2021-04-06 02:04:54'),
(626, 1, 'listening.weeknew', 1, '2021-04-06 02:04:59'),
(627, 1, 'listening.dashboard', 1, '2021-04-06 02:05:42'),
(628, 1, 'listening.weeknew', 1, '2021-04-06 02:06:04'),
(629, 1, 'listening.weeknew', 1, '2021-04-06 02:07:41'),
(630, 1, 'listening.weeknew', 1, '2021-04-06 03:48:17'),
(631, 1, 'listening.weeknew', 1, '2021-04-06 03:49:03'),
(632, 1, 'listening.weeknew', 1, '2021-04-06 03:49:46'),
(633, 1, 'listening.weeknew', 1, '2021-04-06 03:49:49'),
(634, 1, 'listening.weeknew', 1, '2021-04-06 03:50:43'),
(635, 1, 'listening.weeknew', 1, '2021-04-06 03:50:46'),
(636, 1, 'listening.dashboard', 1, '2021-04-06 03:51:27'),
(637, 1, 'listening.weeknew', 1, '2021-04-06 03:52:23'),
(638, 1, 'listening.weeknew', 1, '2021-04-06 03:52:28'),
(639, 1, 'listening.weeknew', 1, '2021-04-06 03:53:49'),
(640, 1, 'listening.weeknew', 1, '2021-04-06 03:53:55'),
(641, 1, 'listening.weeknew', 1, '2021-04-06 03:56:27'),
(642, 1, 'listening.weeknew', 1, '2021-04-06 03:56:54'),
(643, 1, 'listening.weeknew', 1, '2021-04-06 03:56:58'),
(644, 1, 'listening.weeknew', 1, '2021-04-06 03:57:06'),
(645, 1, 'listening.weeknew', 1, '2021-04-06 03:58:23'),
(646, 1, 'listening.weeknew', 1, '2021-04-06 03:58:37'),
(647, 1, 'listening.weeknew', 1, '2021-04-06 03:59:19'),
(648, 1, 'listening.weeknew', 1, '2021-04-06 04:00:48'),
(649, 1, 'listening.weeknew', 1, '2021-04-06 04:12:40'),
(650, 1, 'listening.weeknew', 1, '2021-04-06 04:12:59'),
(651, 1, 'listening.weeknew', 1, '2021-04-06 04:13:08'),
(652, 1, 'listening.weeknew', 1, '2021-04-06 04:13:47'),
(653, 1, 'listening.dashboard', 1, '2021-04-06 04:13:55'),
(654, 1, 'listening.dashboard', 1, '2021-04-06 04:14:10'),
(655, 1, 'listening.weeknew', 1, '2021-04-06 04:14:12'),
(656, 1, 'listening.weeknew', 1, '2021-04-06 04:14:17'),
(657, 1, 'listening.weeknew', 1, '2021-04-06 04:15:07'),
(658, 1, 'home.show', 1, '2021-04-06 04:15:12'),
(659, 1, 'listening.dashboard', 1, '2021-04-06 04:15:15'),
(660, 1, 'listening.weeknew', 1, '2021-04-06 04:15:17'),
(661, 1, 'listening.weeknew', 1, '2021-04-06 04:15:33'),
(662, 1, 'listening.weeknew', 1, '2021-04-06 04:16:09'),
(663, 1, 'listening.weeknew', 1, '2021-04-06 04:16:28'),
(664, 1, 'listening.weeknew', 1, '2021-04-06 04:17:41'),
(665, 1, 'listening.weeknew', 1, '2021-04-06 04:18:22'),
(666, 1, 'listening.weeknew', 1, '2021-04-06 04:19:41'),
(667, 1, 'listening.weeknew', 1, '2021-04-06 04:19:50'),
(668, 1, 'listening.weeknew', 1, '2021-04-06 04:20:01'),
(669, 1, 'home.show', 1, '2021-04-06 04:20:05'),
(670, 1, 'listening.dashboard', 1, '2021-04-06 04:20:06'),
(671, 1, 'listening.dashboard', 1, '2021-04-06 04:20:36'),
(672, 1, 'listening.dashboard', 1, '2021-04-06 04:22:28'),
(673, 1, 'listening.dashboard', 1, '2021-04-06 05:07:24'),
(674, 1, 'home.show', 1, '2021-04-06 05:07:26'),
(675, 1, 'listening.dashboard', 1, '2021-04-06 05:07:28'),
(676, 1, 'home.show', 1, '2021-04-06 05:07:29'),
(677, 1, 'listening.dashboard', 1, '2021-04-06 05:07:30'),
(678, 1, 'listening.dashboard', 1, '2021-04-06 05:08:09'),
(679, 1, 'listening.weeknew', 1, '2021-04-06 05:08:26'),
(680, 1, 'listening.dashboard', 1, '2021-04-06 05:08:29'),
(681, 1, 'listening.weekform', 1, '2021-04-06 05:09:50'),
(682, 1, 'listening.weekform', 1, '2021-04-06 05:10:23'),
(683, 1, 'listening.dashboard', 1, '2021-04-06 05:10:25'),
(684, 1, 'listening.dashboard', 1, '2021-04-06 05:10:31'),
(685, 1, 'listening.weeknew', 1, '2021-04-06 05:10:34'),
(686, 1, 'listening.dashboard', 1, '2021-04-06 05:10:37'),
(687, 1, 'listening.dashboard', 1, '2021-04-06 05:15:29'),
(688, 1, 'listening.dashboard', 1, '2021-04-06 05:16:13'),
(689, 1, 'listening.weekform', 1, '2021-04-06 05:16:58'),
(690, 1, 'listening.weekform', 1, '2021-04-06 05:17:06'),
(691, 1, 'listening.weekform', 1, '2021-04-06 05:24:24'),
(692, 1, 'listening.weekform', 1, '2021-04-06 05:31:24'),
(693, 1, 'listening.weekform', 1, '2021-04-06 05:31:32'),
(694, 1, 'listening.weekform', 1, '2021-04-06 05:32:34'),
(695, 1, 'listening.weekform', 1, '2021-04-06 05:32:44'),
(696, 1, 'listening.dashboard', 1, '2021-04-06 05:33:21'),
(697, 1, 'listening.weeknew', 1, '2021-04-06 05:33:22'),
(698, 1, 'listening.weekform', 1, '2021-04-06 05:33:28'),
(699, 1, 'listening.weekform', 1, '2021-04-06 05:34:33'),
(700, 1, 'listening.weekform', 1, '2021-04-06 05:34:38'),
(701, 1, 'listening.dashboard', 1, '2021-04-06 05:34:45'),
(702, 1, 'listening.weeknew', 1, '2021-04-06 05:34:49'),
(703, 1, 'listening.weekform', 1, '2021-04-06 05:34:52'),
(704, 1, 'listening.dashboard', 1, '2021-04-06 05:35:00'),
(705, 1, 'listening.weeknew', 1, '2021-04-06 05:36:46'),
(706, 1, 'listening.weeknew', 1, '2021-04-06 05:36:50'),
(707, 1, 'listening.weeknew', 1, '2021-04-06 05:37:14'),
(708, 1, 'listening.weeknew', 1, '2021-04-06 05:38:26'),
(709, 1, 'listening.weeknew', 1, '2021-04-06 05:38:45'),
(710, 1, 'listening.weeknew', 1, '2021-04-06 05:38:51'),
(711, 1, 'listening.weeknew', 1, '2021-04-06 05:43:18'),
(712, 1, 'listening.weeknew', 1, '2021-04-06 05:43:25'),
(713, 1, 'listening.weeknew', 1, '2021-04-06 05:44:12'),
(714, 1, 'listening.weeknew', 1, '2021-04-06 05:44:22'),
(715, 1, 'listening.weeknew', 1, '2021-04-06 05:44:32'),
(716, 1, 'listening.weeknew', 1, '2021-04-06 05:45:26'),
(717, 1, 'listening.weeknew', 1, '2021-04-06 05:45:40'),
(718, 1, 'listening.weeknew', 1, '2021-04-06 05:45:49'),
(719, 1, 'listening.weeknew', 1, '2021-04-06 05:46:02'),
(720, 1, 'listening.weeknew', 1, '2021-04-06 05:46:28'),
(721, 1, 'listening.weeknew', 1, '2021-04-06 05:50:11'),
(722, 1, 'listening.weeknew', 1, '2021-04-06 05:50:29'),
(723, 1, 'listening.dashboard', 1, '2021-04-06 05:50:29'),
(724, 1, 'listening.dashboard', 1, '2021-04-06 05:50:35'),
(725, 1, 'listening.dashboard', 1, '2021-04-06 05:52:04'),
(726, 1, 'listening.weekform', 1, '2021-04-06 05:52:14'),
(727, 1, 'listening.dashboard', 1, '2021-04-06 05:52:22'),
(728, 1, 'listening.weekform', 1, '2021-04-06 05:52:23'),
(729, 1, 'listening.weekform', 1, '2021-04-06 05:53:13'),
(730, 1, 'listening.dashboard', 1, '2021-04-06 05:53:18'),
(731, 1, 'listening.weekform', 1, '2021-04-06 05:53:20'),
(732, 1, 'listening.weekform', 1, '2021-04-06 05:54:11'),
(733, 1, 'listening.weekform', 1, '2021-04-06 05:56:05'),
(734, 1, 'listening.weekform', 1, '2021-04-06 05:56:44'),
(735, 1, 'listening.weekform', 1, '2021-04-06 05:59:12'),
(736, 1, 'listening.dashboard', 1, '2021-04-06 05:59:14'),
(737, 1, 'listening.dashboard', 1, '2021-04-06 05:59:52'),
(738, 1, 'listening.weekform', 1, '2021-04-06 05:59:55'),
(739, 1, 'listening.weekform', 1, '2021-04-06 06:00:59'),
(740, 1, 'listening.weekform', 1, '2021-04-06 06:01:03'),
(741, 1, 'listening.weekform', 1, '2021-04-06 06:01:16'),
(742, 1, 'listening.weekform', 1, '2021-04-06 06:02:27'),
(743, 1, 'listening.weekform', 1, '2021-04-06 06:04:37'),
(744, 1, 'listening.weeks', 1, '2021-04-06 06:04:51'),
(745, 1, 'listening.dashboard', 1, '2021-04-06 06:04:56'),
(746, 1, 'listening.weeknew', 1, '2021-04-06 06:05:03'),
(747, 1, 'listening.weeknew', 1, '2021-04-06 06:05:09'),
(748, 1, 'listening.weeknew', 1, '2021-04-06 06:05:38'),
(749, 1, 'listening.dashboard', 1, '2021-04-06 06:05:38'),
(750, 1, 'listening.weekform', 1, '2021-04-06 06:05:41'),
(751, 1, 'listening.weekform', 1, '2021-04-06 06:05:45'),
(752, 1, 'listening.weekform', 1, '2021-04-06 06:06:22'),
(753, 1, 'listening.dashboard', 1, '2021-04-06 06:06:22'),
(754, 1, 'listening.weekform', 1, '2021-04-06 06:06:25'),
(755, 1, 'listening.weekform', 1, '2021-04-06 06:06:30'),
(756, 1, 'listening.dashboard', 1, '2021-04-06 06:06:30'),
(757, 1, 'listening.weekform', 1, '2021-04-06 07:09:36'),
(758, 1, 'listening.weekform', 1, '2021-04-06 07:09:39'),
(759, 1, 'listening.dashboard', 1, '2021-04-06 07:09:39'),
(760, 1, 'listening.weekform', 1, '2021-04-06 07:10:17'),
(761, 1, 'listening.weekform', 1, '2021-04-06 07:10:21'),
(762, 1, 'listening.dashboard', 1, '2021-04-06 07:10:35'),
(763, 1, 'listening.weekform', 1, '2021-04-06 07:11:01'),
(764, 1, 'listening.weekform', 1, '2021-04-06 07:11:05'),
(765, 1, 'listening.weekform', 1, '2021-04-06 07:12:04'),
(766, 1, 'listening.weekform', 1, '2021-04-06 07:12:06'),
(767, 1, 'listening.weekform', 1, '2021-04-06 07:12:09'),
(768, 1, 'listening.weekform', 1, '2021-04-06 07:12:36'),
(769, 1, 'listening.dashboard', 1, '2021-04-06 07:12:36'),
(770, 1, 'listening.weekform', 1, '2021-04-06 07:12:43'),
(771, 1, 'listening.weekform', 1, '2021-04-06 07:12:47'),
(772, 1, 'listening.dashboard', 1, '2021-04-06 07:12:47'),
(773, 1, 'listening.dashboard', 1, '2021-04-06 07:14:46'),
(774, 1, 'listening.weekform', 1, '2021-04-06 07:14:49'),
(775, 1, 'listening.dashboard', 1, '2021-04-06 07:14:51'),
(776, 1, 'listening.dashboard', 1, '2021-04-06 07:15:04'),
(777, 1, 'listening.weekform', 1, '2021-04-06 07:15:08'),
(778, 1, 'listening.dashboard', 1, '2021-04-06 07:16:09'),
(779, 1, 'listening.weekform', 1, '2021-04-06 07:20:39'),
(780, 1, 'listening.weekform', 1, '2021-04-06 07:20:42'),
(781, 1, 'listening.weekform', 1, '2021-04-06 07:21:19'),
(782, 1, 'listening.dashboard', 1, '2021-04-06 07:21:19'),
(783, 1, 'listening.weekform', 1, '2021-04-06 07:22:34'),
(784, 1, 'listening.weekform', 1, '2021-04-06 07:22:37'),
(785, 1, 'listening.dashboard', 1, '2021-04-06 07:22:37'),
(786, 1, 'listening.weekform', 1, '2021-04-06 07:23:00'),
(787, 1, 'listening.weekform', 1, '2021-04-06 07:23:02'),
(788, 1, 'listening.dashboard', 1, '2021-04-06 07:23:02'),
(789, 1, 'listening.weekform', 1, '2021-04-06 07:23:09'),
(790, 1, 'listening.weekform', 1, '2021-04-06 07:23:11'),
(791, 1, 'listening.dashboard', 1, '2021-04-06 07:23:11'),
(792, 1, 'listening.weekform', 1, '2021-04-06 07:23:15'),
(793, 1, 'listening.dashboard', 1, '2021-04-06 07:23:38'),
(794, 1, 'auth.login', 1, '2021-04-06 08:46:28'),
(795, 1, 'home.show', 1, '2021-04-06 08:46:28'),
(796, 1, 'reading.dashboard', 1, '2021-04-06 08:46:31'),
(797, 1, 'home.show', 1, '2021-04-06 08:46:33'),
(798, 1, 'listening.dashboard', 1, '2021-04-06 08:46:35'),
(799, 1, 'listening.dashboard', 1, '2021-04-06 08:49:13'),
(800, 1, 'listening.dashboard', 1, '2021-04-06 08:49:39'),
(801, 1, 'listening.dashboard', 1, '2021-04-06 08:50:14'),
(802, 1, 'listening.dashboard', 1, '2021-04-06 08:50:26'),
(803, 1, 'listening.dashboard', 1, '2021-04-06 08:50:56'),
(804, 1, 'listening.dashboard', 1, '2021-04-06 08:51:28'),
(805, 1, 'listening.dashboard', 1, '2021-04-06 09:00:33'),
(806, 1, 'listening.dashboard', 1, '2021-04-06 09:01:15'),
(807, 1, 'listening.dashboard', 1, '2021-04-06 09:01:22'),
(808, 1, 'auth.login', 1, '2021-04-06 16:29:36'),
(809, 1, 'home.show', 1, '2021-04-06 16:29:36'),
(810, 1, 'listening.dashboard', 1, '2021-04-06 16:29:38'),
(811, 1, 'listening.dashboard', 1, '2021-04-06 16:31:56'),
(812, 1, 'listening.dashboard', 1, '2021-04-06 16:33:22'),
(813, 1, 'listening.dashboard', 1, '2021-04-06 16:33:46'),
(814, 1, 'listening.dashboard', 1, '2021-04-06 16:34:22'),
(815, 1, 'listening.dashboard', 1, '2021-04-06 16:34:52'),
(816, 1, 'listening.dashboard', 1, '2021-04-06 16:36:48'),
(817, 1, 'listening.dashboard', 1, '2021-04-06 16:37:35'),
(818, 1, 'listening.dashboard', 1, '2021-04-06 16:38:17'),
(819, 1, 'listening.dashboard', 1, '2021-04-06 16:38:32'),
(820, 1, 'listening.dayform', 1, '2021-04-06 16:49:24'),
(821, 1, 'listening.dayform', 1, '2021-04-06 16:50:46'),
(822, 1, 'listening.dayform', 1, '2021-04-06 16:51:17'),
(823, 1, 'listening.dayform', 1, '2021-04-06 16:52:01'),
(824, 1, 'listening.dayform', 1, '2021-04-06 16:53:21'),
(825, 1, 'listening.dayform', 1, '2021-04-06 16:53:26'),
(826, 1, 'listening.dayform', 1, '2021-04-06 16:53:30'),
(827, 1, 'listening.dayform', 1, '2021-04-06 16:54:33'),
(828, 1, 'listening.dayform', 1, '2021-04-06 16:54:47'),
(829, 1, 'listening.dashboard', 1, '2021-04-06 16:54:47'),
(830, 1, 'listening.dayform', 1, '2021-04-06 16:54:54'),
(831, 1, 'listening.dayform', 1, '2021-04-06 16:54:57'),
(832, 1, 'listening.dashboard', 1, '2021-04-06 16:54:57'),
(833, 1, 'listening.dayform', 1, '2021-04-06 17:01:39'),
(834, 1, 'listening.dayform', 1, '2021-04-06 17:01:43'),
(835, 1, 'listening.dashboard', 1, '2021-04-06 17:01:43'),
(836, 1, 'listening.dayform', 1, '2021-04-06 17:02:17'),
(837, 1, 'listening.dayform', 1, '2021-04-06 17:02:20'),
(838, 1, 'listening.dashboard', 1, '2021-04-06 17:02:21'),
(839, 1, 'listening.dashboard', 1, '2021-04-06 17:05:24'),
(840, 1, 'listening.dashboard', 1, '2021-04-06 17:09:17'),
(841, 1, 'listening.dashboard', 1, '2021-04-06 17:09:36'),
(842, 1, 'listening.dashboard', 1, '2021-04-06 17:09:56'),
(843, 1, 'listening.dashboard', 1, '2021-04-06 17:10:07'),
(844, 1, 'listening.dashboard', 1, '2021-04-06 17:10:19'),
(845, 1, 'listening.dashboard', 1, '2021-04-06 17:10:24'),
(846, 1, 'listening.dashboard', 1, '2021-04-06 17:11:59'),
(847, 1, 'listening.dashboard', 1, '2021-04-06 17:12:06'),
(848, 1, 'listening.dashboard', 1, '2021-04-06 17:15:54'),
(849, 1, 'listening.dayform', 1, '2021-04-06 17:19:36'),
(850, 1, 'listening.dayform', 1, '2021-04-06 17:19:39'),
(851, 1, 'listening.dashboard', 1, '2021-04-06 17:19:39'),
(852, 1, 'listening.dashboard', 1, '2021-04-06 17:20:13'),
(853, 1, 'listening.weekform', 1, '2021-04-06 17:20:18'),
(854, 1, 'listening.weekform', 1, '2021-04-06 17:20:22'),
(855, 1, 'listening.dashboard', 1, '2021-04-06 17:20:22'),
(856, 1, 'listening.dayform', 1, '2021-04-06 17:20:26'),
(857, 1, 'listening.dayform', 1, '2021-04-06 17:20:29'),
(858, 1, 'listening.dashboard', 1, '2021-04-06 17:20:29'),
(859, 1, 'listening.dayform', 1, '2021-04-06 17:28:01'),
(860, 1, 'listening.dayform', 1, '2021-04-06 17:29:07'),
(861, 1, 'listening.dayform', 1, '2021-04-06 17:29:33'),
(862, 1, 'listening.dayform', 1, '2021-04-06 17:30:25'),
(863, 1, 'listening.dayform', 1, '2021-04-06 17:31:08'),
(864, 1, 'listening.dayform', 1, '2021-04-06 17:31:48'),
(865, 1, 'listening.dayform', 1, '2021-04-06 17:32:22'),
(866, 1, 'listening.dayform', 1, '2021-04-06 17:32:42'),
(867, 1, 'listening.dayform', 1, '2021-04-06 17:32:49'),
(868, 1, 'listening.dayform', 1, '2021-04-06 17:33:19'),
(869, 1, 'listening.dayform', 1, '2021-04-06 17:34:31'),
(870, 1, 'listening.dayform', 1, '2021-04-06 17:35:07'),
(871, 1, 'listening.dayform', 1, '2021-04-06 17:35:48'),
(872, 1, 'listening.dayform', 1, '2021-04-06 17:36:25'),
(873, 1, 'listening.dayform', 1, '2021-04-06 17:36:51'),
(874, 1, 'listening.dayform', 1, '2021-04-06 17:37:37'),
(875, 1, 'listening.dayform', 1, '2021-04-06 17:38:24'),
(876, 1, 'home.show', 1, '2021-04-07 08:47:15'),
(877, 1, 'listening.dashboard', 1, '2021-04-07 08:47:17'),
(878, 1, 'listening.dayform', 1, '2021-04-07 08:47:23'),
(879, 1, 'listening.dayform', 1, '2021-04-07 08:49:05'),
(880, 1, 'listening.dayform', 1, '2021-04-07 08:49:22'),
(881, 1, 'listening.dayform', 1, '2021-04-07 08:49:31'),
(882, 1, 'listening.dayform', 1, '2021-04-07 08:49:55'),
(883, 1, 'listening.dayform', 1, '2021-04-07 08:50:28'),
(884, 1, 'listening.dayform', 1, '2021-04-07 08:50:40'),
(885, 1, 'listening.dayform', 1, '2021-04-07 08:50:59'),
(886, 1, 'listening.dayform', 1, '2021-04-07 08:52:16'),
(887, 1, 'auth.login', 1, '2021-04-07 08:53:08'),
(888, 1, 'home.show', 1, '2021-04-07 08:53:08'),
(889, 1, 'listening.dashboard', 1, '2021-04-07 08:53:12'),
(890, 1, 'auth.login', 1, '2021-04-07 09:00:35'),
(891, 1, 'home.show', 1, '2021-04-07 09:00:35'),
(892, 1, 'listening.dashboard', 1, '2021-04-07 09:13:00'),
(893, 1, 'listening.dashboard', 1, '2021-04-07 09:13:14'),
(894, 1, 'listening.dashboard', 1, '2021-04-07 09:13:24'),
(895, 1, 'listening.dashboard', 1, '2021-04-07 09:13:36'),
(896, 1, 'listening.daynew', 1, '2021-04-07 09:20:00'),
(897, 1, 'listening.dashboard', 1, '2021-04-07 09:23:47'),
(898, 1, 'listening.dayform', 1, '2021-04-07 09:24:29'),
(899, 1, 'listening.dayform', 1, '2021-04-07 09:24:48'),
(900, 1, 'listening.dayform', 1, '2021-04-07 09:25:36'),
(901, 1, 'listening.dayform', 1, '2021-04-07 09:29:35'),
(902, 1, 'listening.dayform', 1, '2021-04-07 09:30:02'),
(903, 1, 'listening.dayform', 1, '2021-04-07 09:30:13'),
(904, 1, 'listening.dayform', 1, '2021-04-07 09:30:21'),
(905, 1, 'listening.dayform', 1, '2021-04-07 09:31:48'),
(906, 1, 'listening.dayform', 1, '2021-04-07 09:32:32'),
(907, 1, 'listening.dayform', 1, '2021-04-07 09:34:44'),
(908, 1, 'listening.dashboard', 1, '2021-04-07 09:34:44'),
(909, 1, 'listening.dashboard', 1, '2021-04-07 09:36:18'),
(910, 1, 'listening.dashboard', 1, '2021-04-07 09:36:36'),
(911, 1, 'listening.dashboard', 1, '2021-04-07 09:36:45'),
(912, 1, 'listening.dashboard', 1, '2021-04-07 09:37:04'),
(913, 1, 'listening.dashboard', 1, '2021-04-07 09:37:24'),
(914, 1, 'auth.login', 1, '2021-04-07 15:46:33'),
(915, 1, 'home.show', 1, '2021-04-07 15:46:33'),
(916, 1, 'listening.dashboard', 1, '2021-04-07 15:46:35'),
(917, 1, 'listening.dashboard', 1, '2021-04-07 16:08:44'),
(918, 1, 'listening.dashboard', 1, '2021-04-07 16:09:21'),
(919, 1, 'listening.dashboard', 1, '2021-04-07 16:10:11'),
(920, 1, 'listening.dashboard', 1, '2021-04-07 16:10:47'),
(921, 1, 'listening.dashboard', 1, '2021-04-07 16:11:46'),
(922, 1, 'listening.dashboard', 1, '2021-04-07 16:12:02'),
(923, 1, 'listening.dashboard', 1, '2021-04-07 16:12:18'),
(924, 1, 'listening.dashboard', 1, '2021-04-07 16:13:07'),
(925, 1, 'listening.dashboard', 1, '2021-04-07 16:13:20'),
(926, 1, 'listening.dashboard', 1, '2021-04-07 16:13:50'),
(927, 1, 'listening.dashboard', 1, '2021-04-07 16:14:04'),
(928, 1, 'listening.dashboard', 1, '2021-04-07 16:14:20'),
(929, 1, 'listening.dashboard', 1, '2021-04-07 16:14:46'),
(930, 1, 'listening.dashboard', 1, '2021-04-07 16:18:12'),
(931, 1, 'listening.dashboard', 1, '2021-04-07 16:19:40'),
(932, 1, 'listening.weeks', 1, '2021-04-07 17:47:58'),
(933, 1, 'listening.weeks', 1, '2021-04-07 17:48:51'),
(934, 1, 'listening.weeks', 1, '2021-04-07 17:49:36'),
(935, 1, 'listening.weeks', 1, '2021-04-07 17:50:12'),
(936, 1, 'listening.weeks', 1, '2021-04-07 17:50:42');
INSERT INTO `logs` (`id`, `type`, `action`, `user`, `date`) VALUES
(937, 1, 'listening.weeks', 1, '2021-04-07 17:50:52'),
(938, 1, 'listening.days', 1, '2021-04-07 17:52:31'),
(939, 1, 'listening.days', 1, '2021-04-07 17:52:48'),
(940, 1, 'listening.days', 1, '2021-04-07 17:53:16'),
(941, 1, 'listening.days', 1, '2021-04-07 17:53:30'),
(942, 1, 'listening.dashboard', 1, '2021-04-07 17:53:33'),
(943, 1, 'listening.days', 1, '2021-04-07 17:54:02'),
(944, 1, 'listening.weeks', 1, '2021-04-07 17:54:16'),
(945, 1, 'listening.days', 1, '2021-04-07 17:54:41'),
(946, 1, 'listening.practices', 1, '2021-04-07 17:54:43'),
(947, 1, 'listening.days', 1, '2021-04-07 17:54:44'),
(948, 1, 'listening.weeks', 1, '2021-04-07 17:54:45'),
(949, 1, 'listening.days', 1, '2021-04-07 17:55:11'),
(950, 1, 'listening.days', 1, '2021-04-07 17:55:18'),
(951, 1, 'listening.days', 1, '2021-04-07 17:55:34'),
(952, 1, 'listening.days', 1, '2021-04-07 17:55:35'),
(953, 1, 'listening.days', 1, '2021-04-07 17:55:47'),
(954, 1, 'listening.weeks', 1, '2021-04-07 17:55:59'),
(955, 1, 'listening.days', 1, '2021-04-07 17:56:00'),
(956, 1, 'listening.weeks', 1, '2021-04-07 17:56:07'),
(957, 1, 'listening.weeks', 1, '2021-04-07 17:56:27'),
(958, 1, 'listening.days', 1, '2021-04-07 17:56:30'),
(959, 1, 'listening.weeks', 1, '2021-04-07 17:56:35'),
(960, 1, 'listening.weeks', 1, '2021-04-07 17:57:14'),
(961, 1, 'listening.days', 1, '2021-04-07 17:57:17'),
(962, 1, 'listening.practices', 1, '2021-04-07 17:58:23'),
(963, 1, 'listening.practices', 1, '2021-04-07 17:59:10'),
(964, 1, 'listening.days', 1, '2021-04-07 17:59:15'),
(965, 1, 'listening.weeks', 1, '2021-04-07 17:59:16'),
(966, 1, 'listening.days', 1, '2021-04-07 17:59:16'),
(967, 1, 'listening.practices', 1, '2021-04-07 17:59:17'),
(968, 1, 'listening.days', 1, '2021-04-07 17:59:18'),
(969, 1, 'listening.practices', 1, '2021-04-07 17:59:20'),
(970, 1, 'listening.days', 1, '2021-04-07 17:59:23'),
(971, 1, 'listening.weeks', 1, '2021-04-07 17:59:24'),
(972, 1, 'listening.days', 1, '2021-04-07 17:59:25'),
(973, 1, 'listening.practices', 1, '2021-04-07 17:59:26'),
(974, 1, 'listening.weeks', 1, '2021-04-07 18:13:13'),
(975, 1, 'listening.days', 1, '2021-04-07 18:13:14'),
(976, 1, 'listening.dayform', 1, '2021-04-07 18:13:16'),
(977, 1, 'listening.days', 1, '2021-04-07 18:13:20'),
(978, 1, 'listening.daynew', 1, '2021-04-07 18:13:22'),
(979, 1, 'listening.daynew', 1, '2021-04-07 18:13:37'),
(980, 1, 'listening.daynew', 1, '2021-04-07 18:14:07'),
(981, 1, 'listening.daynew', 1, '2021-04-07 18:14:38'),
(982, 1, 'listening.daynew', 1, '2021-04-07 18:14:44'),
(983, 1, 'listening.daynew', 1, '2021-04-07 18:14:58'),
(984, 1, 'home.show', 1, '2021-04-07 18:15:09'),
(985, 1, 'listening.weeks', 1, '2021-04-07 18:15:12'),
(986, 1, 'listening.weeks', 1, '2021-04-07 18:15:29'),
(987, 1, 'listening.weeks', 1, '2021-04-07 18:15:56'),
(988, 1, 'listening.weeks', 1, '2021-04-07 18:15:58'),
(989, 1, 'listening.days', 1, '2021-04-07 18:16:13'),
(990, 1, 'listening.days', 1, '2021-04-07 18:16:31'),
(991, 1, 'listening.days', 1, '2021-04-07 18:16:35'),
(992, 1, 'home.show', 1, '2021-04-07 18:17:24'),
(993, 1, 'home.show', 1, '2021-04-07 18:17:24'),
(994, 1, 'listening.weeks', 1, '2021-04-07 18:17:25'),
(995, 1, 'listening.weeks', 1, '2021-04-07 18:17:40'),
(996, 1, 'listening.weeks', 1, '2021-04-07 18:17:48'),
(997, 1, 'listening.weeks', 1, '2021-04-07 18:18:10'),
(998, 1, 'listening.weeks', 1, '2021-04-07 18:18:34'),
(999, 1, 'listening.weeks', 1, '2021-04-07 18:18:41'),
(1000, 1, 'listening.weeks', 1, '2021-04-07 18:18:56'),
(1001, 1, 'listening.days', 1, '2021-04-07 18:19:02'),
(1002, 1, 'listening.days', 1, '2021-04-07 18:19:12'),
(1003, 1, 'listening.days', 1, '2021-04-07 18:19:38'),
(1004, 1, 'listening.days', 1, '2021-04-07 18:19:48'),
(1005, 1, 'listening.practices', 1, '2021-04-07 18:19:51'),
(1006, 1, 'listening.weeks', 1, '2021-04-07 18:19:52'),
(1007, 1, 'listening.days', 1, '2021-04-07 18:19:54'),
(1008, 1, 'listening.practices', 1, '2021-04-07 18:19:55'),
(1009, 1, 'listening.weeks', 1, '2021-04-07 18:19:56'),
(1010, 1, 'listening.weeks', 1, '2021-04-07 18:20:48'),
(1011, 1, 'listening.weeks', 1, '2021-04-07 18:23:19'),
(1012, 1, 'listening.weeks', 1, '2021-04-07 18:23:48'),
(1013, 1, 'listening.weeks', 1, '2021-04-07 18:24:03'),
(1014, 1, 'listening.weeks', 1, '2021-04-07 18:24:15'),
(1015, 1, 'listening.weeks', 1, '2021-04-07 18:25:27'),
(1016, 1, 'listening.weeks', 1, '2021-04-07 18:26:12'),
(1017, 1, 'listening.weeks', 1, '2021-04-07 18:26:17'),
(1018, 1, 'listening.days', 1, '2021-04-07 18:32:28'),
(1019, 1, 'listening.practices', 1, '2021-04-07 18:32:29'),
(1020, 1, 'listening.days', 1, '2021-04-07 18:32:33'),
(1021, 1, 'listening.weeks', 1, '2021-04-07 18:32:34'),
(1022, 1, 'listening.days', 1, '2021-04-07 18:32:37'),
(1023, 1, 'listening.weeks', 1, '2021-04-07 18:32:41'),
(1024, 1, 'listening.days', 1, '2021-04-07 18:32:45'),
(1025, 1, 'listening.practices', 1, '2021-04-07 18:32:50'),
(1026, 1, 'listening.weeks', 1, '2021-04-07 18:32:52'),
(1027, 1, 'listening.weeks', 1, '2021-04-07 18:33:10'),
(1028, 1, 'listening.weeks', 1, '2021-04-07 18:34:08'),
(1029, 1, 'listening.weeks', 1, '2021-04-07 18:35:08'),
(1030, 1, 'listening.weeks', 1, '2021-04-07 18:35:20'),
(1031, 1, 'listening.weekform', 1, '2021-04-07 18:35:25'),
(1032, 1, 'listening.weeks', 1, '2021-04-07 18:35:32'),
(1033, 1, 'listening.weeks', 1, '2021-04-07 18:36:13'),
(1034, 1, 'listening.daynew', 1, '2021-04-07 18:36:19'),
(1035, 1, 'listening.days', 1, '2021-04-07 18:36:25'),
(1036, 1, 'listening.dayform', 1, '2021-04-07 18:36:27'),
(1037, 1, 'listening.weeks', 1, '2021-04-07 18:36:45'),
(1038, 1, 'listening.weeks', 1, '2021-04-07 18:37:15'),
(1039, 1, 'listening.daynew', 1, '2021-04-07 18:37:17'),
(1040, 1, 'listening.weeks', 1, '2021-04-07 18:37:21'),
(1041, 1, 'listening.days', 1, '2021-04-07 18:37:23'),
(1042, 1, 'listening.practices', 1, '2021-04-07 18:37:24'),
(1043, 1, 'home.show', 1, '2021-04-07 18:37:25'),
(1044, 1, 'listening.weeks', 1, '2021-04-07 18:37:27'),
(1045, 1, 'listening.days', 1, '2021-04-07 18:37:31'),
(1046, 1, 'listening.weeks', 1, '2021-04-07 18:37:32'),
(1047, 1, 'listening.days', 1, '2021-04-07 18:37:35'),
(1048, 1, 'listening.weeks', 1, '2021-04-07 18:37:36'),
(1049, 1, 'listening.days', 1, '2021-04-07 18:37:39'),
(1050, 1, 'listening.dayform', 1, '2021-04-07 18:37:40'),
(1051, 1, 'listening.dayform', 1, '2021-04-07 18:41:11'),
(1052, 1, 'listening.dayform', 1, '2021-04-07 18:42:14'),
(1053, 1, 'listening.days', 1, '2021-04-07 18:42:14'),
(1054, 1, 'listening.dayform', 1, '2021-04-07 18:45:13'),
(1055, 1, 'listening.dayform', 1, '2021-04-07 18:45:25'),
(1056, 1, 'listening.dayform', 1, '2021-04-07 18:45:45'),
(1057, 1, 'listening.weeks', 1, '2021-04-07 18:51:15'),
(1058, 1, 'listening.weekform', 1, '2021-04-07 18:51:17'),
(1059, 1, 'listening.weekform', 1, '2021-04-07 18:51:47'),
(1060, 1, 'listening.weekform', 1, '2021-04-07 18:52:04'),
(1061, 1, 'listening.weekform', 1, '2021-04-07 18:52:47'),
(1062, 1, 'listening.weeks', 1, '2021-04-07 18:53:04'),
(1063, 1, 'listening.daynew', 1, '2021-04-07 18:53:09'),
(1064, 1, 'listening.weeks', 1, '2021-04-07 18:53:13'),
(1065, 1, 'listening.weekform', 1, '2021-04-07 18:53:27'),
(1066, 1, 'listening.weekform', 1, '2021-04-07 18:53:30'),
(1067, 1, 'listening.weekform', 1, '2021-04-07 19:04:59'),
(1068, 1, 'listening.weeks', 1, '2021-04-07 19:04:59'),
(1069, 1, 'listening.days', 1, '2021-04-07 19:05:03'),
(1070, 1, 'listening.dayform', 1, '2021-04-07 19:05:05'),
(1071, 1, 'listening.dayform', 1, '2021-04-07 19:05:43'),
(1072, 1, 'listening.days', 1, '2021-04-07 19:05:43'),
(1073, 1, 'listening.dayform', 1, '2021-04-07 19:05:46'),
(1074, 1, 'listening.weeks', 1, '2021-04-07 19:06:00'),
(1075, 1, 'listening.days', 1, '2021-04-07 19:06:02'),
(1076, 1, 'listening.daynew', 1, '2021-04-07 19:06:05'),
(1077, 1, 'listening.weeks', 1, '2021-04-07 19:06:22'),
(1078, 1, 'listening.days', 1, '2021-04-07 19:06:25'),
(1079, 1, 'listening.dayform', 1, '2021-04-07 19:06:27'),
(1080, 1, 'listening.weeks', 1, '2021-04-07 19:07:23'),
(1081, 1, 'listening.days', 1, '2021-04-07 19:07:25'),
(1082, 1, 'listening.daynew', 1, '2021-04-07 19:07:26'),
(1083, 1, 'listening.daynew', 1, '2021-04-07 19:09:52'),
(1084, 1, 'listening.daynew', 1, '2021-04-07 19:14:12'),
(1085, 1, 'listening.daynew', 1, '2021-04-07 19:15:26'),
(1086, 1, 'listening.daynew', 1, '2021-04-07 19:15:35'),
(1087, 1, 'listening.daynew', 1, '2021-04-07 19:16:00'),
(1088, 1, 'listening.daynew', 1, '2021-04-07 19:16:41'),
(1089, 1, 'listening.daynew', 1, '2021-04-07 19:17:03'),
(1090, 1, 'listening.daynew', 1, '2021-04-07 19:17:15'),
(1091, 1, 'listening.dayform', 1, '2021-04-07 19:17:48'),
(1092, 1, 'listening.dayform', 1, '2021-04-07 19:17:57'),
(1093, 1, 'listening.dayform', 1, '2021-04-07 19:18:05'),
(1094, 1, 'listening.dayform', 1, '2021-04-07 19:18:20'),
(1095, 1, 'listening.dayform', 1, '2021-04-07 19:18:56'),
(1096, 1, 'listening.dayform', 1, '2021-04-07 19:19:02'),
(1097, 1, 'listening.dayform', 1, '2021-04-07 19:19:04'),
(1098, 1, 'listening.dayform', 1, '2021-04-07 19:19:05'),
(1099, 1, 'listening.dayform', 1, '2021-04-07 19:19:18'),
(1100, 1, 'listening.dayform', 1, '2021-04-07 19:19:27'),
(1101, 1, 'listening.dayform', 1, '2021-04-07 19:19:32'),
(1102, 1, 'listening.dayform', 1, '2021-04-07 19:19:35'),
(1103, 1, 'listening.dayform', 1, '2021-04-07 19:19:41'),
(1104, 1, 'listening.dayform', 1, '2021-04-07 19:19:44'),
(1105, 1, 'listening.dayform', 1, '2021-04-07 19:19:51'),
(1106, 1, 'listening.weeks', 1, '2021-04-07 19:19:56'),
(1107, 1, 'listening.days', 1, '2021-04-07 19:19:57'),
(1108, 1, 'listening.daynew', 1, '2021-04-07 19:21:14'),
(1109, 1, 'listening.daynew', 1, '2021-04-07 19:21:28'),
(1110, 1, 'listening.daynew', 1, '2021-04-07 19:21:40'),
(1111, 1, 'listening.daynew', 1, '2021-04-07 19:22:13'),
(1112, 1, 'listening.weeks', 1, '2021-04-07 19:25:45'),
(1113, 1, 'listening.days', 1, '2021-04-07 19:25:48'),
(1114, 1, 'listening.practices', 1, '2021-04-07 19:25:50'),
(1115, 1, 'listening.days', 1, '2021-04-07 19:25:51'),
(1116, 1, 'listening.practices', 1, '2021-04-07 19:26:15'),
(1117, 1, 'listening.days', 1, '2021-04-07 19:26:16'),
(1118, 1, 'listening.practices', 1, '2021-04-07 19:26:25'),
(1119, 1, 'listening.days', 1, '2021-04-07 19:26:26'),
(1120, 1, 'listening.weeks', 1, '2021-04-07 19:26:26'),
(1121, 1, 'listening.days', 1, '2021-04-07 19:26:27'),
(1122, 1, 'listening.practices', 1, '2021-04-07 19:26:27'),
(1123, 1, 'listening.days', 1, '2021-04-07 19:26:30'),
(1124, 1, 'listening.weeks', 1, '2021-04-07 19:26:31'),
(1125, 1, 'listening.days', 1, '2021-04-07 19:26:31'),
(1126, 1, 'listening.practices', 1, '2021-04-07 19:26:32'),
(1127, 1, 'listening.days', 1, '2021-04-07 19:26:44'),
(1128, 1, 'listening.weeks', 1, '2021-04-07 19:26:52'),
(1129, 1, 'listening.daynew', 1, '2021-04-07 19:27:03'),
(1130, 1, 'listening.daynew', 1, '2021-04-07 19:27:08'),
(1131, 1, 'listening.daynew', 1, '2021-04-07 19:29:06'),
(1132, 1, 'listening.daynew', 1, '2021-04-07 19:29:19'),
(1133, 1, 'listening.daynew', 1, '2021-04-07 19:29:27'),
(1134, 1, 'listening.daynew', 1, '2021-04-07 19:29:31'),
(1135, 1, 'listening.days', 1, '2021-04-07 19:29:31'),
(1136, 1, 'listening.weeks', 1, '2021-04-07 19:29:36'),
(1137, 1, 'listening.weeks', 1, '2021-04-07 19:29:40'),
(1138, 1, 'listening.daynew', 1, '2021-04-07 19:29:46'),
(1139, 1, 'listening.daynew', 1, '2021-04-07 19:29:50'),
(1140, 1, 'listening.daynew', 1, '2021-04-07 19:29:55'),
(1141, 1, 'listening.daynew', 1, '2021-04-07 19:30:41'),
(1142, 1, 'listening.daynew', 1, '2021-04-07 19:31:21'),
(1143, 1, 'listening.daynew', 1, '2021-04-07 19:31:47'),
(1144, 1, 'listening.daynew', 1, '2021-04-07 19:32:03'),
(1145, 1, 'listening.days', 1, '2021-04-07 19:32:03'),
(1146, 1, 'listening.days', 1, '2021-04-07 19:32:18'),
(1147, 1, 'listening.weeks', 1, '2021-04-07 19:32:19'),
(1148, 1, 'listening.days', 1, '2021-04-07 19:32:20'),
(1149, 1, 'listening.daynew', 1, '2021-04-07 19:35:56'),
(1150, 1, 'listening.days', 1, '2021-04-07 19:35:56'),
(1151, 1, 'listening.days', 1, '2021-04-07 19:36:08'),
(1152, 1, 'listening.days', 1, '2021-04-07 19:36:15'),
(1153, 1, 'listening.daynew', 1, '2021-04-07 19:36:16'),
(1154, 1, 'listening.days', 1, '2021-04-07 19:36:16'),
(1155, 1, 'listening.daynew', 1, '2021-04-07 19:36:24'),
(1156, 1, 'listening.days', 1, '2021-04-07 19:36:24'),
(1157, 1, 'listening.weeks', 1, '2021-04-07 19:36:42'),
(1158, 1, 'listening.daynew', 1, '2021-04-07 19:36:54'),
(1159, 1, 'listening.days', 1, '2021-04-07 19:36:55'),
(1160, 1, 'listening.weeks', 1, '2021-04-07 19:36:58'),
(1161, 1, 'listening.daynew', 1, '2021-04-07 19:37:02'),
(1162, 1, 'listening.days', 1, '2021-04-07 19:37:02'),
(1163, 1, 'listening.daynew', 1, '2021-04-07 19:37:17'),
(1164, 1, 'listening.days', 1, '2021-04-07 19:37:17'),
(1165, 1, 'listening.weeks', 1, '2021-04-07 19:37:19'),
(1166, 1, 'listening.daynew', 1, '2021-04-07 19:37:22'),
(1167, 1, 'listening.days', 1, '2021-04-07 19:37:22'),
(1168, 1, 'listening.daynew', 1, '2021-04-07 19:38:08'),
(1169, 1, 'listening.days', 1, '2021-04-07 19:38:08'),
(1170, 1, 'listening.daynew', 1, '2021-04-07 19:38:17'),
(1171, 1, 'listening.daynew', 1, '2021-04-07 19:38:24'),
(1172, 1, 'listening.daynew', 1, '2021-04-07 19:38:44'),
(1173, 1, 'listening.weeks', 1, '2021-04-07 19:39:01'),
(1174, 1, 'listening.daynew', 1, '2021-04-07 19:39:02'),
(1175, 1, 'listening.daynew', 1, '2021-04-07 19:39:18'),
(1176, 1, 'listening.daynew', 1, '2021-04-07 19:39:46'),
(1177, 1, 'listening.daynew', 1, '2021-04-07 19:39:51'),
(1178, 1, 'listening.daynew', 1, '2021-04-07 19:40:03'),
(1179, 1, 'listening.weeks', 1, '2021-04-07 19:40:43'),
(1180, 1, 'listening.weeks', 1, '2021-04-07 19:41:09'),
(1181, 1, 'listening.daynew', 1, '2021-04-07 19:41:12'),
(1182, 1, 'listening.weeks', 1, '2021-04-07 19:41:16'),
(1183, 1, 'listening.daynew', 1, '2021-04-07 19:41:18'),
(1184, 1, 'listening.daynew', 1, '2021-04-07 19:41:37'),
(1185, 1, 'listening.daynew', 1, '2021-04-07 19:41:41'),
(1186, 1, 'listening.days', 1, '2021-04-07 19:41:41'),
(1187, 1, 'listening.daynew', 1, '2021-04-07 19:41:45'),
(1188, 1, 'listening.days', 1, '2021-04-07 19:41:45'),
(1189, 1, 'listening.dayform', 1, '2021-04-07 19:42:29'),
(1190, 1, 'listening.dayform', 1, '2021-04-07 19:42:51'),
(1191, 1, 'listening.days', 1, '2021-04-07 19:42:51'),
(1192, 1, 'listening.dayform', 1, '2021-04-07 19:42:56'),
(1193, 1, 'listening.dayform', 1, '2021-04-07 19:43:13'),
(1194, 1, 'listening.dayform', 1, '2021-04-07 19:43:21'),
(1195, 1, 'listening.days', 1, '2021-04-07 19:43:21'),
(1196, 1, 'listening.dayform', 1, '2021-04-07 19:43:29'),
(1197, 1, 'listening.dayform', 1, '2021-04-07 19:43:39'),
(1198, 1, 'listening.days', 1, '2021-04-07 19:43:39'),
(1199, 1, 'listening.dayform', 1, '2021-04-07 19:43:44'),
(1200, 1, 'listening.dayform', 1, '2021-04-07 19:45:37'),
(1201, 1, 'listening.dayform', 1, '2021-04-07 19:45:40'),
(1202, 1, 'listening.days', 1, '2021-04-07 19:45:40'),
(1203, 1, 'listening.daynew', 1, '2021-04-07 19:45:42'),
(1204, 1, 'listening.days', 1, '2021-04-07 19:45:42'),
(1205, 1, 'listening.dayform', 1, '2021-04-07 19:45:45'),
(1206, 1, 'listening.days', 1, '2021-04-07 19:45:50'),
(1207, 1, 'listening.weeks', 1, '2021-04-07 19:45:52'),
(1208, 1, 'listening.daynew', 1, '2021-04-07 19:45:53'),
(1209, 1, 'listening.daynew', 1, '2021-04-07 19:46:13'),
(1210, 1, 'listening.weeks', 1, '2021-04-07 19:48:05'),
(1211, 1, 'listening.days', 1, '2021-04-07 19:48:07'),
(1212, 1, 'listening.practices', 1, '2021-04-07 19:48:08'),
(1213, 1, 'home.show', 1, '2021-04-07 19:48:09'),
(1214, 1, 'listening.weeks', 1, '2021-04-07 19:48:11'),
(1215, 1, 'listening.days', 1, '2021-04-07 19:48:12'),
(1216, 1, 'listening.practices', 1, '2021-04-07 19:48:13'),
(1217, 1, 'listening.days', 1, '2021-04-07 19:48:14'),
(1218, 1, 'listening.weeks', 1, '2021-04-07 19:48:15'),
(1219, 1, 'listening.weekform', 1, '2021-04-07 19:48:20'),
(1220, 1, 'listening.weekform', 1, '2021-04-07 19:48:43'),
(1221, 1, 'listening.weekform', 1, '2021-04-07 19:49:14'),
(1222, 1, 'listening.weekform', 1, '2021-04-07 19:59:27'),
(1223, 1, 'listening.weekform', 1, '2021-04-07 19:59:30'),
(1224, 1, 'listening.days', 1, '2021-04-07 19:59:30'),
(1225, 1, 'listening.dayform', 1, '2021-04-07 19:59:32'),
(1226, 1, 'listening.dayform', 1, '2021-04-07 19:59:53'),
(1227, 1, 'listening.weekform', 1, '2021-04-07 20:00:05'),
(1228, 1, 'listening.days', 1, '2021-04-07 20:00:06'),
(1229, 1, 'listening.weeks', 1, '2021-04-07 20:00:08'),
(1230, 1, 'listening.weekform', 1, '2021-04-07 20:00:23'),
(1231, 1, 'listening.weekform', 1, '2021-04-07 20:00:34'),
(1232, 1, 'listening.weeks', 1, '2021-04-07 20:00:34'),
(1233, 1, 'listening.weekform', 1, '2021-04-07 20:02:12'),
(1234, 1, 'listening.weekform', 1, '2021-04-07 20:02:29'),
(1235, 1, 'listening.weekform', 1, '2021-04-07 20:03:19'),
(1236, 1, 'listening.weeks', 1, '2021-04-07 20:03:19'),
(1237, 1, 'listening.weekform', 1, '2021-04-07 20:03:21'),
(1238, 1, 'listening.weekform', 1, '2021-04-07 20:03:25'),
(1239, 1, 'listening.weeks', 1, '2021-04-07 20:03:25'),
(1240, 1, 'listening.weeks', 1, '2021-04-07 20:04:20'),
(1241, 1, 'listening.weekform', 1, '2021-04-07 20:04:23'),
(1242, 1, 'listening.weekform', 1, '2021-04-07 20:05:06'),
(1243, 1, 'listening.weeks', 1, '2021-04-07 20:05:06'),
(1244, 1, 'listening.weekform', 1, '2021-04-07 20:05:09'),
(1245, 1, 'listening.weekform', 1, '2021-04-07 20:05:41'),
(1246, 1, 'listening.weeks', 1, '2021-04-07 20:05:41'),
(1247, 1, 'listening.weekform', 1, '2021-04-07 20:12:22'),
(1248, 1, 'listening.weekform', 1, '2021-04-07 20:12:27'),
(1249, 1, 'listening.weeks', 1, '2021-04-07 20:12:27'),
(1250, 1, 'listening.weekform', 1, '2021-04-07 20:12:33'),
(1251, 1, 'listening.weekform', 1, '2021-04-07 20:12:35'),
(1252, 1, 'listening.weeks', 1, '2021-04-07 20:12:36'),
(1253, 1, 'listening.weeknew', 1, '2021-04-07 20:12:37'),
(1254, 1, 'listening.weeknew', 1, '2021-04-07 20:13:03'),
(1255, 1, 'listening.weeks', 1, '2021-04-07 20:13:03'),
(1256, 1, 'listening.weeks', 1, '2021-04-07 20:13:25'),
(1257, 1, 'listening.weeknew', 1, '2021-04-07 20:13:26'),
(1258, 1, 'listening.weeks', 1, '2021-04-07 20:13:28'),
(1259, 1, 'listening.days', 1, '2021-04-07 20:13:30'),
(1260, 1, 'listening.days', 1, '2021-04-07 20:13:48'),
(1261, 1, 'listening.daynew', 1, '2021-04-07 20:13:49'),
(1262, 1, 'listening.weeks', 1, '2021-04-07 20:14:01'),
(1263, 1, 'listening.practices', 1, '2021-04-07 20:14:03'),
(1264, 1, 'listening.practices', 1, '2021-04-07 20:14:16'),
(1265, 1, 'home.show', 1, '2021-04-07 20:14:47'),
(1266, 1, 'listening.weeks', 1, '2021-04-07 20:14:48'),
(1267, 1, 'listening.practices', 1, '2021-04-07 20:14:50'),
(1268, 1, 'listening.practicenew', 1, '2021-04-07 20:15:02'),
(1269, 1, 'listening.practices', 1, '2021-04-07 20:15:06'),
(1270, 1, 'listening.practices', 1, '2021-04-07 20:15:06'),
(1271, 1, 'listening.days', 1, '2021-04-07 20:15:37'),
(1272, 1, 'listening.dayform', 1, '2021-04-07 20:15:41'),
(1273, 1, 'listening.dayform', 1, '2021-04-07 20:16:56'),
(1274, 1, 'listening.dayform', 1, '2021-04-07 20:17:02'),
(1275, 1, 'listening.days', 1, '2021-04-07 20:17:02'),
(1276, 1, 'listening.daynew', 1, '2021-04-07 20:17:04'),
(1277, 1, 'listening.days', 1, '2021-04-07 20:17:06'),
(1278, 1, 'listening.dayform', 1, '2021-04-07 20:17:10'),
(1279, 1, 'listening.dayform', 1, '2021-04-07 20:17:12'),
(1280, 1, 'listening.days', 1, '2021-04-07 20:17:12'),
(1281, 1, 'listening.daynew', 1, '2021-04-07 20:17:14'),
(1282, 1, 'listening.days', 1, '2021-04-07 20:17:21'),
(1283, 1, 'listening.weeks', 1, '2021-04-07 20:17:22'),
(1284, 1, 'listening.daynew', 1, '2021-04-07 20:17:24'),
(1285, 1, 'listening.daynew', 1, '2021-04-07 20:18:12'),
(1286, 1, 'listening.daynew', 1, '2021-04-07 20:20:14'),
(1287, 1, 'listening.daynew', 1, '2021-04-07 20:20:30'),
(1288, 1, 'listening.daynew', 1, '2021-04-07 20:20:44'),
(1289, 1, 'listening.daynew', 1, '2021-04-07 20:21:14'),
(1290, 1, 'listening.daynew', 1, '2021-04-07 20:21:28'),
(1291, 1, 'listening.daynew', 1, '2021-04-07 20:21:29'),
(1292, 1, 'listening.daynew', 1, '2021-04-07 20:21:33'),
(1293, 1, 'listening.daynew', 1, '2021-04-07 20:21:46'),
(1294, 1, 'listening.daynew', 1, '2021-04-07 20:22:10'),
(1295, 1, 'listening.daynew', 1, '2021-04-07 20:22:30'),
(1296, 1, 'listening.daynew', 1, '2021-04-07 20:22:51'),
(1297, 1, 'listening.daynew', 1, '2021-04-07 20:22:57'),
(1298, 1, 'listening.daynew', 1, '2021-04-07 20:24:35'),
(1299, 1, 'listening.daynew', 1, '2021-04-07 20:28:32'),
(1300, 1, 'listening.weeks', 1, '2021-04-07 20:28:37'),
(1301, 1, 'listening.days', 1, '2021-04-07 20:28:40'),
(1302, 1, 'listening.weeks', 1, '2021-04-07 20:28:42'),
(1303, 1, 'listening.daynew', 1, '2021-04-07 20:28:43'),
(1304, 1, 'listening.daynew', 1, '2021-04-07 20:30:23'),
(1305, 1, 'listening.days', 1, '2021-04-07 20:30:23'),
(1306, 1, 'listening.daynew', 1, '2021-04-07 20:31:40'),
(1307, 1, 'listening.days', 1, '2021-04-07 20:31:44'),
(1308, 1, 'listening.dayform', 1, '2021-04-07 20:31:46'),
(1309, 1, 'listening.dayform', 1, '2021-04-07 20:31:50'),
(1310, 1, 'listening.days', 1, '2021-04-07 20:31:50'),
(1311, 1, 'listening.dayform', 1, '2021-04-07 20:32:26'),
(1312, 1, 'listening.dayform', 1, '2021-04-07 20:32:34'),
(1313, 1, 'listening.days', 1, '2021-04-07 20:32:34'),
(1314, 1, 'listening.dayform', 1, '2021-04-07 20:32:49'),
(1315, 1, 'listening.dayform', 1, '2021-04-07 20:32:53'),
(1316, 1, 'listening.days', 1, '2021-04-07 20:32:53'),
(1317, 1, 'listening.dayform', 1, '2021-04-07 20:33:18'),
(1318, 1, 'listening.dayform', 1, '2021-04-07 20:33:22'),
(1319, 1, 'listening.days', 1, '2021-04-07 20:33:22'),
(1320, 1, 'listening.dayform', 1, '2021-04-07 20:33:51'),
(1321, 1, 'listening.dayform', 1, '2021-04-07 20:33:55'),
(1322, 1, 'listening.days', 1, '2021-04-07 20:33:55'),
(1323, 1, 'listening.days', 1, '2021-04-07 20:34:12'),
(1324, 1, 'listening.dayform', 1, '2021-04-07 20:34:14'),
(1325, 1, 'listening.dayform', 1, '2021-04-07 20:34:17'),
(1326, 1, 'listening.days', 1, '2021-04-07 20:34:17'),
(1327, 1, 'listening.dayform', 1, '2021-04-07 20:34:34'),
(1328, 1, 'listening.dayform', 1, '2021-04-07 20:34:41'),
(1329, 1, 'listening.days', 1, '2021-04-07 20:34:41'),
(1330, 1, 'listening.dayform', 1, '2021-04-07 20:34:44'),
(1331, 1, 'listening.dayform', 1, '2021-04-07 20:34:48'),
(1332, 1, 'listening.days', 1, '2021-04-07 20:34:48'),
(1333, 1, 'listening.daynew', 1, '2021-04-07 20:34:53'),
(1334, 1, 'listening.daynew', 1, '2021-04-07 20:35:02'),
(1335, 1, 'listening.days', 1, '2021-04-07 20:35:03'),
(1336, 1, 'listening.dayform', 1, '2021-04-07 20:35:51'),
(1337, 1, 'listening.dayform', 1, '2021-04-07 20:35:55'),
(1338, 1, 'listening.days', 1, '2021-04-07 20:35:55'),
(1339, 1, 'auth.login', 1, '2021-04-08 09:45:02'),
(1340, 1, 'home.show', 1, '2021-04-08 09:45:02'),
(1341, 1, 'listening.weeks', 1, '2021-04-08 09:45:04'),
(1342, 1, 'listening.days', 1, '2021-04-08 09:45:20'),
(1343, 1, 'listening.practices', 1, '2021-04-08 09:45:34'),
(1344, 1, 'listening.days', 1, '2021-04-08 11:50:54'),
(1345, 1, 'listening.practices', 1, '2021-04-08 11:50:56'),
(1346, 1, 'listening.days', 1, '2021-04-08 11:50:58'),
(1347, 1, 'listening.weeks', 1, '2021-04-08 11:51:05'),
(1348, 1, 'listening.weeks', 1, '2021-04-08 11:51:34'),
(1349, 1, 'listening.days', 1, '2021-04-08 11:51:52'),
(1350, 1, 'listening.weeks', 1, '2021-04-08 11:51:53'),
(1351, 1, 'listening.weeks', 1, '2021-04-08 11:52:42'),
(1352, 1, 'listening.days', 1, '2021-04-08 11:52:46'),
(1353, 1, 'listening.weeks', 1, '2021-04-08 11:52:49'),
(1354, 1, 'listening.weeks', 1, '2021-04-08 11:53:17'),
(1355, 1, 'listening.days', 1, '2021-04-08 11:53:23'),
(1356, 1, 'listening.days', 1, '2021-04-08 11:54:19'),
(1357, 1, 'listening.days', 1, '2021-04-08 11:54:30'),
(1358, 1, 'listening.practices', 1, '2021-04-08 11:55:00'),
(1359, 1, 'listening.practices', 1, '2021-04-08 11:58:04'),
(1360, 1, 'listening.practices', 1, '2021-04-08 11:58:28'),
(1361, 1, 'listening.practices', 1, '2021-04-08 11:59:30'),
(1362, 1, 'listening.days', 1, '2021-04-08 11:59:46'),
(1363, 1, 'listening.practices', 1, '2021-04-08 11:59:57'),
(1364, 1, 'listening.days', 1, '2021-04-08 11:59:59'),
(1365, 1, 'listening.weeks', 1, '2021-04-08 12:00:02'),
(1366, 1, 'listening.days', 1, '2021-04-08 12:00:03'),
(1367, 1, 'listening.dayform', 1, '2021-04-08 12:00:04'),
(1368, 1, 'listening.dayform', 1, '2021-04-08 12:00:13'),
(1369, 1, 'listening.days', 1, '2021-04-08 12:00:13'),
(1370, 1, 'listening.weeks', 1, '2021-04-08 12:00:15'),
(1371, 1, 'listening.days', 1, '2021-04-08 12:00:19'),
(1372, 1, 'listening.days', 1, '2021-04-08 12:03:03'),
(1373, 1, 'listening.days', 1, '2021-04-08 12:03:05'),
(1374, 1, 'listening.weeks', 1, '2021-04-08 12:03:07'),
(1375, 1, 'listening.days', 1, '2021-04-08 12:03:09'),
(1376, 1, 'listening.days', 1, '2021-04-08 12:03:26'),
(1377, 1, 'listening.weeks', 1, '2021-04-08 12:03:30'),
(1378, 1, 'listening.days', 1, '2021-04-08 12:03:33'),
(1379, 1, 'listening.practices', 1, '2021-04-08 12:03:34'),
(1380, 1, 'listening.days', 1, '2021-04-08 12:11:32'),
(1381, 1, 'listening.practices', 1, '2021-04-08 12:11:36'),
(1382, 1, 'listening.practices', 1, '2021-04-08 12:12:04'),
(1383, 1, 'listening.practices', 1, '2021-04-08 12:12:19'),
(1384, 1, 'listening.practices', 1, '2021-04-08 12:13:06'),
(1385, 1, 'listening.practices', 1, '2021-04-08 12:13:15'),
(1386, 1, 'listening.practices', 1, '2021-04-08 12:13:25'),
(1387, 1, 'listening.practices', 1, '2021-04-08 12:13:28'),
(1388, 1, 'listening.days', 1, '2021-04-08 12:13:39'),
(1389, 1, 'listening.weeks', 1, '2021-04-08 12:13:39'),
(1390, 1, 'listening.days', 1, '2021-04-08 12:13:41'),
(1391, 1, 'listening.days', 1, '2021-04-08 12:15:30'),
(1392, 1, 'listening.days', 1, '2021-04-08 12:15:53'),
(1393, 1, 'listening.days', 1, '2021-04-08 12:15:55'),
(1394, 1, 'listening.days', 1, '2021-04-08 12:16:05'),
(1395, 1, 'listening.days', 1, '2021-04-08 12:16:21'),
(1396, 1, 'listening.days', 1, '2021-04-08 12:16:39'),
(1397, 1, 'listening.days', 1, '2021-04-08 12:17:58'),
(1398, 1, 'listening.weeks', 1, '2021-04-08 12:18:03'),
(1399, 1, 'listening.days', 1, '2021-04-08 12:18:06'),
(1400, 1, 'listening.daynew', 1, '2021-04-08 12:18:10'),
(1401, 1, 'listening.days', 1, '2021-04-08 12:18:13'),
(1402, 1, 'listening.weeks', 1, '2021-04-08 12:18:14'),
(1403, 1, 'listening.weeks', 1, '2021-04-08 12:18:47'),
(1404, 1, 'listening.days', 1, '2021-04-08 12:18:49'),
(1405, 1, 'listening.days', 1, '2021-04-08 12:18:53'),
(1406, 1, 'listening.practices', 1, '2021-04-08 12:33:50'),
(1407, 1, 'listening.days', 1, '2021-04-08 12:33:51'),
(1408, 1, 'listening.practices', 1, '2021-04-08 12:33:53'),
(1409, 1, 'listening.practices', 1, '2021-04-08 12:35:57'),
(1410, 1, 'listening.practicenew', 1, '2021-04-08 12:36:02'),
(1411, 1, 'listening.practices', 1, '2021-04-08 12:36:05'),
(1412, 1, 'listening.practices', 1, '2021-04-08 12:36:10'),
(1413, 1, 'listening.days', 1, '2021-04-08 12:36:19'),
(1414, 1, 'listening.practices', 1, '2021-04-08 12:36:21'),
(1415, 1, 'listening.dayform', 1, '2021-04-08 12:36:25'),
(1416, 1, 'listening.dayform', 1, '2021-04-08 12:36:34'),
(1417, 1, 'listening.days', 1, '2021-04-08 12:36:34'),
(1418, 1, 'listening.practices', 1, '2021-04-08 12:36:36'),
(1419, 1, 'listening.dayform', 1, '2021-04-08 12:36:58'),
(1420, 1, 'listening.dayform', 1, '2021-04-08 12:37:11'),
(1421, 1, 'listening.days', 1, '2021-04-08 12:37:11'),
(1422, 1, 'listening.practices', 1, '2021-04-08 12:37:14'),
(1423, 1, 'listening.days', 1, '2021-04-08 12:37:16'),
(1424, 1, 'listening.practices', 1, '2021-04-08 12:37:18'),
(1425, 1, 'listening.days', 1, '2021-04-08 12:37:20'),
(1426, 1, 'listening.practices', 1, '2021-04-08 12:37:21'),
(1427, 1, 'listening.days', 1, '2021-04-08 12:37:23'),
(1428, 1, 'listening.practices', 1, '2021-04-08 12:37:31'),
(1429, 1, 'listening.days', 1, '2021-04-08 12:37:44'),
(1430, 1, 'listening.dayform', 1, '2021-04-08 12:37:46'),
(1431, 1, 'listening.dayform', 1, '2021-04-08 12:37:55'),
(1432, 1, 'listening.days', 1, '2021-04-08 12:37:55'),
(1433, 1, 'listening.dayform', 1, '2021-04-08 12:37:58'),
(1434, 1, 'listening.dayform', 1, '2021-04-08 12:38:10'),
(1435, 1, 'listening.days', 1, '2021-04-08 12:38:10'),
(1436, 1, 'listening.dayform', 1, '2021-04-08 12:38:28'),
(1437, 1, 'listening.dayform', 1, '2021-04-08 12:38:33'),
(1438, 1, 'listening.days', 1, '2021-04-08 12:38:33'),
(1439, 1, 'listening.practices', 1, '2021-04-08 12:38:46'),
(1440, 1, 'listening.practices', 1, '2021-04-08 12:39:25'),
(1441, 1, 'home.show', 1, '2021-04-08 12:39:27'),
(1442, 1, 'listening.weeks', 1, '2021-04-08 12:39:29'),
(1443, 1, 'listening.practices', 1, '2021-04-08 12:39:31'),
(1444, 1, 'listening.days', 1, '2021-04-08 12:39:32'),
(1445, 1, 'listening.practices', 1, '2021-04-08 12:39:33'),
(1446, 1, 'listening.weeks', 1, '2021-04-08 12:39:36'),
(1447, 1, 'listening.days', 1, '2021-04-08 12:39:39'),
(1448, 1, 'listening.days', 1, '2021-04-08 12:40:07'),
(1449, 1, 'listening.days', 1, '2021-04-08 12:40:17'),
(1450, 1, 'listening.weeks', 1, '2021-04-08 12:40:19'),
(1451, 1, 'listening.practices', 1, '2021-04-08 12:40:20'),
(1452, 1, 'listening.practices', 1, '2021-04-08 12:40:33'),
(1453, 1, 'listening.days', 1, '2021-04-08 12:40:39'),
(1454, 1, 'listening.weeks', 1, '2021-04-08 12:40:40'),
(1455, 1, 'listening.days', 1, '2021-04-08 12:40:40'),
(1456, 1, 'listening.practices', 1, '2021-04-08 12:40:41'),
(1457, 1, 'home.show', 1, '2021-04-08 12:40:50'),
(1458, 1, 'listening.weeks', 1, '2021-04-08 12:40:51'),
(1459, 1, 'listening.practices', 1, '2021-04-08 12:40:53'),
(1460, 1, 'listening.days', 1, '2021-04-08 12:41:32'),
(1461, 1, 'listening.weeks', 1, '2021-04-08 12:41:35'),
(1462, 1, 'listening.weeks', 1, '2021-04-08 18:47:34'),
(1463, 1, 'listening.practices', 1, '2021-04-08 18:47:35'),
(1464, 1, 'listening.dayform', 1, '2021-04-08 18:48:41'),
(1465, 1, 'listening.practices', 1, '2021-04-08 18:48:59'),
(1466, 1, 'listening.practices', 1, '2021-04-08 18:49:00'),
(1467, 1, 'listening.practiceform', 1, '2021-04-08 18:49:03'),
(1468, 1, 'listening.practices', 1, '2021-04-08 18:49:45'),
(1469, 1, 'listening.days', 1, '2021-04-08 18:50:03'),
(1470, 1, 'listening.practices', 1, '2021-04-08 18:50:32'),
(1471, 1, 'listening.practiceform', 1, '2021-04-08 18:52:49'),
(1472, 1, 'listening.weeks', 1, '2021-04-08 18:53:31'),
(1473, 1, 'listening.days', 1, '2021-04-08 18:53:32'),
(1474, 1, 'listening.dayform', 1, '2021-04-08 18:53:37'),
(1475, 1, 'listening.dayform', 1, '2021-04-08 18:53:42'),
(1476, 1, 'listening.days', 1, '2021-04-08 18:53:42'),
(1477, 1, 'listening.daynew', 1, '2021-04-08 18:53:59'),
(1478, 1, 'listening.daynew', 1, '2021-04-08 18:54:24'),
(1479, 1, 'listening.days', 1, '2021-04-08 18:54:25'),
(1480, 1, 'listening.daynew', 1, '2021-04-08 18:54:27'),
(1481, 1, 'listening.daynew', 1, '2021-04-08 18:54:51'),
(1482, 1, 'listening.days', 1, '2021-04-08 18:54:51'),
(1483, 1, 'listening.daynew', 1, '2021-04-08 18:54:55'),
(1484, 1, 'listening.daynew', 1, '2021-04-08 18:55:12'),
(1485, 1, 'listening.days', 1, '2021-04-08 18:55:12'),
(1486, 1, 'listening.weeks', 1, '2021-04-08 18:58:05'),
(1487, 1, 'listening.days', 1, '2021-04-08 18:58:08'),
(1488, 1, 'listening.weeks', 1, '2021-04-08 18:59:17'),
(1489, 1, 'listening.days', 1, '2021-04-08 18:59:23'),
(1490, 1, 'listening.days', 1, '2021-04-08 19:07:19'),
(1491, 1, 'listening.practices', 1, '2021-04-08 19:07:21'),
(1492, 1, 'listening.practiceform', 1, '2021-04-08 19:07:25'),
(1493, 1, 'listening.practiceform', 1, '2021-04-08 19:08:50'),
(1494, 1, 'listening.practiceform', 1, '2021-04-08 19:09:13'),
(1495, 1, 'listening.practiceform', 1, '2021-04-08 19:09:19'),
(1496, 1, 'listening.practiceform', 1, '2021-04-08 19:12:46'),
(1497, 1, 'listening.practiceform', 1, '2021-04-08 19:14:19'),
(1498, 1, 'listening.practiceform', 1, '2021-04-08 19:14:53'),
(1499, 1, 'listening.practiceform', 1, '2021-04-08 19:15:04'),
(1500, 1, 'listening.practiceform', 1, '2021-04-08 19:16:20'),
(1501, 1, 'listening.practiceform', 1, '2021-04-08 20:05:29'),
(1502, 1, 'listening.practiceform', 1, '2021-04-08 20:05:56'),
(1503, 1, 'listening.practiceform', 1, '2021-04-08 20:06:10'),
(1504, 1, 'listening.practiceform', 1, '2021-04-08 20:15:53'),
(1505, 1, 'listening.practiceform', 1, '2021-04-08 20:19:06'),
(1506, 1, 'listening.practices', 1, '2021-04-08 20:19:06'),
(1507, 1, 'listening.practiceform', 1, '2021-04-08 20:19:12'),
(1508, 1, 'listening.practiceform', 1, '2021-04-08 20:19:31'),
(1509, 1, 'listening.practices', 1, '2021-04-08 20:19:31'),
(1510, 1, 'listening.weeks', 1, '2021-04-08 20:20:10'),
(1511, 1, 'listening.weeks', 1, '2021-04-08 20:21:18'),
(1512, 1, 'listening.weeks', 1, '2021-04-08 20:21:56'),
(1513, 1, 'listening.days', 1, '2021-04-08 20:34:51'),
(1514, 1, 'listening.days', 1, '2021-04-08 20:34:55'),
(1515, 1, 'listening.weeks', 1, '2021-04-09 08:28:49'),
(1516, 1, 'listening.weeks', 1, '2021-04-09 08:28:59'),
(1517, 1, 'auth.login', 1, '2021-04-11 18:16:34'),
(1518, 1, 'home.show', 1, '2021-04-11 18:16:34'),
(1519, 1, 'media.manager', 1, '2021-04-11 18:16:41'),
(1520, 1, 'media.manager', 1, '2021-04-11 18:17:47'),
(1521, 1, 'media.manager', 1, '2021-04-11 18:18:20'),
(1522, 1, 'media.manager', 1, '2021-04-11 18:18:27'),
(1523, 1, 'media.manager', 1, '2021-04-11 18:20:17'),
(1524, 1, 'media.manager', 1, '2021-04-11 18:21:06'),
(1525, 1, 'media.manager', 1, '2021-04-11 18:21:13'),
(1526, 1, 'media.manager', 1, '2021-04-11 18:21:33'),
(1527, 1, 'media.manager', 1, '2021-04-11 18:21:52'),
(1528, 1, 'media.manager', 1, '2021-04-11 18:22:03'),
(1529, 1, 'home.show', 1, '2021-04-11 18:22:21'),
(1530, 1, 'listening.weeks', 1, '2021-04-11 18:22:23'),
(1531, 1, 'listening.practices', 1, '2021-04-11 18:22:26'),
(1532, 1, 'listening.weeks', 1, '2021-04-11 18:22:34'),
(1533, 1, 'listening.practices', 1, '2021-04-11 18:22:37'),
(1534, 1, 'listening.practices', 1, '2021-04-11 18:23:11'),
(1535, 1, 'listening.practices', 1, '2021-04-11 18:23:26'),
(1536, 1, 'listening.practices', 1, '2021-04-11 18:23:35'),
(1537, 1, 'home.show', 1, '2021-04-11 18:23:37'),
(1538, 1, 'listening.weeks', 1, '2021-04-11 18:23:39'),
(1539, 1, 'media.manager', 1, '2021-04-11 18:24:42'),
(1540, 1, 'media.manager', 1, '2021-04-11 18:24:54'),
(1541, 1, 'media.manager', 1, '2021-04-11 18:25:02'),
(1542, 1, 'media.manager', 1, '2021-04-11 18:25:12'),
(1543, 1, 'media.manager', 1, '2021-04-11 18:25:17'),
(1544, 1, 'media.manager', 1, '2021-04-11 18:25:24'),
(1545, 1, 'media.manager', 1, '2021-04-11 18:25:31'),
(1546, 1, 'media.manager', 1, '2021-04-11 18:26:04'),
(1547, 1, 'media.manager', 1, '2021-04-11 18:26:09'),
(1548, 1, 'media.manager', 1, '2021-04-11 18:26:17'),
(1549, 1, 'media.manager', 1, '2021-04-11 18:26:33'),
(1550, 1, 'media.manager', 1, '2021-04-11 18:27:44'),
(1551, 1, 'media.manager', 1, '2021-04-11 18:27:58'),
(1552, 1, 'media.manager', 1, '2021-04-11 18:29:28'),
(1553, 1, 'media.manager', 1, '2021-04-11 18:30:01'),
(1554, 1, 'media.manager', 1, '2021-04-11 18:31:50'),
(1555, 1, 'media.manager', 1, '2021-04-11 18:31:51'),
(1556, 1, 'media.manager', 1, '2021-04-11 18:32:17'),
(1557, 1, 'media.manager', 1, '2021-04-11 18:32:30'),
(1558, 1, 'media.manager', 1, '2021-04-11 18:33:30'),
(1559, 1, 'auth.login', 1, '2021-04-11 18:35:36'),
(1560, 1, 'home.show', 1, '2021-04-11 18:35:36'),
(1561, 1, 'media.manager', 1, '2021-04-11 18:35:41'),
(1562, 1, 'home.show', 1, '2021-04-11 18:35:41'),
(1563, 1, 'media.manager', 1, '2021-04-11 18:35:49'),
(1564, 1, 'home.show', 1, '2021-04-11 18:35:49'),
(1565, 1, 'media.manager', 1, '2021-04-11 18:36:08'),
(1566, 1, 'home.show', 1, '2021-04-11 18:36:08'),
(1567, 1, 'media.manager', 1, '2021-04-11 18:50:46'),
(1568, 1, 'media.manager', 1, '2021-04-11 18:50:46'),
(1569, 1, 'media.manager', 1, '2021-04-11 18:50:46'),
(1570, 1, 'home.show', 1, '2021-04-11 18:51:07'),
(1571, 1, 'media.manager', 1, '2021-04-11 18:51:09'),
(1572, 1, 'media.manager', 1, '2021-04-11 18:52:02'),
(1573, 1, 'media.manager', 1, '2021-04-11 18:52:03'),
(1574, 1, 'media.manager', 1, '2021-04-11 18:52:09'),
(1575, 1, 'media.manager', 1, '2021-04-11 18:52:09'),
(1576, 1, 'media.manager', 1, '2021-04-11 18:52:11'),
(1577, 1, 'media.manager', 1, '2021-04-11 18:52:11'),
(1578, 1, 'media.manager', 1, '2021-04-11 19:07:34'),
(1579, 1, 'media.manager', 1, '2021-04-11 19:07:34'),
(1580, 1, 'media.manager', 1, '2021-04-11 19:07:39'),
(1581, 1, 'media.manager', 1, '2021-04-11 19:07:39'),
(1582, 1, 'media.manager', 1, '2021-04-11 19:07:39'),
(1583, 1, 'media.manager', 1, '2021-04-11 19:07:45'),
(1584, 1, 'media.manager', 1, '2021-04-11 19:07:45'),
(1585, 1, 'auth.logout', 1, '2021-04-11 19:07:46'),
(1586, 1, 'auth.login', 1, '2021-04-11 19:07:52'),
(1587, 1, 'home.show', 1, '2021-04-11 19:07:52'),
(1588, 1, 'auth.login', 1, '2021-04-11 19:15:06'),
(1589, 1, 'home.show', 1, '2021-04-11 19:15:06'),
(1590, 1, 'listening.weeks', 1, '2021-04-11 19:15:35'),
(1591, 1, 'listening.days', 1, '2021-04-11 19:15:36'),
(1592, 1, 'media.manager', 1, '2021-04-11 19:16:58'),
(1593, 1, 'media.manager', 1, '2021-04-11 19:17:23'),
(1594, 1, 'media.manager', 1, '2021-04-11 19:17:34'),
(1595, 1, 'media.manager', 1, '2021-04-11 19:17:35'),
(1596, 1, 'media.manager', 1, '2021-04-11 19:18:01'),
(1597, 1, 'media.manager', 1, '2021-04-11 19:18:15'),
(1598, 1, 'media.manager', 1, '2021-04-11 19:18:29'),
(1599, 1, 'media.manager', 1, '2021-04-11 19:18:45'),
(1600, 1, 'media.manager', 1, '2021-04-11 19:19:28'),
(1601, 1, 'media.manager', 1, '2021-04-11 19:19:47'),
(1602, 1, 'media.manager', 1, '2021-04-11 19:19:59'),
(1603, 1, 'media.manager', 1, '2021-04-11 19:23:33'),
(1604, 1, 'media.manager', 1, '2021-04-11 19:23:54'),
(1605, 1, 'media.manager', 1, '2021-04-11 19:24:45'),
(1606, 1, 'media.manager', 1, '2021-04-11 19:24:48'),
(1607, 1, 'media.manager', 1, '2021-04-11 19:24:49'),
(1608, 1, 'media.manager', 1, '2021-04-11 19:24:53'),
(1609, 1, 'media.manager', 1, '2021-04-11 19:25:35'),
(1610, 1, 'media.manager', 1, '2021-04-11 19:26:03'),
(1611, 1, 'media.manager', 1, '2021-04-11 19:26:05'),
(1612, 1, 'media.manager', 1, '2021-04-11 19:26:16'),
(1613, 1, 'media.manager', 1, '2021-04-11 19:26:27'),
(1614, 1, 'home.show', 1, '2021-04-12 08:33:03'),
(1615, 1, 'media.manager', 1, '2021-04-12 08:33:22'),
(1616, 1, 'media.manager', 1, '2021-04-12 08:33:22'),
(1617, 1, 'media.manager', 1, '2021-04-12 08:33:22'),
(1618, 1, 'media.manager', 1, '2021-04-12 08:33:22'),
(1619, 1, 'media.files', 1, '2021-04-12 08:37:53'),
(1620, 1, 'media.files', 1, '2021-04-12 08:38:19'),
(1621, 1, 'media.files', 1, '2021-04-12 08:38:39'),
(1622, 1, 'media.files', 1, '2021-04-12 08:38:40'),
(1623, 1, 'media.files', 1, '2021-04-12 08:38:40'),
(1624, 1, 'media.files', 1, '2021-04-12 08:38:40'),
(1625, 1, 'media.files', 1, '2021-04-12 08:38:40'),
(1626, 1, 'media.files', 1, '2021-04-12 08:38:40'),
(1627, 1, 'media.files', 1, '2021-04-12 08:38:54'),
(1628, 1, 'media.files', 1, '2021-04-12 08:39:23'),
(1629, 1, 'media.files', 1, '2021-04-12 08:39:23'),
(1630, 1, 'media.files', 1, '2021-04-12 08:39:24'),
(1631, 1, 'media.files', 1, '2021-04-12 08:39:24'),
(1632, 1, 'media.files', 1, '2021-04-12 08:39:24'),
(1633, 1, 'media.files', 1, '2021-04-12 08:39:24'),
(1634, 1, 'media.files', 1, '2021-04-12 08:39:45'),
(1635, 1, 'media.files', 1, '2021-04-12 08:40:40'),
(1636, 1, 'media.files', 1, '2021-04-12 08:40:41'),
(1637, 1, 'media.files', 1, '2021-04-12 08:40:41'),
(1638, 1, 'media.files', 1, '2021-04-12 08:40:41'),
(1639, 1, 'media.files', 1, '2021-04-12 08:42:29'),
(1640, 1, 'media.files', 1, '2021-04-12 08:43:04'),
(1641, 1, 'home.show', 1, '2021-04-12 08:43:05'),
(1642, 1, 'media.manager', 1, '2021-04-12 08:43:12'),
(1643, 1, 'media.manager', 1, '2021-04-12 08:43:12'),
(1644, 1, 'media.files', 1, '2021-04-12 08:43:14'),
(1645, 1, 'media.files', 1, '2021-04-12 08:43:26'),
(1646, 1, 'home.show', 1, '2021-04-12 08:43:27'),
(1647, 1, 'media.files', 1, '2021-04-12 08:43:32'),
(1648, 1, 'home.show', 1, '2021-04-12 08:43:32'),
(1649, 1, 'media.files', 1, '2021-04-12 08:44:19'),
(1650, 1, 'home.show', 1, '2021-04-12 08:44:19'),
(1651, 1, 'media.manager', 1, '2021-04-12 08:45:14'),
(1652, 1, 'media.files', 1, '2021-04-12 08:45:17'),
(1653, 1, 'home.show', 1, '2021-04-12 08:45:17'),
(1654, 1, 'home.show', 1, '2021-04-12 08:46:32'),
(1655, 1, 'media.files', 1, '2021-04-12 08:46:35'),
(1656, 1, 'media.files', 1, '2021-04-12 08:47:08'),
(1657, 1, 'media.files', 1, '2021-04-12 08:47:09'),
(1658, 1, 'media.files', 1, '2021-04-12 08:47:22'),
(1659, 1, 'media.files', 1, '2021-04-12 08:47:48'),
(1660, 1, 'media.files', 1, '2021-04-12 08:47:50'),
(1661, 1, 'media.files', 1, '2021-04-12 08:47:59'),
(1662, 1, 'media.files', 1, '2021-04-12 08:48:08'),
(1663, 1, 'media.files', 1, '2021-04-12 08:48:17'),
(1664, 1, 'media.files', 1, '2021-04-12 08:48:26'),
(1665, 1, 'media.files', 1, '2021-04-12 08:48:31'),
(1666, 1, 'media.files', 1, '2021-04-12 08:48:43'),
(1667, 1, 'media.files', 1, '2021-04-12 08:48:47'),
(1668, 1, 'media.files', 1, '2021-04-12 08:48:53'),
(1669, 1, 'media.files', 1, '2021-04-12 08:49:00'),
(1670, 1, 'media.files', 1, '2021-04-12 08:49:04'),
(1671, 1, 'media.files', 1, '2021-04-12 08:49:08'),
(1672, 1, 'media.files', 1, '2021-04-12 08:49:12'),
(1673, 1, 'media.files', 1, '2021-04-12 08:49:18'),
(1674, 1, 'media.files', 1, '2021-04-12 08:49:19'),
(1675, 1, 'media.files', 1, '2021-04-12 08:49:20'),
(1676, 1, 'media.files', 1, '2021-04-12 08:49:28'),
(1677, 1, 'media.files', 1, '2021-04-12 08:49:38'),
(1678, 1, 'media.files', 1, '2021-04-12 08:49:49'),
(1679, 1, 'media.files', 1, '2021-04-12 08:49:53'),
(1680, 1, 'media.files', 1, '2021-04-12 08:49:59'),
(1681, 1, 'media.files', 1, '2021-04-12 08:50:08'),
(1682, 1, 'media.files', 1, '2021-04-12 08:50:17'),
(1683, 1, 'media.files', 1, '2021-04-12 08:50:21'),
(1684, 1, 'media.files', 1, '2021-04-12 08:50:31'),
(1685, 1, 'media.files', 1, '2021-04-12 08:50:32'),
(1686, 1, 'media.files', 1, '2021-04-12 08:50:38'),
(1687, 1, 'media.files', 1, '2021-04-12 08:50:41'),
(1688, 1, 'media.files', 1, '2021-04-12 08:50:44'),
(1689, 1, 'media.files', 1, '2021-04-12 08:51:04'),
(1690, 1, 'media.files', 1, '2021-04-12 08:51:11'),
(1691, 1, 'media.files', 1, '2021-04-12 08:51:20'),
(1692, 1, 'media.files', 1, '2021-04-12 08:51:53'),
(1693, 1, 'media.files', 1, '2021-04-12 08:52:03'),
(1694, 1, 'media.files', 1, '2021-04-12 08:52:06'),
(1695, 1, 'media.files', 1, '2021-04-12 08:52:08'),
(1696, 1, 'media.files', 1, '2021-04-12 08:52:17'),
(1697, 1, 'media.files', 1, '2021-04-12 08:52:27'),
(1698, 1, 'media.files', 1, '2021-04-12 08:52:31'),
(1699, 1, 'media.files', 1, '2021-04-12 08:53:51'),
(1700, 1, 'media.files', 1, '2021-04-12 08:53:56'),
(1701, 1, 'media.files', 1, '2021-04-12 08:54:05'),
(1702, 1, 'media.files', 1, '2021-04-12 09:10:06'),
(1703, 1, 'media.files', 1, '2021-04-12 09:10:36'),
(1704, 1, 'media.files', 1, '2021-04-12 09:10:39'),
(1705, 1, 'media.files', 1, '2021-04-12 09:10:39'),
(1706, 1, 'media.files', 1, '2021-04-12 09:10:41'),
(1707, 1, 'media.files', 1, '2021-04-12 09:11:00'),
(1708, 1, 'home.show', 1, '2021-04-12 09:11:00'),
(1709, 1, 'media.manager', 1, '2021-04-12 09:12:23'),
(1710, 1, 'media.manager', 1, '2021-04-12 09:12:23'),
(1711, 1, 'media.files', 1, '2021-04-12 09:12:24'),
(1712, 1, 'media.files', 1, '2021-04-12 09:12:26'),
(1713, 1, 'media.files', 1, '2021-04-12 09:12:42'),
(1714, 1, 'media.files', 1, '2021-04-12 09:12:45'),
(1715, 1, 'media.files', 1, '2021-04-12 09:12:51'),
(1716, 1, 'media.files', 1, '2021-04-12 09:12:57'),
(1717, 1, 'media.files', 1, '2021-04-12 09:13:19'),
(1718, 1, 'media.files', 1, '2021-04-12 09:13:23'),
(1719, 1, 'media.files', 1, '2021-04-12 09:13:32'),
(1720, 1, 'media.files', 1, '2021-04-12 09:13:35'),
(1721, 1, 'media.files', 1, '2021-04-12 09:14:19'),
(1722, 1, 'media.files', 1, '2021-04-12 09:14:27'),
(1723, 1, 'media.files', 1, '2021-04-12 09:14:30'),
(1724, 1, 'media.files', 1, '2021-04-12 09:14:31'),
(1725, 1, 'media.files', 1, '2021-04-12 09:14:41'),
(1726, 1, 'media.files', 1, '2021-04-12 09:14:44'),
(1727, 1, 'media.files', 1, '2021-04-12 09:14:54'),
(1728, 1, 'media.files', 1, '2021-04-12 09:14:58'),
(1729, 1, 'media.files', 1, '2021-04-12 09:15:08'),
(1730, 1, 'media.files', 1, '2021-04-12 09:15:15'),
(1731, 1, 'media.files', 1, '2021-04-12 09:15:16'),
(1732, 1, 'media.files', 1, '2021-04-12 09:16:23'),
(1733, 1, 'media.files', 1, '2021-04-12 09:16:25'),
(1734, 1, 'media.files', 1, '2021-04-12 09:16:41'),
(1735, 1, 'media.files', 1, '2021-04-12 09:16:50'),
(1736, 1, 'media.files', 1, '2021-04-12 09:17:16'),
(1737, 1, 'media.files', 1, '2021-04-12 09:17:27'),
(1738, 1, 'home.show', 1, '2021-04-12 09:17:40'),
(1739, 1, 'media.files', 1, '2021-04-12 09:17:55'),
(1740, 1, 'media.files', 1, '2021-04-12 09:18:20'),
(1741, 1, 'media.files', 1, '2021-04-12 09:18:45'),
(1742, 1, 'media.files', 1, '2021-04-12 09:18:50'),
(1743, 1, 'media.files', 1, '2021-04-12 09:18:53'),
(1744, 1, 'media.files', 1, '2021-04-12 09:19:00'),
(1745, 1, 'media.files', 1, '2021-04-12 09:19:18'),
(1746, 1, 'media.files', 1, '2021-04-12 09:19:21'),
(1747, 1, 'media.files', 1, '2021-04-12 09:19:24'),
(1748, 1, 'media.files', 1, '2021-04-12 09:20:28'),
(1749, 1, 'media.files', 1, '2021-04-12 09:20:29'),
(1750, 1, 'home.show', 1, '2021-04-12 17:31:15'),
(1751, 1, 'media.files', 1, '2021-04-13 08:50:22'),
(1752, 1, 'media.files', 1, '2021-04-13 08:50:41'),
(1753, 1, 'media.files', 1, '2021-04-13 08:50:44'),
(1754, 1, 'media.files', 1, '2021-04-13 08:50:48'),
(1755, 1, 'media.files', 1, '2021-04-13 08:50:50'),
(1756, 1, 'media.files', 1, '2021-04-13 08:50:52');

-- --------------------------------------------------------

--
-- Структура таблицы `media`
--

CREATE TABLE `media` (
  `id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `text` text COLLATE utf8mb4_general_ci,
  `type` int DEFAULT NULL,
  `data` text COLLATE utf8mb4_general_ci,
  `url` text COLLATE utf8mb4_general_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `media`
--

INSERT INTO `media` (`id`, `title`, `name`, `text`, `type`, `data`, `url`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 'Lesson 1', 'lesson.mp3', NULL, 1, NULL, '/media/music/music.mp3', '2021-04-08 19:29:32', 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `slides`
--

CREATE TABLE `slides` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `url` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category` int NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `slides`
--

INSERT INTO `slides` (`id`, `user`, `url`, `date`, `category`, `status`) VALUES
(1, 1, 'https://i.picsum.photos/id/809/400/250.jpg?hmac=2f-6tfCMn0Xqf1ekbU_3yjVTiOttcDGgBw8me_BFdDw', '2020-12-06 00:55:42', 0, 1),
(2, 1, 'https://i.picsum.photos/id/545/400/250.jpg?hmac=dwV06WncX9F-q2Yw37l-ffGz1iH3Lt9aFX4ya5RSYiw', '2020-12-06 00:56:51', 0, 1),
(3, 1, 'https://i.picsum.photos/id/2/400/250.jpg?hmac=xQarGFhPP7UqhRdaEoqEBcWnlHuctr99TQdkB9fv3O8', '2020-12-06 00:56:51', 0, 1),
(4, 1, 'https://i.picsum.photos/id/1079/400/250.jpg?hmac=InqR2wOl1RhfBRCZIlImg1KQdZFCs3OUc_vzn3BQpBU', '2020-12-06 00:57:18', 0, 1),
(5, 1, 'https://i.picsum.photos/id/580/400/250.jpg?hmac=TGatCf-0jVubHnohnmf0vOr4-ySdVgiQ021ShH_L2KY', '2020-12-06 00:57:18', 0, 1),
(6, 1, 'https://i.picsum.photos/id/80/400/250.jpg?hmac=_SdiunS86ZDGpytlABJ7u16UWvd8UT-H01atgESh_D0', '2020-12-06 00:57:24', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tips`
--

CREATE TABLE `tips` (
  `id` int NOT NULL,
  `text` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `tips`
--

INSERT INTO `tips` (`id`, `text`, `user`, `date`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-07 02:38:34');

-- --------------------------------------------------------

--
-- Структура таблицы `tips_user`
--

CREATE TABLE `tips_user` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `tips` int NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `tips_user`
--

INSERT INTO `tips_user` (`id`, `user`, `tips`, `status`, `date`) VALUES
(1, 1, 1, 0, '2020-12-07 02:51:10');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `modified`, `created`) VALUES
(1, 'admin', '$2y$10$MszeSZpsEJI93y19.4EXUe78rXAkWI5loFH5j9sDrgq5pFNBzqbre', 'lostov@hi.uz', NULL, '2020-12-03 03:38:33');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `GROUP.NAME` (`name`) USING BTREE;

--
-- Индексы таблицы `config_colors`
--
ALTER TABLE `config_colors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contest`
--
ALTER TABLE `contest`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `listening`
--
ALTER TABLE `listening`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `listening_days`
--
ALTER TABLE `listening_days`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `listening_practices`
--
ALTER TABLE `listening_practices`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `listening_weeks`
--
ALTER TABLE `listening_weeks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tips`
--
ALTER TABLE `tips`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tips_user`
--
ALTER TABLE `tips_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `config_colors`
--
ALTER TABLE `config_colors`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `contest`
--
ALTER TABLE `contest`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `items`
--
ALTER TABLE `items`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `listening`
--
ALTER TABLE `listening`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `listening_days`
--
ALTER TABLE `listening_days`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT для таблицы `listening_practices`
--
ALTER TABLE `listening_practices`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `listening_weeks`
--
ALTER TABLE `listening_weeks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1757;

--
-- AUTO_INCREMENT для таблицы `media`
--
ALTER TABLE `media`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `tips`
--
ALTER TABLE `tips`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `tips_user`
--
ALTER TABLE `tips_user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
