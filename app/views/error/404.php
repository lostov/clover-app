  <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
    <div class="container">
      <div class="collapse navbar-collapse justify-content-end">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="../dashboard/home" class="nav-link">
              <i class="material-icons">dashboard</i>
              Dashboard
            </a>
          </li>
          <li class="nav-item ">
            <a href="../system/login" class="nav-link">
              <i class="material-icons">fingerprint</i>
              Login
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="wrapper wrapper-full-page">
    <div class="page-header error-page header-filter" style="background-image: url('<?php echo __SITE__; ?>/www/assets/img/clint-mckoy.jpg')">
      <div class="content-center">
        <div class="row">
          <div class="col-md-12">
            <h1 class="title">404</h1>
            <h2>Page not found :(</h2>
            <h4>Ooooups! Looks like you got lost.</h4>
          </div>
        </div>
      </div>
    </div>
  </div>