<?php use Core\Routing\Helper AS RouteHelper; ?>
<?php use Core\Support\HTML; ?>

<?php HTML::set('configNavTitle', 'Weeks dashboard') ?>
<?php HTML::set('configBackgroundImage', __SITE__ . '/assets/img/sidebar-1.jpg'); ?>
<?php HTML::set('configSidebarUsername', $username); ?>
<?php HTML::set('configSidebarUsermail', $email); ?>
<?php HTML::set('configSidebarNavPage', RouteHelper::getLink('Listening::weeks')); ?>

<div class="wrapper">

  <?php echo HTML::sidebar(); ?>

  <div class="main-panel">

    <?php echo HTML::navbar(); ?>

    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 ml-auto mr-auto">
            <div class="page-categories">
              <h3 class="title text-center">Listening</h3>
              <br>
              <ul class="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center">
                <li class="nav-item">
                  <a class="nav-link active show" href="<?php echo RouteHelper::getLink('Listening::weeks') ?>">
                    <i class="material-icons">view_week</i> Weeks
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo RouteHelper::getLink('Listening::days') ?>">
                    <i class="material-icons">article</i> Days
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo RouteHelper::getLink('Listening::practices') ?>">
                    <i class="material-icons">view_list</i> Practices
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo RouteHelper::getLink('Listening::contests') ?>">
                    <i class="material-icons">list</i> Contests
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo RouteHelper::getLink('Listening::bonuses') ?>">
                    <i class="material-icons">question_answer</i> Bonuses
                  </a>
                </li>
              </ul>
              <div class="tab-content tab-space tab-subcategories">
                <div class="tab-pane active show" id="tabWeeks">

                  <div class="row">
                    <div class="col-md-12">
                      <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                          <a href="<?php echo RouteHelper::getLink('Listening::weekNew') . '?session=' . $session; ?>" class="card-icon btn-primary">
                            <i class="material-icons">add</i>
                          </a>
                          <h4 class="card-title">
                            Add week
                          </h4>
                        </div>
                        <div class="card-body">
                          <div class="toolbar">List of Weeks</div>
                          <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>Title</th>
                                  <th>Icon</th>
                                  <th>Created</th>
                                  <th class="disabled-sorting text-right">Action</th>
                                </tr>
                              </thead>
                              <tfoot>
                                <tr>
                                  <th>ID</th>
                                  <th>Title</th>
                                  <th>Icon</th>
                                  <th>Created</th>
                                  <th class="text-right">Action</th>
                                </tr>
                              </tfoot>
                              <tbody>
                                <?php foreach ($weeks as $key => $week): ?>
                                  <tr>
                                    <td><?php echo $week['id']; ?></td>
                                    <td><?php echo $week['title']; ?></td>
                                    <td><?php echo $week['icon']; ?></td>
                                    <td><?php echo $week['created']; ?></td>
                                    <td class="text-right">
                                      <a class="btn btn-success btn-sm" href="<?php echo RouteHelper::getLink('Listening::days') . '?week=' . $week['id'] . '&session=' . $session; ?>">
                                        <span class="btn-label"><i class="material-icons">article</i></span>
                                        Days
                                        <div class="ripple-container"></div>
                                      </a>
                                      <a class="btn btn-info btn-sm" href="<?php echo RouteHelper::getLink('Listening::dayNew') . '?week=' . $week['id'] . '&session=' . $session; ?>">
                                        <span class="btn-label"><i class="material-icons">add</i></span>
                                        Day
                                        <div class="ripple-container"></div>
                                      </a>
                                      <a href="<?php echo RouteHelper::getLink('Listening::weekForm') . '?id=' . $week['id'] . '&session=' . $session; ?>" class="btn btn-link btn-success btn-just-icon to-word"><i class="material-icons">create</i></a>
                                      <?php if ($week['status'] == 0): ?>
                                        <a href="#" title="click to enable" class="btn btn-link btn-default btn-just-icon" data-click="visibility-off" data-id="<?php echo $week['id'] ?>"><i class="material-icons">visibility_off</i></a>
                                      <?php elseif($week['status'] == -1): ?>
                                        <a href="#" title="click to disable" class="btn btn-link btn-primary btn-just-icon" data-click="visibility" data-id="<?php echo $week['id'] ?>"><i class="material-icons">archive</i></a>
                                      <?php else: ?>
                                        <a href="#" title="click to disable" class="btn btn-link btn-warning btn-just-icon" data-click="visibility" data-id="<?php echo $week['id'] ?>"><i class="material-icons">visibility</i></a>
                                      <?php endif ?>
                                      <a href="#" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">delete</i></a>
                                    </td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php echo HTML::footer(); ?>

  </div>
</div>
<?php
  $site = __SITE__;
  $script =
<<<script
    $(document).ready(function() {
      $('.datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        \$tr = $(this).closest('tr');
        var data = table.row(\$tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        \$tr = $(this).closest('tr');
        table.row(\$tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });

      table.on('click', '.to-word', function() {
        // alert('You clicked on Like button');
      });

      table.on('click', '.to-export', function() {
        var btn = $(this);
            btn.toggleClass('btn-info btn-warning word-selected');
        var elem = btn.find('i');
        if( elem.text() == 'add') {
          elem.text('close');
        } else {
          elem.text('add');
        }
      });
      $('#to-word').click(function(){
        var keys = '';
        $('.word-selected').each(function(){
          var elem = $(this);
          keys+=','+elem.data('id');
        });
        var url = '['+keys.substring(1)+']';
        $('#form-word-id').val(url).parent().submit();
        return;
        var link = '$site/export/word?id=[' + keys.substring(1) + ']';
        var popup = window.open(link,'Popup','left=10000,top=10000,width=0,height=0');
        // popup.close();
        $(popup).load(function(){
          this.opener.postMessage({'loaded': true}, "*");
          this.close();
      });
      });

      var hash = document.location.hash;
      if (hash.length > 0) {
        var tab = jQuery('a[href="'+hash+'"');
        if ( tab ) {
          tab.tab('show');
        }
      }
    });
script;
  $postScripts[] = ['script' => $script];
?>