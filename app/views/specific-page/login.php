  <div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" filter-color="black" style="background-image: url('../assets/img/login.jpg'); background-size: cover; background-position: top center;">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
            <form class="form" method="POST" action="login" id="form">
              <div class="card card-login card-hidden">
                <div class="card-header card-header-rose text-center">
                  <h4 class="card-title">Login</h4>
                </div>
                <div class="card-body ">
                  <?php if ($error['message']): ?>
                    <p class="card-description text-center"><?php echo $error['message']; ?></p>
                  <?php endif ?>
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">account_box</i>
                        </span>
                      </div>
                      <input type="username" name="auth[username]" class="form-control" placeholder="Username..." required>
                    </div>
                  </span>
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">lock_outline</i>
                        </span>
                      </div>
                      <input type="password" name="auth[password]" class="form-control" placeholder="Password..." required>
                    </div>
                  </span>
                </div>
                <div class="card-footer justify-content-center">
                  <a href="#submit" class="btn btn-rose btn-link btn-lg" onclick="$('#form').submit();">Submit</a>
                </div>
              </div>
              <input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;" tabindex="-1" />
              <input type="hidden" name="auth[session]" value="<?php echo $session; ?>">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
$postScript[] = 
<<<script
  <script>
    $(document).ready(function() {
      md.checkFullPageBackgroundImage();
      setTimeout(function() {
        // after 1000 ms we add the class animated to the login/register card
        $('.card').removeClass('card-hidden');
      }, 700);
    });
  </script>
script;
?>