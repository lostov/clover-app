<?php
use Core\Support\HTML;
?>
<div class="wrapper ">

  <div class="sidebar" data-color="rose" data-background-color="black" data-image="<?php echo __SITE__; ?>/assets/img/sidebar-1.jpg">
    <div class="logo">
    	<a href="/" class="simple-text logo-mini">
        <i class="mi">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24" fill="#fff"><path d="M12,11.18C15.3,8.18 17,6.64 17,4.69C17,3.19 15.75,2 14.25,2C13.39,2 12.57,2.36 12,3C11.43,2.36 10.61,2 9.69,2C8.19,2 7,3.25 7,4.75C7,6.64 8.7,8.18 12,11.18M11.18,12C8.18,8.7 6.64,7 4.69,7C3.19,7 2,8.25 2,9.75C2,10.61 2.36,11.43 3,12C2.36,12.57 2,13.39 2,14.31C2,15.81 3.25,17 4.75,17C6.64,17 8.18,15.3 11.18,12M12.83,12C15.82,15.3 17.36,17 19.31,17C20.81,17 22,15.75 22,14.25C22,13.39 21.64,12.57 21,12C21.64,11.43 22,10.61 22,9.69C22,8.19 20.75,7 19.25,7C17.36,7 15.82,8.7 12.83,12M12,12.82C8.7,15.82 7,17.36 7,19.31C7,20.81 8.25,22 9.75,22C10.61,22 11.43,21.64 12,21C12.57,21.64 13.39,22 14.31,22C15.81,22 17,20.75 17,19.25C17,17.36 15.3,15.82 12,12.82Z" /></svg>
        </i>
      </a>
      <a href="#" class="simple-text logo-normal">
        CloverApp
      </a>
    </div>
    <div class="sidebar-wrapper">
      <div class="user">
        <div class="photo">
          <img src="<?php echo __SITE__; ?>/assets/img/faces/user.png" />
        </div>
        <div class="user-info">
          <a data-toggle="collapse" href="#collapseExample" class="username">
            <span>
              <?php echo $username . "[{$email}]"; ?>
              <b class="caret"></b>
            </span>
          </a>
          <div class="collapse" id="collapseExample">
            <ul class="nav">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span class="sidebar-mini"> MP </span>
                  <span class="sidebar-normal"> My Profile </span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span class="sidebar-mini"> EP </span>
                  <span class="sidebar-normal"> Edit Profile </span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <span class="sidebar-mini"> S </span>
                  <span class="sidebar-normal"> Settings </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <ul class="nav">
        <li class="nav-item active ">
          <a class="nav-link" href="/">
            <i class="material-icons">dashboard</i>
            <p> Dashboard </p>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/clover/listening">
            <i class="material-icons">hearing</i>
            <p> Listening </p>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/clover/reading">
            <i class="material-icons">chrome_reader_mode</i>
            <p> Reading </p>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/clover/writing">
            <i class="material-icons">edit</i>
            <p> Writing </p>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/clover/speaking">
            <i class="material-icons">speaker_phone</i>
            <p> Speaking </p>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="/clover/speaking">
            <i class="material-icons">menu_book</i>
            <p> Vocabulary </p>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="/clover/settings">
            <i class="material-icons">settings_applications</i>
            <p> App Settings </p>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="/system/settings">
            <i class="material-icons">settings</i>
            <p> Settings </p>
          </a>
        </li>

        <?php if (false): ?>
          <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#formsExamples">
              <i class="material-icons">content_paste</i>
              <p> Forma
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="formsExamples">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> F1 </span>
                    <span class="sidebar-normal"> Forma 1 </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> F2 </span>
                    <span class="sidebar-normal"> Forma 2 </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> F3 </span>
                    <span class="sidebar-normal"> Forma 3 </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> F4 </span>
                    <span class="sidebar-normal"> Forma 4 </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#tablesExamples">
              <i class="material-icons">grid_on</i>
              <p> Jadval
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="tablesExamples">
              <ul class="nav">
                <li class="nav-item ">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> J1 </span>
                    <span class="sidebar-normal"> Jadval 1 </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> J2 </span>
                    <span class="sidebar-normal"> Jadval 2 </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> J3</span>
                    <span class="sidebar-normal"> Jadval 3</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
        <?php endif ?>

        <li class="nav-item ">
          <a class="nav-link" href="system/logs">
            <i class="material-icons">date_range</i>
            <p> Logs </p>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="main-panel">
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
              <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
              <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
            </button>
          </div>
          <a class="navbar-brand" href="javascript:;">Dashboard</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon icon-bar"></span>
          <span class="navbar-toggler-icon icon-bar"></span>
          <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">notifications</i>
                <p class="d-lg-none d-md-block">
                  Habarlar
                </p>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Mike John responded to your email</a>
                <a class="dropdown-item" href="#">You have 5 new tasks</a>
                <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                <a class="dropdown-item" href="#">Another Notification</a>
                <a class="dropdown-item" href="#">Another One</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">person</i>
                <p class="d-lg-none d-md-block">
                  Account
                </p>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                <a class="dropdown-item" href="#">Account</a>
                <a class="dropdown-item" href="#">Settings</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="system/logout">Logout</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="material-icons"></i>
                </div>
                <h4 class="card-title">Global Stats by Top Locations</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-6">
                    <div class="table-responsive table-sales">
                      <table class="table">
                        <tbody>
                          <tr>
                            <td>
                              <div class="flag">
                                <img class="lazyload" data-src="../assets/img/flags/US.png" </div>
                            </td>
                            <td>Uzbekistan</td>
                            <td class="text-right">
                              2.920
                            </td>
                            <td class="text-right">
                              53.23%
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div class="flag">
                                <img class="lazyload" data-src="../assets/img/flags/DE.png" </div>
                            </td>
                            <td>Germany</td>
                            <td class="text-right">
                              1.300
                            </td>
                            <td class="text-right">
                              20.43%
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div class="flag">
                                <img class="lazyload" data-src="../assets/img/flags/AU.png" </div>
                            </td>
                            <td>Australia</td>
                            <td class="text-right">
                              760
                            </td>
                            <td class="text-right">
                              10.35%
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div class="flag">
                                <img class="lazyload" data-src="../assets/img/flags/GB.png" </div>
                            </td>
                            <td>United Kingdom</td>
                            <td class="text-right">
                              690
                            </td>
                            <td class="text-right">
                              7.87%
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div class="flag">
                                <img class="lazyload" data-src="../assets/img/flags/RO.png" </div>
                            </td>
                            <td>Romania</td>
                            <td class="text-right">
                              600
                            </td>
                            <td class="text-right">
                              5.94%
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <div class="flag">
                                <img class="lazyload" data-src="../assets/img/flags/BR.png" </div>
                            </td>
                            <td>Brasil</td>
                            <td class="text-right">
                              550
                            </td>
                            <td class="text-right">
                              4.34%
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col-md-6 ml-auto mr-auto">
                    <div id="worldMap" style="height: 300px;"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="card card-chart">
              <div class="card-header card-header-rose" data-header-animation="true">
                <div class="ct-chart" id="websiteViewsChart"></div>
              </div>
              <div class="card-body">
                <div class="card-actions">
                  <button type="button" class="btn btn-danger btn-link fix-broken-card">
                    <i class="material-icons">build</i> Fix Header!
                  </button>
                  <button type="button" class="btn btn-info btn-link" rel="tooltip" data-placement="bottom" title="Refresh">
                    <i class="material-icons">refresh</i>
                  </button>
                  <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Change Date">
                    <i class="material-icons">edit</i>
                  </button>
                </div>
                <h4 class="card-title">Website Views</h4>
                <p class="card-category">Last Campaign Performance</p>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i> campaign sent 2 days ago
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-chart">
              <div class="card-header card-header-success" data-header-animation="true">
                <div class="ct-chart" id="dailySalesChart"></div>
              </div>
              <div class="card-body">
                <div class="card-actions">
                  <button type="button" class="btn btn-danger btn-link fix-broken-card">
                    <i class="material-icons">build</i> Fix Header!
                  </button>
                  <button type="button" class="btn btn-info btn-link" rel="tooltip" data-placement="bottom" title="Refresh">
                    <i class="material-icons">refresh</i>
                  </button>
                  <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Change Date">
                    <i class="material-icons">edit</i>
                  </button>
                </div>
                <h4 class="card-title">Daily stats</h4>
                <p class="card-category">
                  <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today stats.</p>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i> updated 4 minutes ago
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-chart">
              <div class="card-header card-header-info" data-header-animation="true">
                <div class="ct-chart" id="completedTasksChart"></div>
              </div>
              <div class="card-body">
                <div class="card-actions">
                  <button type="button" class="btn btn-danger btn-link fix-broken-card">
                    <i class="material-icons">build</i> Fix Header!
                  </button>
                  <button type="button" class="btn btn-info btn-link" rel="tooltip" data-placement="bottom" title="Refresh">
                    <i class="material-icons">refresh</i>
                  </button>
                  <button type="button" class="btn btn-default btn-link" rel="tooltip" data-placement="bottom" title="Change Date">
                    <i class="material-icons">edit</i>
                  </button>
                </div>
                <h4 class="card-title">Completed Tasks</h4>
                <p class="card-category">Last Campaign Performance</p>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">access_time</i> campaign sent 2 days ago
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">weekend</i>
                </div>
                <p class="card-category">Bookings</p>
                <h3 class="card-title">184</h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons text-danger">warning</i>
                  <a href="#pablo">Get More Space...</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">equalizer</i>
                </div>
                <p class="card-category">Website Visits</p>
                <h3 class="card-title">75.521</h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">local_offer</i> Tracked from Google Analytics
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">store</i>
                </div>
                <p class="card-category">Revenue</p>
                <h3 class="card-title">$34,245</h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">date_range</i> Last 24 Hours
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                  <i class="fa fa-twitter"></i>
                </div>
                <p class="card-category">Followers</p>
                <h3 class="card-title">+245</h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons">update</i> Just Updated
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="footer">
      <div class="container-fluid">
        <div class="copyright float-right">
          &copy;
          <script>
            document.write(new Date().getFullYear())
          </script>, made with <i class="material-icons">favorite</i> by
          <a href="https://www.t.me/lostov" target="_blank">lostov</a> for a better web.
        </div>
      </div>
    </footer>
  </div>
</div>
<?php
  $site = __SITE__;
  $script =
<<<script
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

      md.initVectorMap();

    });
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        \$tr = $(this).closest('tr');
        var data = table.row(\$tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        \$tr = $(this).closest('tr');
        table.row(\$tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });

      table.on('click', '.to-word', function() {
        alert('You clicked on Like button');
      });

      table.on('click', '.to-export', function() {
        var btn = $(this);
            btn.toggleClass('btn-info btn-warning word-selected');
        var elem = btn.find('i');
        if( elem.text() == 'add') {
          elem.text('close');
        } else {
          elem.text('add');
        }
      });
      $('#to-word').click(function(){
        var keys = '';
        $('.word-selected').each(function(){
          var elem = $(this);
          keys+=','+elem.data('id');
        });
        var url = '['+keys.substring(1)+']';
        $('#form-word-id').val(url).parent().submit();
        return;
        var link = '$site/export/word?id=[' + keys.substring(1) + ']';
        var popup = window.open(link,'Popup','left=10000,top=10000,width=0,height=0');
        // popup.close();
        $(popup).load(function(){
          this.opener.postMessage({'loaded': true}, "*");
          this.close();
      });
      });
    });
script;
  $postScripts[] = ['script' => $script];
?>