<?php use Core\Routing\Helper AS RouteHelper; ?>
<?php use Core\Support\HTML; ?>

<?php HTML::set('configNavTitle', 'Add Day') ?>
<?php HTML::set('configBackgroundImage', __SITE__ . '/assets/img/sidebar-2.jpg'); ?>
<?php HTML::set('configSidebarUsername', $username); ?>
<?php HTML::set('configSidebarUsermail', $email); ?>
<?php HTML::set('configSidebarNavPage', RouteHelper::getLink('Reading::weeks')); ?>

<div class="wrapper">

  <?php echo HTML::sidebar(); ?>

  <div class="main-panel">

    <?php echo HTML::navbar(); ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">edit</i>
                  </div>
                  <h4 class="card-title">Day</h4>
                </div>
                <div class="card-body ">
                  <form method="POST" action="<?php echo RouteHelper::getLink('Reading::dayNew') ?>">
                    <div class="form-group bmd-form-group is-filled">
                      <label for="day_week" class="bmd-label-floating">Week</label>
                      <select class="selectpicker" data-style="select-with-transition" name="day[week]" id="day_week" title="Choose Week" data-size="5" required>
                        <?php foreach ($weeks as $key => $week): ?>
                          <option value="<?php echo $week['id'] ?>"<?php echo ($week['id'] == $weekDay) ? ' selected' : '' ?>><?php echo $week['title'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="day_title" class="bmd-label-floating">Title</label>
                      <input type="title" name="day[title]" class="form-control" id="day_title" value="<?php echo $day['title']; ?>" required>
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="day_text" class="bmd-label-floating">Description</label>
                      <input type="text" name="day[text]" class="form-control" id="day_text" value="<?php echo $day['text']; ?>">
                    </div>
                    <div class="form-group bmd-form-group is-filled">
                      <label for="day_color" class="bmd-label-floating">Color</label>
                      <select class="selectpicker" data-style="select-with-transition" name="day[color]" id="day_color" title="Choose Color" data-size="5" required>
                        <?php foreach ($colors as $key => $color): ?>
                          <option value="<?php echo $color['id'] ?>"<?php echo ($color['id'] == $colorDay) ? ' selected' : '' ?>>[<?php echo $color['code'] ?>] <?php echo $color['name'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group bmd-form-group">
                      <h5 class="title mb-0 mt-0">
                        <label for="day_icon" class="title bmd-label-floating">icon</label>
                      </h5>
                      <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail img-circle">
                          <img src="/assets/img/placeholder.jpg" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
                        <div>
                          <span class="btn btn-round btn-rose btn-file">
                            <span class="fileinput-new">Add ICON</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="..." />
                          </span>
                          <br />
                          <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                        </div>
                      </div>
                    </div>
                    <div class="form-group bmd-form-group">
                      <h5 class="title mb-0 mt-0">
                        <label for="day_icon" class="title bmd-label-floating">Status</label>
                      </h5>
                      <select name="day[status]" class="selectpicker" data-size="4" data-style="btn btn-primary btn-round" title="status state">
                        <option value="1" selected>Enabled</option>
                        <option value="0">Disabled</option>
                        <option value="-1">Archived</option>
                      </select>
                    </div>
                    <div class="form-group bmd-form-group">
                      <button type="submit" class="btn btn-fill btn-rose">Submit</button>
                    </div>
                    <input type="hidden" name="session" value="<?php echo $session; ?>">
                  </form>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>

    <?php echo HTML::footer(); ?>

  </div>
</div>
<?php
  $site = __SITE__;
  $script =
<<<script
    $(document).ready(function() {

    });
script;
  $postScripts[] = ['script' => $script];
?>