<?php use Core\Routing\Helper AS RouteHelper; ?>
<?php use Core\Support\HTML; ?>

<?php HTML::set('configNavTitle', 'Edit Day') ?>
<?php HTML::set('configBackgroundImage', __SITE__ . '/assets/img/sidebar-2.jpg'); ?>
<?php HTML::set('configSidebarUsername', $username); ?>
<?php HTML::set('configSidebarUsermail', $email); ?>
<?php HTML::set('configSidebarNavPage', RouteHelper::getLink('Speaking::weeks')); ?>

<div class="wrapper">

  <?php echo HTML::sidebar(); ?>

  <div class="main-panel">

    <?php echo HTML::navbar(); ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">edit</i>
                  </div>
                  <h4 class="card-title">Day</h4>
                </div>
                <div class="card-body ">
                  <form method="POST" action="<?php RouteHelper::getLink('Speaking::dayForm') . '?session=' . $session; ?>">
                    <div class="form-group bmd-form-group is-filled">
                      <label for="week_week" class="bmd-label-floating">Week</label>
                      <select class="selectpicker" data-style="select-with-transition" name="day[week]" id="day_week" title="Choose Week" data-size="5">
                        <?php foreach ($weeks as $key => $week): ?>
                          <option value="<?php echo $week['id'] ?>"<?php echo ($day['week'] === $week['id']) ? ' selected' : '' ?>><?php echo $week['title'] ?></option>
                        <?php endforeach ?>
                      </select>
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="week_title" class="bmd-label-floating">Title</label>
                      <input type="text" name="day[title]" class="form-control" id="day_title" value="<?php echo $day['title']; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="day_text" class="bmd-label-floating">Description</label>
                      <input type="text" name="day[text]" class="form-control" id="day_text" value="<?php echo $day['text']; ?>">
                    </div>
                    <div class="form-group bmd-form-group is-filled">
                      <label for="day_color" class="bmd-label-floating">Color</label>
                      <select class="selectpicker" data-style="select-with-transition" name="day[color]" id="day_color" title="Choose Color" data-size="5" required>
                        <?php foreach ($colors as $key => $color): ?>
                          <option value="<?php echo $color['id'] ?>"<?php echo ($day['color'] === $color['id']) ? ' selected' : '' ?>>[<?php echo $color['code'] ?>] <?php echo $color['name'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="day_created" class="bmd-label-floating">Created</label>
                      <input type="text" class="form-control disable" disabled id="day_created" value="<?php echo $day['created']; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="day_created_by" class="bmd-label-floating">Created by</label>
                      <input type="text" class="form-control disable" disabled id="day_created_by" value="<?php echo $day['created_user']; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="day_modified" class="bmd-label-floating">Modified</label>
                      <input type="text" class="form-control disable" disabled id="day_modified" value="<?php echo $day['modified'] ?? 'NaN'; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="day_modified_by" class="bmd-label-floating">Modified by</label>
                      <input type="text" class="form-control disable" disabled id="day_modified_by" value="<?php echo $day['modified_user'] ?? 'NaN'; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="day_id" class="bmd-label-floating">ID</label>
                      <input type="text" name="day[id]" class="form-control disable" disabled id="day_id" value="<?php echo $day['id'] ?? 'NaN'; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <h5 class="title mb-0 mt-0">
                        <label for="day_icon" class="title bmd-label-floating">icon</label>
                      </h5>
                      <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail img-circle">
                          <img src="/assets/img/placeholder.jpg" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
                        <div>
                          <span class="btn btn-round btn-rose btn-file">
                            <span class="fileinput-new">Add ICON</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="..." />
                          </span>
                          <br />
                          <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                        </div>
                      </div>
                    </div>
                    <div class="form-group bmd-form-group">
                      <h5 class="title mb-0 mt-0">
                        <label for="day_icon" class="title bmd-label-floating">Status</label>
                      </h5>
                      <select name="day[status]" class="selectpicker" data-size="4" data-style="btn btn-primary btn-round" title="status state">
                        <option value="1"<?php echo ($day['status'] == 1) ? ' selected':''; ?>>Enabled</option>
                        <option value="0"<?php echo ($day['status'] == 0) ? ' selected':''; ?>>Disabled</option>
                        <option value="-1"<?php echo ($day['status'] == -1) ? ' selected':''; ?>>Archived</option>
                      </select>
                    </div>
                    <div class="form-group bmd-form-group">
                      <button type="submit" class="btn btn-fill btn-rose">Submit</button>
                    </div>
                    <input type="hidden" name="day[id]" value="<?php echo $day['id']; ?>">
                    <input type="hidden" name="session" value="<?php echo $session; ?>">
                  </form>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>

    <?php echo HTML::footer(); ?>

  </div>
</div>
<?php
  $site = __SITE__;
  $script =
  <<<script
      $(document).ready(function() {
        /*
        $('#editor').wysiwyg({
          fontSizes: ['8px', '9px', '10px', '11px', '12px', '14px', '15px', '16px', '18px', '20px', '24px', '30px', '32px', '36px', '48px'],
          fontSizeDefault: '12px',
          fontFamilies: ['Open Sans', 'Arial', 'Arial Black', 'Courier', 'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times', 'Times New Roman', 'Verdana'],
          fontFamilyDefault: 'Open Sans',
          emojiDefault: ["\u{1f600}", "\u{1f62c}", "\u{1f601}", "\u{1f602}", "\u{1f603}", "\u{1f604}", "\u{1f605}", "\u{1f606}", "\u{1f607}", "\u{1f609}", "\u{1f60a}", "\u{1f642}", "\u{1f643}", "\u{1f60b}", "\u{1f60c}", "\u{1f60d}", "\u{1f618}", "\u{1f617}", "\u{1f619}", "\u{1f61a}", "\u{1f61c}", "\u{1f61d}", "\u{1f61b}", "\u{1f911}", "\u{1f913}", "\u{1f60e}", "\u{1f917}", "\u{1f60f}", "\u{1f636}", "\u{1f610}", "\u{1f611}", "\u{1f612}", "\u{1f644}", "\u{1f914}", "\u{1f633}", "\u{1f61e}", "\u{1f61f}", "\u{1f620}", "\u{1f621}", "\u{1f614}", "\u{1f615}", "\u{1f641}", "\u{1f623}", "\u{1f616}", "\u{1f62b}", "\u{1f629}", "\u{1f624}", "\u{1f62e}", "\u{1f631}", "\u{1f628}", "\u{1f630}", "\u{1f62f}", "\u{1f626}", "\u{1f627}", "\u{1f622}", "\u{1f625}", "\u{1f62a}", "\u{1f613}", "\u{1f62d}", "\u{1f635}", "\u{1f632}", "\u{1f910}", "\u{1f637}", "\u{1f912}", "\u{1f915}", "\u{1f634}", "\u{1f4a4}"],
          symbolsDefault: ["&lt;", "&gt;", "&laquo;", "&raquo;", "&lsaquo;", "&rsaquo;", "&quot;", "&prime;", "&Prime;", "&lsquo;", "&rsquo;", "&sbquo;", "&ldquo;", "&rdquo;", "&bdquo;", "&#10076;", "&#10075;", "&amp;", "&apos;", "&sect;", "&copy;", "&not;", "&reg;", "&macr;", "&deg;", "&plusmn;", "&sup1;", "&sup2;", "&sup3;", "&frac14;", "&frac12;", "&frac34;", "&acute;", "&micro;", "&para;", "&middot;", "&iquest;", "&fnof;", "&trade;", "&bull;", "&hellip;", "&oline;", "&ndash;", "&mdash;", "&permil;", "&#125;", "&#123;", "&#61;", "&ne;", "&cong;", "&asymp;", "&le;", "&ge;", "&ang;", "&perp;", "&radic;", "&sum;", "&int;", "&#8251;", "&divide;", "&infin;", "&#64;", "&#91;", "&#93;", "&larr;", "&uarr;", "&rarr;", "&darr;", "&harr;", "&crarr;", "&lArr;", "&uArr;", "&rArr;", "&dArr;", "&hArr;", "&#10144;", "&#10148;", "&#10149;", "&#10150;", "&#10163;", "&#8634;", "&#8635;", "&#8679;", "&#8617;", "&#11015;", "&#11014;", "&spades;", "&clubs;", "&hearts;", "&diams;", "&#9825;", "&#9826;", "&#9828;", "&#9831;", "&#8372;", "&euro;", "&dollar;", "&cent;", "&pound;", "&#8381;", "&yen;", "&#8377;", "&#22291;", "&#8376;"],
          colorPalette: [["rgb(0, 0, 0)","rgb(67, 67, 67)","rgb(102, 102, 102)","rgb(153, 153, 153)","rgb(183, 183, 183)","rgb(204, 204, 204)","rgb(217, 217, 217)","rgb(239, 239, 239)","rgb(243, 243, 243)","rgb(255, 255, 255)"],["rgb(152, 0, 0)","rgb(255, 0, 0)","rgb(255, 153, 0)","rgb(255, 255, 0)","rgb(0, 255, 0)","rgb(0, 255, 255)","rgb(74, 134, 232)","rgb(0, 0, 255)","rgb(153, 0, 255)","rgb(255, 0, 255)"],["rgb(230, 184, 175)","rgb(244, 204, 204)","rgb(252, 229, 205)","rgb(255, 242, 204)","rgb(217, 234, 211)","rgb(208, 224, 227)","rgb(201, 218, 248)","rgb(207, 226, 243)","rgb(217, 210, 233)","rgb(234, 209, 220)","rgb(221, 126, 107)","rgb(234, 153, 153)","rgb(249, 203, 156)","rgb(255, 229, 153)","rgb(182, 215, 168)","rgb(162, 196, 201)","rgb(164, 194, 244)","rgb(159, 197, 232)","rgb(180, 167, 214)","rgb(213, 166, 189)","rgb(204, 65, 37)","rgb(224, 102, 102)","rgb(246, 178, 107)","rgb(255, 217, 102)","rgb(147, 196, 125)","rgb(118, 165, 175)","rgb(109, 158, 235)","rgb(111, 168, 220)","rgb(142, 124, 195)","rgb(194, 123, 160)","rgb(166, 28, 0)","rgb(204, 0, 0)","rgb(230, 145, 56)","rgb(241, 194, 50)","rgb(106, 168, 79)","rgb(69, 129, 142)","rgb(60, 120, 216)","rgb(61, 133, 198)","rgb(103, 78, 167)","rgb(166, 77, 121)","rgb(133, 32, 12)","rgb(153, 0, 0)","rgb(180, 95, 6)","rgb(191, 144, 0)","rgb(56, 118, 29)","rgb(19, 79, 92)","rgb(17, 85, 204)","rgb(11, 83, 148)","rgb(53, 28, 117)","rgb(116, 27, 71)","rgb(91, 15, 0)","rgb(102, 0, 0)","rgb(120, 63, 4)","rgb(127, 96, 0)","rgb(39, 78, 19)","rgb(12, 52, 61)","rgb(28, 69, 135)","rgb(7, 55, 99)","rgb(32, 18, 77)","rgb(76, 17, 48)"]],
        });
        */
      });
  script;
  $postScripts[] = ['script' => $script];
?>