<?php use Core\Routing\Helper AS RouteHelper; ?>
<?php use Core\Support\HTML; ?>

<?php HTML::set('configNavTitle', 'Edit Week') ?>
<?php HTML::set('configBackgroundImage', __SITE__ . '/assets/img/sidebar-2.jpg'); ?>
<?php HTML::set('configSidebarUsername', $username); ?>
<?php HTML::set('configSidebarUsermail', $email); ?>
<?php HTML::set('configSidebarNavPage', RouteHelper::getLink('Speaking::weeks')); ?>

<div class="wrapper">

  <?php echo HTML::sidebar(); ?>

  <div class="main-panel">

    <?php echo HTML::navbar(); ?>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header card-header-rose card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">edit</i>
                  </div>
                  <h4 class="card-title">Week</h4>
                </div>
                <div class="card-body ">
                  <form method="POST" action="<?php RouteHelper::getLink('Speaking::weekForm') ?>">
                    <div class="form-group bmd-form-group">
                      <label for="week_title" class="bmd-label-floating">Title</label>
                      <input type="title" name="week[title]" class="form-control" id="week_title" value="<?php echo $week['title']; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="week_text" class="bmd-label-floating">Description</label>
                      <input type="text" name="week[text]" class="form-control" id="week_text" value="<?php echo $week['text']; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="week_created" class="bmd-label-floating">Created</label>
                      <input type="text" class="form-control disable" disabled id="week_created" value="<?php echo $week['created']; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="week_created_by" class="bmd-label-floating">Created by</label>
                      <input type="text" class="form-control disable" disabled id="week_created_by" value="<?php echo $week['created_user']; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="week_modified" class="bmd-label-floating">Modified</label>
                      <input type="text" class="form-control disable" disabled id="week_modified" value="<?php echo $week['modified'] ?? 'NaN'; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="week_modified_by" class="bmd-label-floating">Modified by</label>
                      <input type="text" class="form-control disable" disabled id="week_modified_by" value="<?php echo $week['modified_user'] ?? 'NaN'; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <label for="week_id" class="bmd-label-floating">ID</label>
                      <input type="text" name="week[id]" class="form-control disable" disabled id="week_id" value="<?php echo $week['id'] ?? 'NaN'; ?>">
                    </div>
                    <div class="form-group bmd-form-group">
                      <h5 class="title mb-0 mt-0">
                        <label for="week_icon" class="title bmd-label-floating">icon</label>
                      </h5>
                      <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail img-circle">
                          <img src="/assets/img/placeholder.jpg" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
                        <div>
                          <span class="btn btn-round btn-rose btn-file">
                            <span class="fileinput-new">Add ICON</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="..." />
                          </span>
                          <br />
                          <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                        </div>
                      </div>
                    </div>
                    <div class="form-group bmd-form-group">
                      <h5 class="title mb-0 mt-0">
                        <label for="week_icon" class="title bmd-label-floating">Status</label>
                      </h5>
                      <select name="week[status]" class="selectpicker" data-size="4" data-style="btn btn-primary btn-round" title="status state">
                        <option value="1"<?php echo ($week['status'] == 1) ? ' selected':''; ?>>Enabled</option>
                        <option value="0"<?php echo ($week['status'] == 0) ? ' selected':''; ?>>Disabled</option>
                        <option value="-1"<?php echo ($week['status'] == -1) ? ' selected':''; ?>>Archived</option>
                      </select>
                    </div>
                    <div class="form-group bmd-form-group">
                      <button type="submit" class="btn btn-fill btn-rose">Submit</button>
                    </div>
                    <input type="hidden" name="week[id]" value="<?php echo $week['id']; ?>">
                    <input type="hidden" name="sessions" value="<?php echo $session; ?>">
                  </form>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>

    <?php echo HTML::footer(); ?>

  </div>
</div>
<?php
  $site = __SITE__;
  $script =
<<<script
    $(document).ready(function() {

    });
script;
  $postScripts[] = ['script' => $script];
?>