<?php use Core\Routing\Helper AS RouteHelper; ?>
<?php use Core\Support\HTML; ?>

<?php HTML::set('configNavTitle', 'Speaking dashboard') ?>
<?php HTML::set('configBackgroundImage', __SITE__ . '/assets/img/sidebar-1.jpg'); ?>
<?php HTML::set('configSidebarUsername', $username); ?>
<?php HTML::set('configSidebarUsermail', $email); ?>
<?php HTML::set('configSidebarNavPage', RouteHelper::getLink('Speaking::dashboard')); ?>

<div class="wrapper">

  <?php echo HTML::sidebar(); ?>

  <div class="main-panel">

    <?php echo HTML::navbar(); ?>

    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 ml-auto mr-auto">
            <div class="page-categories">
              <h3 class="title text-center">Speaking</h3>
              <br>
              <ul class="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center" id="tablist" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active show" data-toggle="tab" href="#tabWeeks" role="tablist">
                    <i class="material-icons">view_week</i> Weeks
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#tabDays" role="tablist">
                    <i class="material-icons">article</i> Days
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#tabPractices" role="tablist">
                    <i class="material-icons">list</i> Practices
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#tabBonuses" role="tablist">
                    <i class="material-icons">question_answer</i> Bonuses
                  </a>
                </li>
              </ul>
              <div class="tab-content tab-space tab-subcategories">
                <div class="tab-pane active show" id="tabWeeks">

                  <div class="row">
                    <div class="col-md-12">
                      <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                          <a href="<?php echo RouteHelper::getLink('Speaking::weekNew') ?>" class="card-icon btn-primary">
                            <i class="material-icons">add</i>
                          </a>
                          <h4 class="card-title">
                            Add week
                          </h4>
                        </div>
                        <div class="card-body">
                          <div class="toolbar">List of Weeks</div>
                          <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>Title</th>
                                  <th>Icon</th>
                                  <th>Created</th>
                                  <th class="disabled-sorting text-right">Action</th>
                                </tr>
                              </thead>
                              <tfoot>
                                <tr>
                                  <th>ID</th>
                                  <th>Title</th>
                                  <th>Icon</th>
                                  <th>Created</th>
                                  <th class="text-right">Action</th>
                                </tr>
                              </tfoot>
                              <tbody>
                                <?php foreach ($weeks as $key => $week): ?>
                                  <tr>
                                    <td><?php echo $week['id']; ?></td>
                                    <td><?php echo $week['title']; ?></td>
                                    <td><?php echo $week['icon']; ?></td>
                                    <td><?php echo $week['created']; ?></td>
                                    <td class="text-right">
                                      <a href="<?php echo RouteHelper::getLink('Speaking::weekForm'); ?>?id=<?php echo $week['id']; ?>&session=<?php echo $session; ?>" class="btn btn-link btn-success btn-just-icon to-word"><i class="material-icons">create</i></a>
                                      <?php if ($week['status'] == 0): ?>
                                        <a href="#" title="click to enable" class="btn btn-link btn-default btn-just-icon" data-click="visibility-off" data-id="<?php echo $week['id'] ?>"><i class="material-icons">visibility_off</i></a>
                                      <?php elseif($week['status'] == -1): ?>
                                        <a href="#" title="click to disable" class="btn btn-link btn-primary btn-just-icon" data-click="visibility" data-id="<?php echo $week['id'] ?>"><i class="material-icons">archive</i></a>
                                      <?php else: ?>
                                        <a href="#" title="click to disable" class="btn btn-link btn-warning btn-just-icon" data-click="visibility" data-id="<?php echo $week['id'] ?>"><i class="material-icons">visibility</i></a>
                                      <?php endif ?>
                                      <a href="#" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">delete</i></a>
                                    </td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="tab-pane" id="tabDays">

                  <div class="row">
                    <div class="col-md-12">
                      <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                          <a href="<?php echo RouteHelper::getLink('Speaking::dayNew') ?>" class="card-icon btn-primary">
                            <i class="material-icons">add</i>
                          </a>
                          <h4 class="card-title">
                            Add day
                          </h4>
                        </div>
                        <div class="card-body">
                          <div class="toolbar">List of Days</div>
                          <div class="material-datatables">
                            <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>Week</th>
                                  <th>Title</th>
                                  <th>Icon</th>
                                  <th>Color</th>
                                  <th>Created</th>
                                  <th class="disabled-sorting text-right">Action</th>
                                </tr>
                              </thead>
                              <tfoot>
                                <tr>
                                  <th>ID</th>
                                  <th>Week</th>
                                  <th>Title</th>
                                  <th>Icon</th>
                                  <th>Color</th>
                                  <th>Created</th>
                                  <th class="text-right">Action</th>
                                </tr>
                              </tfoot>
                              <tbody>
                                <?php foreach ($days as $key => $day): ?>
                                  <tr>
                                    <td><?php echo $day['id']; ?></td>
                                    <td><?php echo $day['week_title']; ?></td>
                                    <td><?php echo $day['title']; ?></td>
                                    <td><?php echo $day['icon']; ?></td>
                                    <td><?php echo $day['color']; ?></td>
                                    <td><?php echo $day['created']; ?></td>
                                    <td class="text-right">
                                      <a href="<?php echo RouteHelper::getLink('Speaking::dayForm'); ?>?id=<?php echo $day['id']; ?>&session=<?php echo $session; ?>" class="btn btn-link btn-success btn-just-icon to-word"><i class="material-icons">create</i></a>
                                      <?php if ($day['status'] == 0): ?>
                                        <a href="#" title="click to enable" class="btn btn-link btn-default btn-just-icon" data-click="visibility-off" data-id="<?php echo $day['id'] ?>"><i class="material-icons">visibility_off</i></a>
                                      <?php elseif($day['status'] == -1): ?>
                                        <a href="#" title="click to disable" class="btn btn-link btn-primary btn-just-icon" data-click="visibility" data-id="<?php echo $day['id'] ?>"><i class="material-icons">archive</i></a>
                                      <?php else: ?>
                                        <a href="#" title="click to disable" class="btn btn-link btn-warning btn-just-icon" data-click="visibility" data-id="<?php echo $day['id'] ?>"><i class="material-icons">visibility</i></a>
                                      <?php endif ?>
                                      <a href="#" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">delete</i></a>
                                    </td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="tab-pane" id="tabPractices">

                  <div class="row">
                    <div class="col-md-12">
                      <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                          <a href="<?php echo RouteHelper::getLink('Speaking::dayNew') ?>" class="card-icon btn-primary">
                            <i class="material-icons">add</i>
                          </a>
                          <h4 class="card-title">
                            Add practice
                          </h4>
                        </div>
                        <div class="card-body">
                          <div class="toolbar">List of Days</div>
                          <div class="material-datatables">
                            <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>Week</th>
                                  <th>Day</th>
                                  <th>Title</th>
                                  <th>Description</th>
                                  <th>Icon</th>
                                  <th>Color</th>
                                  <th>Created</th>
                                  <th class="disabled-sorting text-right">Action</th>
                                </tr>
                              </thead>
                              <tfoot>
                                <tr>
                                  <th>ID</th>
                                  <th>Week</th>
                                  <th>Day</th>
                                  <th>Title</th>
                                  <th>Icon</th>
                                  <th>Color</th>
                                  <th>Created</th>
                                  <th class="text-right">Action</th>
                                </tr>
                              </tfoot>
                              <tbody>
                                <?php foreach ($practices as $key => $practice): ?>
                                  <tr>
                                    <td><?php echo $practice['id']; ?></td>
                                    <td><?php echo $practice['week_title']; ?></td>
                                    <td><?php echo $practice['day_title']; ?></td>
                                    <td><?php echo $practice['title']; ?></td>
                                    <td><?php echo $practice['text']; ?></td>
                                    <td><?php echo $practice['icon']; ?></td>
                                    <td><?php echo $practice['color']; ?></td>
                                    <td><?php echo $practice['created']; ?></td>
                                    <td class="text-right">
                                      <a href="<?php echo RouteHelper::getLink('Speaking::dayForm'); ?>?id=<?php echo $practice['id']; ?>&session=<?php echo $session; ?>" class="btn btn-link btn-success btn-just-icon to-word"><i class="material-icons">create</i></a>
                                      <?php if ($practice['status'] == 0): ?>
                                        <a href="#" title="click to enable" class="btn btn-link btn-default btn-just-icon" data-click="visibility-off" data-id="<?php echo $practice['id'] ?>"><i class="material-icons">visibility_off</i></a>
                                      <?php elseif($practice['status'] == -1): ?>
                                        <a href="#" title="click to disable" class="btn btn-link btn-primary btn-just-icon" data-click="visibility" data-id="<?php echo $practice['id'] ?>"><i class="material-icons">archive</i></a>
                                      <?php else: ?>
                                        <a href="#" title="click to disable" class="btn btn-link btn-warning btn-just-icon" data-click="visibility" data-id="<?php echo $practice['id'] ?>"><i class="material-icons">visibility</i></a>
                                      <?php endif ?>
                                      <a href="#" class="btn btn-link btn-danger btn-just-icon remove"><i class="material-icons">delete</i></a>
                                    </td>
                                  </tr>
                                <?php endforeach ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="tab-pane" id="tabBonuses">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Help center</h4>
                      <p class="card-category">
                        More information here
                      </p>
                    </div>
                    <div class="card-body">
                      From the seamless transition of glass and metal to the streamlined profile, every detail was carefully considered to enhance your experience. So while its display is larger, the phone feels just right.
                      <br>
                      <br> Another Text. The first thing you notice when you hold the phone is how great it feels in your hand. The cover glass curves down around the sides to meet the anodized aluminum enclosure in a remarkable, simplified design.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php echo HTML::footer(); ?>

  </div>
</div>
<?php
  $site = __SITE__;
  $script =
<<<script
    $(document).ready(function() {
      $('.datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        \$tr = $(this).closest('tr');
        var data = table.row(\$tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        \$tr = $(this).closest('tr');
        table.row(\$tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });

      table.on('click', '.to-word', function() {
        // alert('You clicked on Like button');
      });

      table.on('click', '.to-export', function() {
        var btn = $(this);
            btn.toggleClass('btn-info btn-warning word-selected');
        var elem = btn.find('i');
        if( elem.text() == 'add') {
          elem.text('close');
        } else {
          elem.text('add');
        }
      });
      $('#to-word').click(function(){
        var keys = '';
        $('.word-selected').each(function(){
          var elem = $(this);
          keys+=','+elem.data('id');
        });
        var url = '['+keys.substring(1)+']';
        $('#form-word-id').val(url).parent().submit();
        return;
        var link = '$site/export/word?id=[' + keys.substring(1) + ']';
        var popup = window.open(link,'Popup','left=10000,top=10000,width=0,height=0');
        // popup.close();
        $(popup).load(function(){
          this.opener.postMessage({'loaded': true}, "*");
          this.close();
      });
      });

      var hash = document.location.hash;
      if (hash.length > 0) {
        var tab = jQuery('a[href="'+hash+'"');
        if ( tab ) {
          tab.tab('show');
        }
      }
    });
script;
  $postScripts[] = ['script' => $script];
?>