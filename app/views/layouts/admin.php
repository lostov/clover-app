<!DOCTYPE html>
<html lang="en-GB">
<head>
	<meta charset="UTF-8">
	<title>Admin | <?php echo htmlspecialchars($baliseTitle); ?></title>
	<meta name="description" content="<?php echo htmlspecialchars($metaDescription); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
	<![endif]-->
</head>
<body>
	<header>

	</header>

	<?php echo $contentInLayout;  // View; ?>

	<footer>

	</footer>
</body>
</html>