<!DOCTYPE html>
<html lang="uz-UZ">
<head>
	<meta charset="UTF-8">
	<title><?php echo htmlspecialchars($title); echo (strlen($title) > 0) ? ' | ' : '';  echo htmlspecialchars($baliseTitle); ?></title>
	<meta name="description" content="<?php echo htmlspecialchars($metaDescription); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="icon" href="/assets/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="/assets/fonts/all-fonts.css" />
	<link rel="stylesheet" type="text/css" href="/assets/fonts/font-awesome/css/font-awesome.min.css">
	<?php if (isset($stylesheets)): ?>
		<?php foreach ($stylesheets as $key => $stylesheet): ?>
			<?php if (isset($stylesheet['href'])): ?>
				<link href="<?=$stylesheet['href'];?>" rel="stylesheet" />
			<?php elseif (isset($stylesheet['style'])): ?>
				<style>
					<?=$stylesheet['style'];?>
				</style>
			<?php endif ?>
		<?php endforeach ?>
	<?php endif ?>
	<?php if (isset($preScripts)): ?>
		<?php foreach ($preScripts as $key => $script): ?>
			<?php if (isset($script['src'])): ?>
				<script src="<?=$script['src'];?>"></script>
			<?php elseif (isset($script['script'])): ?>
				<script>
					<?=$script['script'];?>
				</script>
			<?php endif ?>
		<?php endforeach ?>
	<?php endif ?>
</head>
<body>
	<?php echo $contentInLayout; ?>

	<?php if (isset($postScripts)): ?>
		<?php foreach ($postScripts as $key => $script): ?>
			<?php if (isset($script['src'])): ?>
				<script src="<?=$script['src'];?>"></script>
			<?php elseif (isset($script['script'])): ?>
				<script>
					<?=$script['script'];?>
				</script>
			<?php endif ?>
		<?php endforeach ?>
	<?php endif ?>
</body>
</html>