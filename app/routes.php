<?php

use Core\Routing\{Router, Helper AS RouteHelper};

/**
 * Список маршрутов
 */

$router = Router::getInstance();

$router->add('', 'Home::show');
$router->add('api/user/login', 'MainApi::index');
$router->add('api/user/registration', 'MainApi::registration');
$router->add('api/app/slides', 'MainApi::slides');
$router->add('api/app/items', 'MainApi::items');
$router->add('api/app/listening', 'Api::listening');
$router->add('api/app/reading', 'Api::reading');
$router->add('api/app/writing', 'Api::writing');
$router->add('api/app/speaking', 'Api::speaking');
$router->add('api/app/vocabulary', 'Api::vocabulary');
$router->add('api/app/tips', 'ListeningApi::tips');
$router->add('api/media', 'Api::media');

$router->group('clover/listening', [
	'' => 'Listening::dashboard',
	'' => 'Listening::weeks',
	'/weeks' => 'Listening::weeks',
	'/week/add' => 'Listening::weekNew',
	'/week/form' => 'Listening::weekForm',
	'/week/remove' => 'Listening::weekRemove',

	'/days' => 'Listening::days',
	'/day/add' => 'Listening::dayNew',
	'/day/form' => 'Listening::dayForm',
	'/day/remove' => 'Listening::dayRemove',

	'/practices' => 'Listening::practices',
	'/practice/add' => 'Listening::practiceNew',
	'/practice/form' => 'Listening::practiceForm',
	'/practice/remove' => 'Listening::practiceRemove',
]);

$router->group('clover/reading', [
	'' => 'Reading::dashboard',
	'' => 'Reading::weeks',
	'/weeks' => 'Reading::weeks',
	'/week/add' => 'Reading::weekNew',
	'/week/form' => 'Reading::weekForm',
	'/week/remove' => 'Reading::weekRemove',

	'/days' => 'Reading::days',
	'/day/add' => 'Reading::dayNew',
	'/day/form' => 'Reading::dayForm',
	'/day/remove' => 'Reading::dayRemove',

	'/practices' => 'Reading::practices',
	'/practice/add' => 'Reading::practiceNew',
	'/practice/form' => 'Reading::practiceForm',
	'/practice/remove' => 'Reading::practiceRemove',
]);

$router->group('clover/writing', [
	'' => 'Writing::dashboard',
	'' => 'Writing::weeks',
	'/weeks' => 'Writing::weeks',
	'/week/add' => 'Writing::weekNew',
	'/week/form' => 'Writing::weekForm',
	'/week/remove' => 'Writing::weekRemove',

	'/days' => 'Writing::days',
	'/day/add' => 'Writing::dayNew',
	'/day/form' => 'Writing::dayForm',
	'/day/remove' => 'Writing::dayRemove',

	'/practices' => 'Writing::practices',
	'/practice/add' => 'Writing::practiceNew',
	'/practice/form' => 'Writing::practiceForm',
	'/practice/remove' => 'Writing::practiceRemove',
]);

$router->group('clover/speaking', [
	'' => 'Speaking::dashboard',
	'' => 'Speaking::weeks',
	'/weeks' => 'Speaking::weeks',
	'/week/add' => 'Speaking::weekNew',
	'/week/form' => 'Speaking::weekForm',
	'/week/remove' => 'Speaking::weekRemove',

	'/days' => 'Speaking::days',
	'/day/add' => 'Speaking::dayNew',
	'/day/form' => 'Speaking::dayForm',
	'/day/remove' => 'Speaking::dayRemove',

	'/practices' => 'Speaking::practices',
	'/practice/add' => 'Speaking::practiceNew',
	'/practice/form' => 'Speaking::practiceForm',
	'/practice/remove' => 'Speaking::practiceRemove',
]);
/*
$router->group('clover/reading', [
	'' => 'Reading::dashboard',
	'/weeks' => 'Reading::weeks',
	'/week/add' => 'Reading::weekNew',
	'/week/form' => 'Reading::weekForm',
	'/week/remove' => 'Reading::weekRemove',
	'/days' => 'Reading::days',
	'/day/add' => 'Reading::dayNew',
	'/day/form' => 'Reading::dayForm',
	'/day/remove' => 'Reading::dayRemove',
	'/items' => 'Reading::items',
	'/item/form' => 'Reading::itemForm',
	'/item/remove' => 'Reading::itemRemove',
]);
*/
$router->group('system', [
	'/login' => 'Auth::login',
	'/logout' => 'Auth::logout',
	'/settings/account' => 'Settings::accountSettings',
	'/settings' => 'Settings::settings',
	'/media/getfile' => 'Media::get',
	'/media/getlink' => 'Media::link',
	'/media/manager' => 'Media::manager',
	'/media/files' => 'Media::files',
]);

// $router->add('system/login', 'Auth::login');
// $router->add('system/logout', 'Auth::logout');

$router->add('articles', 'Article::index');
$router->add('articles/id', 'Article::article', ['id']);


RouteHelper::importRoute($router->getRoutes());
RouteHelper::importMenus([
	[
		'action' => RouteHelper::getLink('Home::show'),
		'icon'   => 'dashboard',
		'title'  => 'Dashboard',
		'access' => 1,
		'status' => true
	],
	[
		'action' => RouteHelper::getLink('Listening::weeks'),
		'icon'   => 'hearing',
		'title'  => 'Listening',
		'access' => 1,
		'status' => true
	],
	[
		'action' => RouteHelper::getLink('Reading::weeks'),
		'icon'   => 'chrome_reader_mode',
		'title'  => 'Reading',
		'access' => 1,
		'status' => true
	],
	[
		'action' => RouteHelper::getLink('Writing::weeks'),
		'icon'   => 'edit',
		'title'  => 'Writing',
		'access' => 1,
		'status' => true
	],
	[
		'action' => RouteHelper::getLink('Speaking::weeks'),
		'icon'   => 'speaker_phone',
		'title'  => 'Speaking',
		'access' => 1,
		'status' => true
	],
	[
		'action' => '/clover/vocabulary',
		'icon'   => 'menu_book',
		'title'  => 'Vocabulary',
		'access' => 1,
		'status' => true
	],
	[
		'action' => '/clover/settings',
		'icon'   => 'settings_applications',
		'title'  => 'App Settings',
		'access' => 1,
		'status' => true
	],
	[
		'action' => '/system/settings',
		'icon'   => 'settings',
		'title'  => 'Settings',
		'access' => 1,
		'status' => true
	],
	[
		'action' => '/system/logs',
		'icon'   => 'date_range',
		'title'  => 'Logs',
		'access' => 1,
		'status' => true
	],
	[
		'action' => 'a',
		'icon'   => 'dashboard',
		'title'  => 'Dashboard',
		'status' => false
	]
]);