<?php

namespace App\Controllers;

use App\Models\User;
use \Core\Http\{Response,Input,Session};

class AuthController extends Controller
{
    /**
     * tizimga kirish method
     */
    public function login()
    {
        if (Input::hasPost('auth')) {
            $auth = Input::post('auth', null, 'object');
            if (isset($auth->session) && Session::checkToken($auth->session)) {
                $user = User::auth($auth->username,$auth->password);
                if(!$user){
                    $error = ['message'=>'Login yoki Parol xato kiritilgan'];
                    $status = false;
                } else {
                    $status = $user;
                    $error = [];
                }
            }
        } else {
            $auth = [];
        }
        if ((int) Session::get('user') > 0) {
            Response::redirect('../');
        }
        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/demo/demo.css"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/demo/demo.js"];
        $postScripts[] = ["script" => "$(document).ready(function() { md.checkFullPageBackgroundImage(); setTimeout(function() { $('.card').removeClass('card-hidden'); }, 700); });"];
        // $model = parent::getModel("Article");
        return $this->useLayout("front")->view(
            "specific-page/login",
            [
                "title" => "Login",
                "baliseTitle" => "Login to system",
                "metaDescription" => "Nothing to describe",
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "auth" => $auth,
                "error" => $error,
                "status" => $status,
                "session" => Session::getToken()
            ]
        );
    }

    public function logout()
    {
        Session::destroy();
        Response::redirect('login');
    }
}
