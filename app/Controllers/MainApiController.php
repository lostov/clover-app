<?php

namespace App\Controllers;
use App\Models\{User,Config,Log,Slides};
use \Core\Http\{Input AS I, Response AS Res, Request AS Req};

/**
 * Управление контентом
 */
class MainApiController extends Api
{
    /**
     * Methods
     */
    public function index()
    {
        return $this->json(
            [
                'headers' => Res::getAllHeaders(),
                'baliseTitle' => 'Article listing title',
                'metaDescription' => 'Article listing desciption',
                'file' => __DIR__,
            ]
        );  
    }

    public function slides()
    {
        $data = Slides::get();
        $code = 204;
        $message = "Empty result";
        foreach ($data as $key => $slide) {
            unset($data[$key]['user']);
            unset($data[$key]['date']);
            unset($data[$key]['status']);
            unset($data[$key]['category']);
        }
        if (count($data) > 0) {
            $message = "All items of slider";
            $code = 200;
        }
        return $this->json(
            [
                // 'headers' => Res::getAllHeaders(),
                'code' => $code,
                'message' => $message,
                'data' => $data,
            ]
        );  
    }

    public function items()
    {
        $data = Config::getMainItems();
        $code = 204;
        $message = "Empty result";
        if (count($data) > 0) {
            $message = "All items of Main menu";
            $code = 200;
        }
        return $this->json(
            [
                'code' => $code,
                'message' => $message,
                'data' => $data,
            ]
        );
    }

    public function media()
    {
        return $this->getMedia(
            [
                'Content-Disposition: inline;filename="/source/mp3.mp3"'
            ]
        );
    }

    public function user()
    {
        return $this->view('article/index', [
            'baliseTitle' => 'Article listing title',
            'metaDescription' => 'Article listing desciption',
        ]);  
    }
}
