<?php

namespace App\Controllers;
use App\Models\{User,Reading,Config,Media};
use \Core\Http\{Input,Request,Response,Session};
use \Core\Routing\Helper as RouteHelper;

class ReadingController extends Controller
{
    public function dashboard()
    {
        // Session::regenerate();

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $weeks = Reading::getWeeks();
        $days = Reading::getDays();
        $practices = Reading::getPractices();

        return $this->auth()->useLayout("front")->view(
            'reading/dashboard',
            [
                'title' => 'Dashboard',
                'baliseTitle' => 'Reading dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "weeks" => $weeks,
                "days" => $days,
                "practices" => $practices,
                "bonuses" => [],
                "session" => Session::getToken()
            ]
        );
    }

    public function weeks()
    {
        // Session::regenerate();

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $weeks = Reading::getWeeks();

        return $this->auth()->useLayout("front")->view(
            'reading/dashboardWeeks',
            [
                'title' => 'Weeks',
                'baliseTitle' => 'Reading dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "weeks" => $weeks,
                "session" => Session::getToken()
            ]
        );
    }

    public function days()
    {
        // Session::regenerate();

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $week = Input::get('week',0,'int');
        $days = Reading::getDays($week);

        return $this->auth()->useLayout("front")->view(
            'reading/dashboardDays',
            [
                'title' => 'Days',
                'baliseTitle' => 'Reading dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "week" => $week,
                "days" => $days,
                "session" => Session::getToken()
            ]
        );
    }

    public function practices()
    {
        // Session::regenerate();

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $day = Input::get('day',0,'int');
        $practices = Reading::getPractices($day);

        return $this->auth()->useLayout("front")->view(
            'reading/dashboardPractices',
            [
                'title' => 'Practices',
                'baliseTitle' => 'Reading dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "day" => $day,
                "practices" => $practices,
                "session" => Session::getToken()
            ]
        );
    }

    public function weekForm() {
    	// Session::regenerate();

        $session = '';
        if (Input::hasGet('session')) {
            $session = Input::get('session', null, 'string');
        }
        if (!Session::checkToken($session)) {
            Response::redirect(RouteHelper::getLink('Reading::weeks'));
        }

        if (Input::hasPost('week')) {
            if (Input::hasPost('session')) {
                $session = Input::post('session', null, 'string');
            }
            $check = Session::checkToken($session);
            if ($check) {
                $week = Input::post('week', null, 'object');
                $object = $week;
                $result = Reading::weekForm($week);
                if ($result) {
                	Response::redirect(RouteHelper::getLink('Reading::weeks'));
                }
            }
            Response::redirect(RouteHelper::getLink('Reading::weeks'));
        }
        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $week = Reading::getWeek(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'reading/weekForm',
            [
                'title' => 'Edit a week',
                'baliseTitle' => 'Reading dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "week" => $week,
                "session" => Session::getToken()
            ]
        );
    }

    public function weekNew() {
        // Session::regenerate();


        $session = '';
        if (Input::hasRequest('session')) {
            $session = Input::request('session', null, 'string');
        }
        if (!Session::checkToken($session)) {
            Response::redirect(RouteHelper::getLink('Reading::weeks'));
        }


        if (Input::hasPost('week')) {
            $week = Input::post('week', null, 'object');
            $result = Reading::weekNew($week);
            if ($result) {
            	Response::redirect(RouteHelper::getLink('Reading::weeks'));
            }
        }

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $week = Reading::getWeek(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'reading/weekNew',
            [
                'title' => 'Add a week',
                'baliseTitle' => 'Reading dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "object" => $object,
                "result" => $result,
                "email" => $email,
                "session" => Session::getToken()
            ]
        );
    }

    public function dayForm() {
        // Session::regenerate();
        // if ( !Session::checkToken(Input::get('session', null, 'string')) ) {
        //     Response::redirect(RouteHelper::getLink('Reading::days'));
        // }

        if (Input::hasPost('day')) {
            $day = Input::post('day', null, 'object');
            $object = $day;
            $result = Reading::dayForm($day);
            if ($result) {
            	Response::redirect(RouteHelper::getLink('Reading::days'));
            }
        }
        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];
        // $stylesheets[] = ["href" => __SITE__ . "/assets/js/jsmodules/dist/css/highlight.min.css"];
        // $stylesheets[] = ["href" => __SITE__ . "/assets/js/jsmodules/dist/css/wysiwyg.min.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        // $postScripts[] = ["src" => __SITE__ . "/assets/js/jsmodules/dist/js/highlight.min.js"];
        // $postScripts[] = ["src" => __SITE__ . "/assets/js/jsmodules/dist/js/wysiwyg.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $weeks = Reading::optionsWeeks();
        $day = Reading::getDay(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'reading/dayForm',
            [
                'title' => 'Edit a day',
                'baliseTitle' => 'Reading dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "weeks" => $weeks,
                "colors" => Config::optionsColors(),
                "day" => $day,
                "session" => Session::getToken()
            ]
        );
    }

    public function dayNew() {
        // Session::regenerate();
        $session = '';
        if (Input::hasGet('session')) {
            $session = Input::get('session', null, 'string');
        } elseif (Input::hasPost('session')) {
            $session = Input::post('session', null, 'string');
        }
        if (!Session::checkToken($session)) {
            Response::redirect(RouteHelper::getLink('Reading::days'));
        }
        if (Input::hasPost('day')) {
            $day = Input::post('day', null, 'object');
            $result = Reading::dayNew($day);
            if ($result) {
            	Response::redirect(RouteHelper::getLink('Reading::days'));
            }
        }

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $week = Input::get('week', 0, 'int');
        $color = Input::get('color', 0, 'int');
        $weeks = Reading::optionsWeeks();
        $day = Reading::getDay(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'reading/dayNew',
            [
                'title' => 'Add a day',
                'baliseTitle' => 'Reading Dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "weeks" => $weeks,
                "weekDay" => $week,
                "colorDay" => $color,
                "colors" => Config::optionsColors(),
                "result" => $result,
                "email" => $email,
                "session" => Session::getToken()
            ]
        );
    }

    public function practiceForm() {
        Session::regenerate();

        if (Input::hasPost('practice')) {
            $practice = Input::post('practice', null, 'object');
            $object = $practice;
            $result = Reading::practiceForm($practice);
            if ($result) {
                Response::redirect(RouteHelper::getLink('Reading::practices'));
            }
        }
        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $days = Reading::allDays();
        $media = Media::getMedia();
        $practice = Reading::getPractice(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'reading/practiceForm',
            [
                'title' => 'Edit practice',
                'baliseTitle' => 'Reading dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "practice" => $practice,
                "days" => $days,
                "media" => $media,
                "colors" => Config::optionsColors(),
                "session" => Session::getToken()
            ]
        );
    }

    public function practiceNew() {
        Session::regenerate();

        if (Input::hasPost('day')) {
            $day = Input::post('practice', null, 'object');
            $result = Reading::practiceNew($practice);
            if ($result) {
                Response::redirect(RouteHelper::getLink('Reading::practices'));
            }
        }

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $practice = Reading::getPractice(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'reading/practiceNew',
            [
                'title' => 'New practice',
                'baliseTitle' => 'Reading dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "result" => $result,
                "email" => $email,
                "session" => Session::getToken()
            ]
        );
    }
}
