<?php

namespace App\Controllers;
use App\Models\{User AS CUser,Config AS CloverConfig,Log AS CLog,Slides AS CloverSlides};
use \Core\Http\{Input AS I, Response AS CResponse, Request AS Req};

/**
 * Управление контентом
 */
class ApiController extends Api
{
    /**
     * Methods
     */
    public function index()
    {
        return $this->json(
            [
                'headers' => CResponse::getAllHeaders(),
                'baliseTitle' => 'Article listing title',
                'metaDescription' => 'Article listing desciption',
                'file' => __DIR__,
            ]
        );  
    }

    public function slides()
    {
        $data = CloverSlides::get();
        $code = 204;
        $message = "Empty result";
        foreach ($data as $key => $slide) {
            unset($data[$key]['user']);
            unset($data[$key]['date']);
            unset($data[$key]['status']);
            unset($data[$key]['category']);
        }
        if (count($data) > 0) {
            $message = "All items of slider";
            $code = 200;
        }
        return $this->json(
            [
                // 'headers' => CResponse::getAllHeaders(),
                'code' => $code,
                'message' => $message,
                'data' => $data,
            ]
        );  
    }

    public function items()
    {
        $data = CloverConfig::getMainItems();
        $code = 204;
        $message = "Empty result";
        if (count($data) > 0) {
            $message = "All items of Main menu";
            $code = 200;
        }
        return $this->json(
            [
                'code' => $code,
                'message' => $message,
                'data' => $data,
            ]
        );
    }

    public function media()
    {
        return $this->getMedia(
            [
                'Content-Disposition: inline;filename="/source/mp3.mp3"'
            ]
        );
    }

    public function listening()
    {
        $data = [
            'user' => CUser::checkUserToken()
        ];
        $code = 200;
        $message = "Empty result";
        return $this->json([
            'message' => $message,
            'code' => $code,
            'data' => $data
        ]);            
    }

    public function user()
    {
        return $this->view('article/index', [
            'baliseTitle' => 'Article listing title',
            'metaDescription' => 'Article listing desciption',
        ]);  
    }
}
