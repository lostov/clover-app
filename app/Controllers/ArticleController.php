<?php

namespace App\Controllers;

/**
 * Управление контентом
 */
class ArticleController extends Controller
{
    /**
     * Распечатка статей
     */
    public function index()
    {
        return $this->view('article/index', [
            'baliseTitle' => 'Article listing title',
            'metaDescription' => 'Article listing desciption',
        ]);  
    }
    public function article($id = 0)
    {
        return $this->view('article/index', [
            'baliseTitle' => 'Article listing title',
            'metaDescription' => 'Article listing desciption',
        ]);  
    }
}
