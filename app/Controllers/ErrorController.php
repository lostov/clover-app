<?php

namespace App\Controllers;

/**
 * Управление ошибками
 */
class ErrorController extends Controller
{
    /**
     * Ошибка HTTP 404
     */
    public function show404()
    {
        $this->header('HTTP/1.0 404 Not Found');

        $stylesheets = $postScripts = [];
        $stylesheets[] = ['href' => __SITE__ . '/assets/css/material-dashboard.css'];
        $stylesheets[] = ['href' => __SITE__ . '/assets/demo/demo.css'];

        return $this->useLayout('front')->view(
            'error/404',
            [
                'title' => 'Oops',
                'baliseTitle' => 'Error 404 title',
                'metaDescription' => 'Error 404 desciption',
                'stylesheets' => $stylesheets,
                'postScripts' => $postScripts
            ]
        );  
    }
}
