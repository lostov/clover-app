<?php

namespace App\Controllers;
use App\Models\{User, Listening};
use \Core\Http\{Input,Request,Response,Session};
use \Core\Routing\Helper as RouteHelper;

/**
 * Управление домашней страницей
 */
class MediaController extends Controller
{
    public function get()
    {
        $filename = $_GET['file'];

        // required for IE, otherwise Content-disposition is ignored
        if( ini_get('zlib.output_compression') ) {
          ini_set('zlib.output_compression', 'Off');
        }

        $dir = __ROOT__ . DS . "source" . DS;
        $file_extension = strtolower(substr(strrchr($filename,"."),1));
        $dir_file = $dir . $filename;
        if( $dir_file == "" )
        {
          echo "<html><title>eLouai's Download Script</title><body>ERROR: download file NOT SPECIFIED. USE force-download.php?file=filepath</body></html>";
          exit;
        } elseif ( ! file_exists( $dir_file ) )
        {
          echo "<html><title>eLouai's Download Script</title><body>ERROR: File not found. USE force-download.php?file=filepath {$dir_file}</body></html>";
          exit;
        };
        switch( $file_extension )
        {
          case 'mp3':
          header('Content-type: audio/mpeg');
          header("Content-Transfer-Encoding: binary");
          $ctype= "audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3";
          break;
          case "pdf": $ctype="application/pdf"; break;
          case "exe": $ctype="application/octet-stream"; break;
          case "zip": $ctype="application/zip"; break;
          case "doc": $ctype="application/msword"; break;
          case "xls": $ctype="application/vnd.ms-excel"; break;
          case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
          case "gif": $ctype="image/gif"; break;
          case "png": $ctype="image/png"; break;
          case "jpeg":
          case "jpg": $ctype="image/jpg"; break;
          default: $ctype="application/force-download";
        }
        header("Pragma: public"); // required
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: $ctype");
        header("Content-Disposition: attachment; filename=\"".basename($dir_file)."\";" );
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".filesize($dir_file));
        readfile("$dir_file");
        exit();
    }

    public function link()
    {
        $filename = $_GET['file'];

        // required for IE, otherwise Content-disposition is ignored
        if( ini_get('zlib.output_compression') ) {
          ini_set('zlib.output_compression', 'Off');
        }

        $dir = __ROOT__ . DIRECTORY_SEPARATOR . "source" . DIRECTORY_SEPARATOR;
        $file_extension = strtolower(substr(strrchr($filename,"."),1));
        $dir_file = $dir . $filename;
        if( $dir_file == "" )
        {
          echo "<html><title>eLouai's Download Script</title><body>ERROR: download file NOT SPECIFIED. USE force-download.php?file=filepath</body></html>";
          exit;
        } elseif ( ! file_exists( $dir_file ) )
        {
          echo "<html><title>eLouai's Download Script</title><body>ERROR: File not found. USE force-download.php?file=filepath {$dir_file}</body></html>";
          exit;
        };
        switch( $file_extension )
        {
          case 'mp3':
          header('Content-type: audio/mpeg');
          header("Content-Transfer-Encoding: binary");
          $ctype= "audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3";
          break;
          case "pdf": $ctype="application/pdf"; break;
          case "exe": $ctype="application/octet-stream"; break;
          case "zip": $ctype="application/zip"; break;
          case "doc": $ctype="application/msword"; break;
          case "xls": $ctype="application/vnd.ms-excel"; break;
          case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
          case "gif": $ctype="image/gif"; break;
          case "png": $ctype="image/png"; break;
          case "jpeg":
          case "jpg": $ctype="image/jpg"; break;
          default: $ctype="application/force-download";
        }
        header("Pragma: public"); // required
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: $ctype");
        header("Content-Disposition: filename=\"".basename($dir_file)."\";" );
        // header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".filesize($dir_file));
        readfile("$dir_file");
        exit();
    }

    public function manager()
    {

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        // require __LIBS__ . DS . 'filemanager' . DS . 'filemanager.php';
        //
        return $this->auth()->useLayout("front")->view(
            'media/filemanager',
            [
                'title' => 'Practices',
                'baliseTitle' => 'Listening dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "day" => $day,
                "practices" => $practices,
                "session" => Session::getToken()
            ]
        );

    }

    public function files()
    {

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        require __ROOT__ . '\app\views\media\config.php';
        require __ROOT__ . '\app\views\media\tinyfilemanager.php';
        return;
        return $this->auth()->useLayout("front")->view(
            'media/tinyfilemanager.php',
            [
                'title' => 'Practices',
                'baliseTitle' => 'Listening dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "day" => $day,
                "practices" => $practices,
                "session" => Session::getToken()
            ]
        );

    }
}
