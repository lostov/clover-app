<?php

namespace App\Controllers;
use App\Models\{User,Speaking,Config,Media};
use \Core\Http\{Input,Request,Response,Session};
use \Core\Routing\Helper as RouteHelper;

class SpeakingController extends Controller
{
    public function dashboard()
    {
        // Session::regenerate();

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $weeks = Speaking::getWeeks();
        $days = Speaking::getDays();
        $practices = Speaking::getPractices();

        return $this->auth()->useLayout("front")->view(
            'speaking/dashboard',
            [
                'title' => 'Dashboard',
                'baliseTitle' => 'Speaking dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "weeks" => $weeks,
                "days" => $days,
                "practices" => $practices,
                "bonuses" => [],
                "session" => Session::getToken()
            ]
        );
    }

    public function weeks()
    {
        // Session::regenerate();

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $weeks = Speaking::getWeeks();

        return $this->auth()->useLayout("front")->view(
            'speaking/dashboardWeeks',
            [
                'title' => 'Weeks',
                'baliseTitle' => 'Speaking dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "weeks" => $weeks,
                "session" => Session::getToken()
            ]
        );
    }

    public function days()
    {
        // Session::regenerate();

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $week = Input::get('week',0,'int');
        $days = Speaking::getDays($week);

        return $this->auth()->useLayout("front")->view(
            'speaking/dashboardDays',
            [
                'title' => 'Days',
                'baliseTitle' => 'Speaking dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "week" => $week,
                "days" => $days,
                "session" => Session::getToken()
            ]
        );
    }

    public function practices()
    {
        // Session::regenerate();

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $day = Input::get('day',0,'int');
        $practices = Speaking::getPractices($day);

        return $this->auth()->useLayout("front")->view(
            'speaking/dashboardPractices',
            [
                'title' => 'Practices',
                'baliseTitle' => 'Speaking dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "day" => $day,
                "practices" => $practices,
                "session" => Session::getToken()
            ]
        );
    }

    public function weekForm() {
    	// Session::regenerate();

        $session = '';
        if (Input::hasGet('session')) {
            $session = Input::get('session', null, 'string');
        }
        if (!Session::checkToken($session)) {
            Response::redirect(RouteHelper::getLink('Speaking::weeks'));
        }

        if (Input::hasPost('week')) {
            if (Input::hasPost('session')) {
                $session = Input::post('session', null, 'string');
            }
            $check = Session::checkToken($session);
            if ($check) {
                $week = Input::post('week', null, 'object');
                $object = $week;
                $result = Speaking::weekForm($week);
                if ($result) {
                	Response::redirect(RouteHelper::getLink('Speaking::weeks'));
                }
            }
            Response::redirect(RouteHelper::getLink('Speaking::weeks'));
        }
        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $week = Speaking::getWeek(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'speaking/weekForm',
            [
                'title' => 'Edit a week',
                'baliseTitle' => 'Speaking dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "week" => $week,
                "session" => Session::getToken()
            ]
        );
    }

    public function weekNew() {
        // Session::regenerate();


        $session = '';
        if (Input::hasGet('session')) {
            $session = Input::get('session', null, 'string');
        }
        if (!Session::checkToken($session)) {
            Response::redirect(RouteHelper::getLink('Speaking::weeks'));
        }

        if (Input::hasPost('week')) {
            $week = Input::post('week', null, 'object');
            $result = Speaking::weekNew($week);
            if ($result) {
            	Response::redirect(RouteHelper::getLink('Speaking::weeks'));
            }
        }

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $week = Speaking::getWeek(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'speaking/weekNew',
            [
                'title' => 'Add a week',
                'baliseTitle' => 'Speaking dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "object" => $object,
                "result" => $result,
                "email" => $email,
                "session" => Session::getToken()
            ]
        );
    }

    public function dayForm() {
        // Session::regenerate();
        // if ( !Session::checkToken(Input::get('session', null, 'string')) ) {
        //     Response::redirect(RouteHelper::getLink('Speaking::days'));
        // }

        if (Input::hasPost('day')) {
            $day = Input::post('day', null, 'object');
            $object = $day;
            $result = Speaking::dayForm($day);
            if ($result) {
            	Response::redirect(RouteHelper::getLink('Speaking::days'));
            }
        }
        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];
        // $stylesheets[] = ["href" => __SITE__ . "/assets/js/jsmodules/dist/css/highlight.min.css"];
        // $stylesheets[] = ["href" => __SITE__ . "/assets/js/jsmodules/dist/css/wysiwyg.min.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        // $postScripts[] = ["src" => __SITE__ . "/assets/js/jsmodules/dist/js/highlight.min.js"];
        // $postScripts[] = ["src" => __SITE__ . "/assets/js/jsmodules/dist/js/wysiwyg.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $weeks = Speaking::optionsWeeks();
        $day = Speaking::getDay(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'speaking/dayForm',
            [
                'title' => 'Edit a day',
                'baliseTitle' => 'Speaking dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "weeks" => $weeks,
                "colors" => Config::optionsColors(),
                "day" => $day,
                "session" => Session::getToken()
            ]
        );
    }

    public function dayNew() {
        // Session::regenerate();
        $session = '';
        if (Input::hasGet('session')) {
            $session = Input::get('session', null, 'string');
        } elseif (Input::hasPost('session')) {
            $session = Input::post('session', null, 'string');
        }
        if (!Session::checkToken($session)) {
            Response::redirect(RouteHelper::getLink('Speaking::days'));
        }
        if (Input::hasPost('day')) {
            $day = Input::post('day', null, 'object');
            $result = Speaking::dayNew($day);
            if ($result) {
            	Response::redirect(RouteHelper::getLink('Speaking::days'));
            }
        }

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $week = Input::get('week', 0, 'int');
        $color = Input::get('color', 0, 'int');
        $weeks = Speaking::optionsWeeks();
        $day = Speaking::getDay(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'speaking/dayNew',
            [
                'title' => 'Add a day',
                'baliseTitle' => 'Speaking Dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "weeks" => $weeks,
                "weekDay" => $week,
                "colorDay" => $color,
                "colors" => Config::optionsColors(),
                "result" => $result,
                "email" => $email,
                "session" => Session::getToken()
            ]
        );
    }

    public function practiceForm() {
        Session::regenerate();

        if (Input::hasPost('practice')) {
            $practice = Input::post('practice', null, 'object');
            $object = $practice;
            $result = Speaking::practiceForm($practice);
            if ($result) {
                Response::redirect(RouteHelper::getLink('Speaking::practices'));
            }
        }
        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $days = Speaking::allDays();
        $media = Media::getMedia();
        $practice = Speaking::getPractice(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'speaking/practiceForm',
            [
                'title' => 'Edit practice',
                'baliseTitle' => 'Speaking dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "practice" => $practice,
                "days" => $days,
                "media" => $media,
                "colors" => Config::optionsColors(),
                "session" => Session::getToken()
            ]
        );
    }

    public function practiceNew() {
        Session::regenerate();

        if (Input::hasPost('day')) {
            $day = Input::post('practice', null, 'object');
            $result = Speaking::practiceNew($practice);
            if ($result) {
                Response::redirect(RouteHelper::getLink('Speaking::practices'));
            }
        }

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');

        $practice = Speaking::getPractice(Input::get('id', 0 , 'int'));

        return $this->auth()->useLayout("front")->view(
            'speaking/practiceNew',
            [
                'title' => 'New practice',
                'baliseTitle' => 'Speaking dashboard',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "result" => $result,
                "email" => $email,
                "session" => Session::getToken()
            ]
        );
    }
}
