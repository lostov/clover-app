<?php

namespace App\Controllers;

use Core\Controller\BaseController;
use App\Models;

/**
 * Родительский контроллер приложения
 */
abstract class Api extends BaseController
{

	public function auth($needAccess = true)
	{
		if ($needAccess) {
			$user_id = (int) \Core\Http\Session::get('user');
			if($user_id > 0) return $this;
		}
		\Core\Http\Response::redirect('system/login');
	}

	public function useLayout($layout)
	{
		parent::setLayout($layout);
		return $this;
	}

	public function getModel(string $model)
	{
		$link = base_path() . "/app/Models/$model.php";
		if (file_exists($link)) {
			$name = "App\\Models\\{$model}";
			$object = new $name;
			if (!is_object($object)) {
				return false;
			}
		} else {
			return false;
		}
		return $object;
	}

}
