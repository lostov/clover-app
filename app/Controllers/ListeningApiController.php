<?php

namespace App\Controllers;
use App\Models\{User,Config,Log,Listening};
use \Core\Http\{Input AS I, Response AS Res, Request AS Req};

class ListeningApiController extends Controller
{

    public function items()
    {

    }

    public function item()
    {

    }

    public function tips()
    {
        $tips = Listening::getTips(1,1);
        if (!$tips) {
            $code = 201;
            $message = 'Quick Tips not enabled';
        } else {
            $code = 200;
            $message = 'Quick Tips';
        }
        return $this->json(
            [
                'code' => $code,
                'message' => $message,
                'data' => $tips,
            ]
        );  
    }
}
