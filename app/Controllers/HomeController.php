<?php

namespace App\Controllers;
use App\Models\{User,Article};
use \Core\Http\{Input,Request,Response,Session};

/**
 * Управление домашней страницей
 */
class HomeController extends Controller
{
    /**
     * Показ страниц
     */
    public function show()
    {
        Session::regenerate();

        $stylesheets = $postScripts = [];
        $stylesheets[] = ["href" => __SITE__ . "/assets/css/material-dashboard.css"];
        $stylesheets[] = ["href" => __SITE__ . "/assets/app/app.css"];

        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/popper.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core/bootstrap-material-design.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/perfect-scrollbar.jquery.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/moment.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/sweetalert2.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.validate.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.bootstrap-wizard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-selectpicker.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-datetimepicker.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery.dataTables.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-tagsinput.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jasny-bootstrap.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/fullcalendar.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/jquery-jvectormap.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/nouislider.min.js"];
        // $postScripts[] = ["src" => "https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/core-js/client/core.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/arrive.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/chartist.min.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/plugins/bootstrap-notify.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/js/material-dashboard.js"];
        $postScripts[] = ["src" => __SITE__ . "/assets/app/app.js"];

        $user = (int) Session::get('user');
        $username = Session::get('username');
        $email = Session::get('email');
        $articles = Article::getItems();
        foreach ($articles as $key => $article) {
            if (User::load((int) $article['user'])) {
                $articles[$key]['email'] = User::get('email');
            } else {
                $articles[$key]['email'] = '[' . $article['user'] . ']';
            }
        }
        return $this->auth()->useLayout("front")->view(
            'specific-page/home',
            [
                'title' => 'Dashboard',
                'baliseTitle' => 'Homepage title',
                'metaDescription' => 'Homepage desciption',
                "stylesheets" => $stylesheets,
                "postScripts" => $postScripts,
                "username" => $username,
                "email" => $email,
                "session" => Session::getToken()
            ]
        );
    }

    public function exportWord()
    {
        $session = Input::get('session', null, 'string');
        if (!Session::checkToken($session)) {
            Response::redirect('error/777');
        }
        $ids = json_decode(Input::get('id'));
        $articles = Article::getItems($ids);

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $phpWord->getSettings()->setUpdateFields(true);

        $section = $phpWord->addSection();

        // Define the TOC font style
        $fontStyle = array('name' => 'Times New Roman', 'spaceAfter' => 60, 'size' => 12);
        $fontStyle2 = array('size' => 10);

        // Add title styles
        $phpWord->addTitleStyle(1, array('name' => 'Times New Roman', 'size' => 20, 'color' => '000000', 'bold' => true));
        $phpWord->addTitleStyle(2, array('name' => 'Times New Roman', 'size' => 16, 'color' => '282828'));
        $phpWord->addTitleStyle(3, array('name' => 'Times New Roman', 'size' => 14, 'italic' => true));
        $phpWord->addTitleStyle(4, array('name' => 'Times New Roman', 'size' => 12));

        // Add text elements
        $section->addText('Sarlavha');
        $section->addTextBreak(2);

        // Add TOC #1
        $toc = $section->addTOC($fontStyle);
        $section->addTextBreak(2);

        // Filler
        // $section->addText('Text between TOC');
        $section->addTextBreak(2);
        foreach ($articles as $key => $article) {
            $section->addPageBreak();
            $section->addTitle($article['title'], 1);
            $section->addText($article['text']);
            $section->addTextBreak(2);
        }

        //$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord,'Word2007');
        //$objWriter->save('doc.docx');

        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="word_'.date('YmdHis').'.docx"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $xmlWriter->save("php://output");
    }
}
