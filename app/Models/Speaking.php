<?php

namespace App\Models;
use Core\Http\Session;

class Speaking extends Model
{

	public static function add()
	{

	}

	public static function get()
	{

	}

	public static function setTips(int $user, int $tips, bool $status)
	{

	}

	/**
	 * Quick Tips - Pratice
	 *
	 * @param int $tips - tips id
	 * @param int $user - user id
	 * @return false|array
	 */
	public static function getTips(int $tips, int $user)
	{
		$tips = \DB::query('SELECT t.id, t.text FROM `tips` AS t LEFT JOIN tips_user AS tu ON t.id = tu.tips WHERE tu.status = 1 AND t.id = %i_tips AND tu.user = %i_user',['tips' => $tips, 'user' => $user]);
		if (!$tips) {
			return false;
		}
		return $tips;
	}

	public static function getDays(int $week = 0)
	{
		if ($week > 0) {
			$days = \DB::query('SELECT d.*, w.title AS week_title FROM `speaking_days` AS d LEFT JOIN `speaking_weeks` AS w ON d.week = w.id WHERE (d.status = %i_enable OR d.status = %i_disable OR d.status = %i_archive) AND (d.week = %i_week)',['enable' => 1, 'disable' => 0, 'archive' => -1, 'week' => $week]);
		} else {
			$days = \DB::query('SELECT d.*, w.title AS week_title FROM `speaking_days` AS d LEFT JOIN `speaking_weeks` AS w ON d.week = w.id WHERE d.status = %i_enable OR d.status = %i_disable OR d.status = %i_archive',['enable' => 1, 'disable' => 0, 'archive' => -1]);
		}
		if (!$days) {
			return false;
		}
		return $days;
	}

	public static function optionsDays(int $week = 0)
	{
		if ($week > 0) {
			$days = \DB::query('SELECT d.id, d.title, d.icon FROM `speaking_days` as d WHERE d.status = 1 AND d.week = %i_week', ['week' => $week]);
		} else {
			$days = \DB::query('SELECT d.id, d.title, d.icon FROM `speaking_days` as d WHERE d.status = 1');
		}

		if (!$days) {
			return false;
		}
		return $days;
	}

	public static function allDays()
	{
		$days = \DB::query('SELECT d.*, w.title AS week_title FROM `speaking_days` AS d LEFT JOIN `speaking_weeks` AS w ON d.week = w.id WHERE w.status = 1 AND d.status = 1');
		if (!$days) {
			return false;
		}
		return $days;
	}

	public static function getDay(int $id)
	{
		if ($id === 0) {
			return false;
		}
		$day = \DB::queryFirstRow('SELECT d.*, c.username AS created_user, m.username AS modified_user FROM `speaking_days` AS d LEFT JOIN `users` AS c ON d.created_by = c.id LEFT JOIN `users` AS m ON d.modified_by = c.id WHERE (d.status = %i_enable OR d.status = %i_disable OR d.status = %i_archive) AND (d.id = %i_day)',['enable' => 1, 'disable' => 0, 'archive' => -1,  'day' => $id]);
		if (!$day) {
			return false;
		}
		return $day;
	}

	public static function getPractices(int $day = 0)
	{
		if ($day > 0) {
			$practices = \DB::query('SELECT p.*, w.title AS week_title, d.title AS day_title, c.username AS created_user, m.username AS modified_user FROM `speaking_practices` AS p LEFT JOIN `users` AS c ON p.created_by = c.id LEFT JOIN `users` AS m ON p.modified_by = c.id LEFT JOIN `speaking_days` AS d ON p.day = d.id LEFT JOIN `speaking_weeks` AS w ON d.week = w.id WHERE (p.status = %i_enable OR p.status = %i_disable OR p.status = %i_archive) AND p.day = %i_day',['enable' => 1, 'disable' => 0, 'archive' => -1, 'day' => $day]);
		} else {
			$practices = \DB::query('SELECT p.*, w.title AS week_title, d.title AS day_title, c.username AS created_user, m.username AS modified_user FROM `speaking_practices` AS p LEFT JOIN `users` AS c ON p.created_by = c.id LEFT JOIN `users` AS m ON p.modified_by = c.id LEFT JOIN `speaking_days` AS d ON p.day = d.id LEFT JOIN `speaking_weeks` AS w ON d.week = w.id WHERE p.status = %i_enable OR p.status = %i_disable OR p.status = %i_archive',['enable' => 1, 'disable' => 0, 'archive' => -1]);
		}
		if (!$practices) {
			return false;
		}
		return $practices;
	}

	public static function optionsPractices(int $day = 0)
	{
		if ($day > 0) {
			$practices = \DB::query('SELECT p.id, p.title, p.icon FROM `speaking_practices` as p WHERE p.status = 1 AND p.day = %i_day', ['day' => $day]);
		} else {
			$practices = \DB::query('SELECT p.id, p.title, p.icon FROM `speaking_practices` as p WHERE p.status = 1');
		}

		if (!$practices) {
			return false;
		}
		return $practices;
	}

	public static function getPractice(int $id)
	{
		if ($id === 0) {
			return false;
		}
		$practice = \DB::queryFirstRow('SELECT * FROM `speaking_practices` WHERE (status = %i_enable OR status = %i_disable OR status = %i_archive) AND (id = %i_practice)',['enable' => 1, 'disable' => 0, 'archive' => -1,  'practice' => $id]);
		if (!$practice) {
			return false;
		}
		return $practice;
	}

	public static function getWeeks()
	{
		$weeks = \DB::query('SELECT * FROM `speaking_weeks` WHERE status = %i_enable OR status = %i_disable OR status = %i_archive',['enable' => 1, 'disable' => 0, 'archive' => -1]);
		if (!$weeks) {
			return false;
		}
		return $weeks;
	}
	public static function optionsWeeks()
	{
		$weeks = \DB::query('SELECT w.id, w.title, w.icon FROM `speaking_weeks` as w WHERE w.status = 1');
		if (!$weeks) {
			return false;
		}
		return $weeks;
	}

	public static function getWeek(int $id = 0)
	{
		if ($id === 0) {
			return false;
		}
		$week = \DB::queryFirstRow('SELECT w.*, c.username AS created_user, m.username AS modified_user FROM `speaking_weeks` AS w LEFT JOIN `users` AS c ON w.created_by = c.id LEFT JOIN `users` AS m ON w.modified_by = m.id WHERE (w.status = %i_enable OR w.status = %i_disable OR w.status = %i_archive) AND (w.id = %i_week)',['enable' => 1, 'disable' => 0, 'archive' => -1,  'week' => $id]);
		if (!$week) {
			return false;
		}
		return $week;

	}

	public static function weekNew(object $object)
	{
		$user = (int) Session::get('user');
		// $title = $object->title
		// DB::update('tbl', ['age' => 25, 'height' => 10.99], "name=%s", $name);
		$result = \DB::insert(
			'speaking_weeks',
			[
				'title' => $object->title,
				'text' => $object->text,
				'created_by' => $user,
				'status' => (int) $object->status
			]
		);
		return $result;
	}

	public static function weekForm(object $object)
	{
		date_default_timezone_set('Asia/Tashkent');
		$user = (int) Session::get('user');
		$id = $object->id;
		$result = \DB::update(
			'speaking_weeks',
			[
				'title' => $object->title,
				'icon' => (isset($object->icon) ? $object->icon : null),
				'text' => $object->text,
				'modified' => date('Y-m-d H:i:s', time()),
				'modified_by' => $user,
				'status' => (int) $object->status
			],
			"id=%i", $id
		);
		return $result;
	}

	public static function dayNew(object $object)
	{
		$user = (int) Session::get('user');
		$result = \DB::insert(
			'speaking_days',
			[
				'week' => $object->week,
				'title' => $object->title,
				'color' => $object->color,
				'icon' => $object->icon ?? null,
				'text' => $object->text,
				'created_by' => $user,
				'status' => (int) $object->status
			]
		);
		return $result;
	}

	public static function dayForm(object $object)
	{
		date_default_timezone_set('Asia/Tashkent');
		$user = (int) Session::get('user');
		$id = $object->id;
		$result = \DB::update(
			'speaking_days',
			[
				'week' => $object->week,
				'title' => $object->title,
				'text' => $object->text,
				'color' => $object->color,
				'icon' => $object->icon ?? null,
				'modified' => date('Y-m-d H:i:s', time()),
				'modified_by' => $user,
				'status' => (int) $object->status
			],
			"id=%i", $id
		);
		return $result;
	}

	public static function practiceNew(object $object)
	{
		$user = (int) Session::get('user');
		$result = \DB::insert(
			'speaking_practices',
			[
				'day' => $object->day,
				'title' => $object->title,
				'text' => $object->text,
				'color' => $object->color,
				'media' => $object->media,
				'content' => $object->content,
				'icon' => $object->icon ?? null,
				'text' => $object->text,
				'created_by' => $user,
				'status' => (int) $object->status
			]
		);
		return $result;
	}

	public static function practiceForm(object $object)
	{
		date_default_timezone_set('Asia/Tashkent');
		$user = (int) Session::get('user');
		$id = $object->id;
		$result = \DB::update(
			'speaking_practices',
			[
				'day' => $object->day,
				'title' => $object->title,
				'text' => $object->text,
				'color' => $object->color,
				'media' => $object->media,
				'content' => $object->content,
				'icon' => $object->icon ?? null,
				'modified' => date('Y-m-d H:i:s', time()),
				'modified_by' => $user,
				'status' => (int) $object->status
			],
			"id=%i", $id
		);
		return $result;
	}
}
