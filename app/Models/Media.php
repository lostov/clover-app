<?php

namespace App\Models;

/**
 * Модель предмета
 */
class Media extends Model
{
	public static function getByType($type = 0)
	{
		if ($type <= 0 ) {
			return false;
		}
		return \DB::query('SELECT m.* FROM `media` AS m WHERE m.type = %i_type', ['type' => $type]);
	}

	public static function getMedia()
	{
		return \DB::query('SELECT m.* FROM `media` AS m');
	}

	public static function getByUser(int $user)
	{
		if ($user <= 0 ) {
			return false;
		}
		return \DB::query('SELECT m.* FROM `media` AS m WHERE m.created_by = %i_user', ['user' => $user]);
	}
}
