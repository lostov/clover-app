<?php

namespace App\Models;

/**
 * Модель предмета
 */
class Log extends Model
{

	public static function add(int $type = 1, string $action, int $user)
	{
		\DB::insert('logs', array('type' => $type, 'action' => $action, 'user' => $user));
	}
}
