<?php

namespace App\Models;

/**
 * Модель предмета
 */
class Config extends Model
{

	public static function add(string $name, string $value, int $type, string $title, int $user = 1)
	{
		\DB::insert('configs', array('name' => $name, 'value' => $value, 'type' => $type, 'title' => $title, 'user' => $user));
	}

	public static function get(string $name, bool $isgroup = false, bool $type = false, $order = true)
	{
		if ( $order == true ) {
			$sort = " ORDER BY sort ASC";
		} elseif ( $order == false ) {
			$sort = " ORDER BY sort DESC";
		} else {
			$sort = " ORDER BY $order";
		}
		$types = [0=>"null",1=>"string",2=>"int",3=>"bool",4=>"array",5=>"object"];
		$SQL = "SELECT * FROM configs WHERE name";
		if ( !$isgroup ) {
			$SQL .= "=%s" . $sort;
			$param = \DB::queryFirstRow( $SQL, $name );
			if ( $param ) {
				if ( $type ) {
					$type = (int) $param['type'];
					if ( $type < 4 && $type >= 0 ) {
						settype( $param['value'], $types[$type] );
					} elseif ( $type == 4 || $type == 5 ) {
						$value = json_decode($param['value']);
						settype($value, $types[$type]);
						$param['value'] = $value;
					}
				}
				return $param;
			}
		} else {
			$SQL .= " LIKE %?" . $sort;
			$group = $name.".%";
			$params = \DB::query( $SQL, $group );
			if ( $params ) {
				if ( $type ) {
					foreach ($params as $key => $param) {
						$type = (int) $param['type'];
						if ( $type < 4 && $type >= 0 ) {
							settype( $param['value'], $types[$type] );
						} elseif ( $type == 4 || $type == 5 ) {
							$value = json_decode($param['value']);
							settype($value, $types[$type]);
							$param['value'] = $value;
						}
						$params[$key] = $param;
					}
				}
				return $params;
			}
		}
		return false;
	}

	public static function getMainItems()
	{
		return \DB::query('SELECT `code`, `icon`, `title`, `text` FROM items WHERE type = 1');
	}

	public static function getColors()
	{
		$colors = \DB::query('SELECT c.* `config_colors` AS c');
		if (!$colors) {
			return false;
		}
		return $colors;
	}

	public static function optionsColors()
	{
		$colors = \DB::query('SELECT c.id, c.name, c.code FROM `config_colors` AS c');
		if (!$colors) {
			return false;
		}
		return $colors;
	}
}
