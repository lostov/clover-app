<?php

namespace App\Models;

/**
 * Модель предмета
 */
class Slides extends Model
{

	public static function add(string $url, int $category = 0, int $user = 1)
	{
		\DB::insert('slides', array('url' => $url, 'category' => $category, 'user' => $user));
	}

	public static function get(int $category = 0, int $count = 0, int $user = 0, int $status = 1)
	{
		if ($category == 0 && $count == 0 && $user == 0) {
			return \DB::query('SELECT * FROM slides WHERE status=%i',$status);
		}
	}
}
