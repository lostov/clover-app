<?php

namespace App\Models;

/**
 * Модель предмета
 */
class Article extends Model
{

	protected static $id = 0;
	protected static $__article = [];

	public static function load(int $id)
	{
		if ( !is_null($id) ) {
			$article = \DB::queryFirstRow("SELECT * FROM articles WHERE id=%i", $id);
			if ( $article['id'] > 0 ) {
				self::$id = $id;
				self::$__article = $article;
				 return true;
			}
		}
		self::$id = 0;
		return false;
	}

	public static function bind(array $data)
	{
		$columns = \DB::columnList('articles');
		$keys =  [];
		foreach ($data as $key => $value) {
			$id = array_search($key, $columns);
			if ($id) {
				$keys[] = $key;
			}
		}
		return $keys;
	}

	public static function store()
	{

	}

	public static function save()
	{
		# code...
	}

	public static function id()
	{
		return self::$id;
	}

	public static function set(string $key, $value)
	{
		self::$__article[$key] = $value;
	}

	public static function get(string $value)
	{
		return self::$__article[$value];
	}

	public static function getItems(array $ids = [], int $status = 1)
	{
		if ( count($ids) == 0 ) {
			$sql = "SELECT * FROM articles";
			if ( $status == -1 ) {
				$sql .= " WHERE status != %i";
			} else {
				$sql .= " WHERE status = %i";
			}
			$articles = \DB::query($sql, $status);
			if ( count($articles) > 0 ) {
				return $articles;
			}
			return false;
		}

		$articles = [];
		foreach ($ids as $key => $id) {
			$article = self::getItem($id);
			if ($article) {
				$articles[] = $article;
			}
		}
		return $articles;
	}

	public static function getItem(int $id = null)
	{
		if( is_null($id) && self::id() > 0 ) {
			return self::$__article;
		} elseif ( self::load($id) ) {
			return self::$__article;
		} else {
			return false;
		}
	}

	public static function update(array $values, int $id = null)
	{
		if (is_null($id) && self::id() > 0) {
			$id = self::id();
		} 

		return \DB::update('articles', $values, "id=%i", $id);
	}

	public static function publish(int $id = null)
	{
		if (is_null($id) && self::id() > 0) {
			if (self::update(['status' => 1], self::id())) {
				return true;
			}
		} elseif ( $id > 0 ) {
			if (self::update(['status' => 1], $id)) {
				return true;
			}
		}
		return false;
	}

	public static function unpublish(int $id = null)
	{
		if (is_null($id) && self::id() > 0) {
			if (self::update(['status' => 0], self::id())) {
				return true;
			}
		} elseif ( $id > 0 ) {
			if (self::update(['status' => 0], $id)) {
				return true;
			}
		}
		return false;
	}

	public static function archive(int $id = null)
	{
		if (is_null($id) && self::id() > 0) {
			if (self::update(['status' => 2], self::id())) {
				return true;
			}
		} elseif ( $id > 0 ) {
			if (self::update(['status' => 2], $id)) {
				return true;
			}
		}
		return false;
	}

	public static function delete(int $id = null)
	{
		if (is_null($id) && self::id() > 0) {
			$id = self::id();
		} 

		return \DB::delete('articles', "id=%i", $id);
	}

	public static function find(array $values)
	{

	}

}
