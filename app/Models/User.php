<?php

namespace App\Models;
use App\Models\Log AS CloverLog;
use \Core\Http\{Input AS CInput, Response AS CResponse, Request AS CRequest, Session AS CloverSession};
use \Core\Support\Factory AS CloverFactory;
/**
 * Модель User
 */
class User extends Model
{

	protected static $id = 0;
	protected static $__user = [];

	public static function load(int $id = null)
	{
		if (!is_null($id)) {
			$user = \DB::queryFirstRow("SELECT * FROM users WHERE id=%i", $id);
			if ($user['id'] > 0) {
				self::$id = $id;
				self::$__user = $user;
				 return true;
			}
		}
		self::$id = 0;
		return false;
	}

	public static function id()
	{
		return self::$id;
	}

	public static function get($value)
	{
		return self::$__user[$value];
	}

	public static function getUser(int $id)
	{
		if (self::load($id)) {
			return self::$__user;
		} else {
			return false;
		}
	}

	public static function auth(string $username, string $password)
	{
		$user = \DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
		if (CloverFactory::passwordCheck($password,$user['password'])) {
			self::load($user['id']);
			CloverLog::add(1,'auth.login',self::id());
			CloverSession::set('user',self::id());
			CloverSession::set('username',self::get('username'));
			CloverSession::set('email',self::get('email'));
			return $user;
		}
		return false;
	}

	public static function find(array $params = array())
	{
		
	}

	public static function checkUserByToken(string $token = "")
	{
	}

	public static function checkUserToken()
	{
		return CInput::get('user','asd','string');
	}
}
