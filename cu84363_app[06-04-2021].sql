-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 06 2021 г., 05:34
-- Версия сервера: 8.0.19
-- Версия PHP: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cu84363_app`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int NOT NULL,
  `title` text NOT NULL,
  `text` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category` int NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `text`, `user`, `date`, `category`, `status`) VALUES
(1, 'Some title 1', 'Lorem1 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:41', 1, 1),
(2, 'Some title 2', 'Lorem2 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:47', 1, 1),
(3, 'Some title 3', 'Lorem3 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:51', 1, 1),
(4, 'Some title 4', 'Lorem4 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:54', 1, 1),
(5, 'Some title 5', 'Lorem5 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:57', 1, 1),
(6, 'Some title 6', 'Lorem6 ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-04 04:39:59', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int NOT NULL,
  `title` text NOT NULL,
  `parent` int NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `configs`
--

CREATE TABLE `configs` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = default.null\r\n1 = string\r\n2 = int\r\n3 = bool\r\n4 = json.array\r\n5 = json.object',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sort` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `configs`
--

INSERT INTO `configs` (`id`, `user`, `name`, `value`, `date`, `type`, `title`, `sort`) VALUES
(1, 1, 'slides.count', '5', '2020-12-06 01:06:46', 2, 'How Many Slides for a use on home page', 1),
(2, 1, 'slides.total', '10', '2020-12-06 01:47:58', 2, 'Totaly', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `config_colors`
--

CREATE TABLE `config_colors` (
  `id` int NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `config_colors`
--

INSERT INTO `config_colors` (`id`, `name`, `code`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'Black', '#000', '2021-04-06 07:24:50', 1, NULL, NULL),
(2, 'White', '#fff', '2021-04-06 07:24:55', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `items`
--

CREATE TABLE `items` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `type` int NOT NULL DEFAULT '1',
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `icon` text,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `items`
--

INSERT INTO `items` (`id`, `user`, `type`, `code`, `title`, `icon`, `text`) VALUES
(1, 1, 1, 'listening', 'Listening', 'some-icon', 'Extend your listening ability with question based exercises and b...'),
(2, 1, 1, 'reading', 'Reading', 'some-icon', 'Upgrade your reading skills with customized passages ...'),
(3, 1, 1, 'writing', 'Writing', 'some-icon', 'Become an excellent writer via master class sessions in-a...'),
(4, 1, 1, 'speaking', 'Speaking', 'some-icon', 'Transform your speaking skills using our \"Record\" feature and r...'),
(5, 1, 1, 'vocabulary', 'Vocabulary', 'some-icon', 'Turn into a vocabulary guru by just playing quizzes');

-- --------------------------------------------------------

--
-- Структура таблицы `listening`
--

CREATE TABLE `listening` (
  `id` int NOT NULL,
  `day` tinyint NOT NULL,
  `tips` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `listening`
--

INSERT INTO `listening` (`id`, `day`, `tips`, `title`, `text`, `user`, `date`) VALUES
(1, 1, 1, 'Lorem ipsum practice first', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-07 02:20:11'),
(2, 2, 1, 'Lorem ipsum practice second', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-07 02:20:28');

-- --------------------------------------------------------

--
-- Структура таблицы `listening_days`
--

CREATE TABLE `listening_days` (
  `id` int NOT NULL,
  `week` int NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` int DEFAULT NULL,
  `color` int NOT NULL,
  `text` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `listening_weeks`
--

CREATE TABLE `listening_weeks` (
  `id` int NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` int DEFAULT NULL,
  `text` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `listening_weeks`
--

INSERT INTO `listening_weeks` (`id`, `title`, `icon`, `text`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 'Week 1', NULL, 'Some text', '2021-04-04 13:32:59', 1, NULL, NULL, 1),
(2, 'Week 2', NULL, 'Some text', '2021-04-04 13:33:06', 1, NULL, NULL, 0),
(3, 'ASdsa', NULL, 'adssadsdasadsad', '2021-04-06 05:46:28', 1, NULL, NULL, -1),
(4, 'ASdsa222', NULL, 'adssadsdasadsad', '2021-04-06 05:50:29', 1, '2021-04-06 07:23:02', 1, -1),
(5, 'Aba', NULL, 'Bbbb', '2021-04-06 06:05:38', 1, '2021-04-06 07:23:11', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `logs`
--

CREATE TABLE `logs` (
  `id` int NOT NULL,
  `type` int NOT NULL,
  `action` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `logs`
--

INSERT INTO `logs` (`id`, `type`, `action`, `user`, `date`) VALUES
(1, 1, 'home.show', 1, '2020-12-03 11:11:38'),
(2, 1, 'auth.logout', 1, '2020-12-03 11:12:07'),
(3, 1, 'home.show', 1, '2020-12-03 11:12:28'),
(4, 1, 'auth.logout', 1, '2020-12-03 11:14:51'),
(5, 1, 'auth.login', 1, '2020-12-03 11:15:10'),
(6, 1, 'home.show', 1, '2020-12-03 11:15:10'),
(7, 1, 'home.show', 1, '2020-12-04 03:48:02'),
(8, 1, 'home.show', 1, '2020-12-04 03:48:19'),
(9, 1, 'home.show', 1, '2020-12-04 03:48:32'),
(10, 1, 'home.show', 1, '2020-12-04 03:51:13'),
(11, 1, 'home.show', 1, '2020-12-04 03:52:19'),
(12, 1, 'home.show', 1, '2020-12-04 03:53:17'),
(13, 1, 'home.show', 1, '2020-12-04 03:54:11'),
(14, 1, 'home.show', 1, '2020-12-04 03:56:41'),
(15, 1, 'home.show', 1, '2020-12-04 03:56:52'),
(16, 1, 'home.show', 1, '2020-12-04 03:57:31'),
(17, 1, 'home.show', 1, '2020-12-04 03:58:01'),
(18, 1, 'home.show', 1, '2020-12-04 03:58:08'),
(19, 1, 'home.show', 1, '2020-12-04 03:59:34'),
(20, 1, 'home.show', 1, '2020-12-04 04:00:18'),
(21, 1, 'home.show', 1, '2020-12-04 04:00:40'),
(22, 1, 'home.show', 1, '2020-12-04 04:01:52'),
(23, 1, 'home.show', 1, '2020-12-04 04:03:15'),
(24, 1, 'home.show', 1, '2020-12-04 04:03:32'),
(25, 1, 'home.show', 1, '2020-12-04 04:04:46'),
(26, 1, 'home.show', 1, '2020-12-04 04:05:03'),
(27, 1, 'home.show', 1, '2020-12-04 04:05:09'),
(28, 1, 'home.show', 1, '2020-12-04 04:06:36'),
(29, 1, 'home.show', 1, '2020-12-04 04:09:26'),
(30, 1, 'home.show', 1, '2020-12-04 04:09:37'),
(31, 1, 'home.show', 1, '2020-12-04 04:09:53'),
(32, 1, 'home.show', 1, '2020-12-04 04:10:27'),
(33, 1, 'home.show', 1, '2020-12-04 04:10:39'),
(34, 1, 'home.show', 1, '2020-12-04 04:11:07'),
(35, 1, 'home.show', 1, '2020-12-04 04:13:37'),
(36, 1, 'home.show', 1, '2020-12-04 04:13:53'),
(37, 1, 'home.show', 1, '2020-12-04 04:14:03'),
(38, 1, 'home.show', 1, '2020-12-04 04:14:49'),
(39, 1, 'home.show', 1, '2020-12-04 04:15:15'),
(40, 1, 'home.show', 1, '2020-12-04 04:17:12'),
(41, 1, 'home.show', 1, '2020-12-04 04:17:53'),
(42, 1, 'home.show', 1, '2020-12-04 04:18:21'),
(43, 1, 'home.show', 1, '2020-12-04 04:19:02'),
(44, 1, 'home.show', 1, '2020-12-04 04:19:15'),
(45, 1, 'home.show', 1, '2020-12-04 04:19:21'),
(46, 1, 'home.show', 1, '2020-12-04 04:19:51'),
(47, 1, 'home.show', 1, '2020-12-04 04:20:45'),
(48, 1, 'home.show', 1, '2020-12-04 04:21:57'),
(49, 1, 'home.show', 1, '2020-12-04 04:22:04'),
(50, 1, 'home.show', 1, '2020-12-04 04:22:13'),
(51, 1, 'home.exportword', 1, '2020-12-04 04:25:25'),
(52, 1, 'home.exportword', 1, '2020-12-04 04:25:28'),
(53, 1, 'home.exportword', 1, '2020-12-04 04:25:44'),
(54, 1, 'home.exportword', 1, '2020-12-04 04:26:34'),
(55, 1, 'home.exportword', 1, '2020-12-04 04:26:59'),
(56, 1, 'home.exportword', 1, '2020-12-04 04:31:12'),
(57, 1, 'home.exportword', 1, '2020-12-04 04:31:40'),
(58, 1, 'home.exportword', 1, '2020-12-04 04:34:34'),
(59, 1, 'home.exportword', 1, '2020-12-04 04:35:44'),
(60, 1, 'home.exportword', 1, '2020-12-04 04:36:00'),
(61, 1, 'home.exportword', 1, '2020-12-04 04:37:36'),
(62, 1, 'home.exportword', 1, '2020-12-04 04:38:25'),
(63, 1, 'home.exportword', 1, '2020-12-04 04:46:46'),
(64, 1, 'home.exportword', 1, '2020-12-04 04:57:39'),
(65, 1, 'home.exportword', 1, '2020-12-04 04:58:00'),
(66, 1, 'home.exportword', 1, '2020-12-04 04:58:09'),
(67, 1, 'home.exportword', 1, '2020-12-04 04:58:25'),
(68, 1, 'home.exportword', 1, '2020-12-04 04:58:35'),
(69, 1, 'home.exportword', 1, '2020-12-04 04:58:42'),
(70, 1, 'home.exportword', 1, '2020-12-04 04:59:25'),
(71, 1, 'home.exportword', 1, '2020-12-04 05:02:38'),
(72, 1, 'home.exportword', 1, '2020-12-04 05:04:42'),
(73, 1, 'home.exportword', 1, '2020-12-04 05:04:57'),
(74, 1, 'home.exportword', 1, '2020-12-04 05:05:10'),
(75, 1, 'home.exportword', 1, '2020-12-04 05:05:21'),
(76, 1, 'home.exportword', 1, '2020-12-04 05:07:27'),
(77, 1, 'home.exportword', 1, '2020-12-04 05:07:33'),
(78, 1, 'home.exportword', 1, '2020-12-04 05:08:18'),
(79, 1, 'home.exportword', 1, '2020-12-04 05:16:20'),
(80, 1, 'home.exportword', 1, '2020-12-04 05:17:18'),
(81, 1, 'home.exportword', 1, '2020-12-04 05:17:54'),
(82, 1, 'home.exportword', 1, '2020-12-04 05:18:07'),
(83, 1, 'home.exportword', 1, '2020-12-04 05:18:10'),
(84, 1, 'home.exportword', 1, '2020-12-04 05:18:15'),
(85, 1, 'home.exportword', 1, '2020-12-04 05:18:19'),
(86, 1, 'home.exportword', 1, '2020-12-04 05:18:21'),
(87, 1, 'home.exportword', 1, '2020-12-04 05:18:30'),
(88, 1, 'home.exportword', 1, '2020-12-04 05:18:55'),
(89, 1, 'home.exportword', 1, '2020-12-04 05:19:41'),
(90, 1, 'home.exportword', 1, '2020-12-04 05:20:01'),
(91, 1, 'home.exportword', 1, '2020-12-04 05:20:11'),
(92, 1, 'home.exportword', 1, '2020-12-04 05:20:28'),
(93, 1, 'home.exportword', 1, '2020-12-04 05:20:54'),
(94, 1, 'home.exportword', 1, '2020-12-04 05:23:31'),
(95, 1, 'home.exportword', 1, '2020-12-04 05:23:36'),
(96, 1, 'home.exportword', 1, '2020-12-04 05:24:01'),
(97, 1, 'home.exportword', 1, '2020-12-04 05:24:10'),
(98, 1, 'home.exportword', 1, '2020-12-04 05:24:21'),
(99, 1, 'home.exportword', 1, '2020-12-04 05:25:40'),
(100, 1, 'home.exportword', 1, '2020-12-04 05:28:52'),
(101, 1, 'home.exportword', 1, '2020-12-04 05:29:34'),
(102, 1, 'home.exportword', 1, '2020-12-04 05:30:27'),
(103, 1, 'home.exportword', 1, '2020-12-04 05:30:39'),
(104, 1, 'home.exportword', 1, '2020-12-04 05:31:36'),
(105, 1, 'home.exportword', 1, '2020-12-04 05:31:55'),
(106, 1, 'home.exportword', 1, '2020-12-04 05:35:21'),
(107, 1, 'home.exportword', 1, '2020-12-04 05:35:49'),
(108, 1, 'home.exportword', 1, '2020-12-04 05:36:20'),
(109, 1, 'home.exportword', 1, '2020-12-04 05:36:24'),
(110, 1, 'home.exportword', 1, '2020-12-04 05:40:18'),
(111, 1, 'home.exportword', 1, '2020-12-04 05:43:54'),
(112, 1, 'home.exportword', 1, '2020-12-04 05:44:35'),
(113, 1, 'home.show', 1, '2020-12-04 05:52:31'),
(114, 1, 'home.show', 1, '2020-12-04 05:52:31'),
(115, 1, 'home.show', 1, '2020-12-04 05:53:00'),
(116, 1, 'home.show', 1, '2020-12-04 05:53:39'),
(117, 1, 'home.show', 1, '2020-12-04 05:54:21'),
(118, 1, 'home.show', 1, '2020-12-04 05:54:36'),
(119, 1, 'home.show', 1, '2020-12-04 05:54:42'),
(120, 1, 'home.show', 1, '2020-12-04 05:55:22'),
(121, 1, 'home.show', 1, '2020-12-04 05:56:32'),
(122, 1, 'home.show', 1, '2020-12-04 05:56:50'),
(123, 1, 'home.show', 1, '2020-12-04 05:57:19'),
(124, 1, 'home.show', 1, '2020-12-04 05:57:44'),
(125, 1, 'home.show', 1, '2020-12-04 05:58:04'),
(126, 1, 'home.show', 1, '2020-12-04 05:59:36'),
(127, 1, 'home.show', 1, '2020-12-04 05:59:49'),
(128, 1, 'home.show', 1, '2020-12-04 05:59:59'),
(129, 1, 'home.show', 1, '2020-12-04 06:00:30'),
(130, 1, 'home.show', 1, '2020-12-04 06:00:44'),
(131, 1, 'home.show', 1, '2020-12-04 06:00:57'),
(132, 1, 'home.show', 1, '2020-12-04 06:02:24'),
(133, 1, 'home.show', 1, '2020-12-04 06:02:30'),
(134, 1, 'home.show', 1, '2020-12-04 06:03:07'),
(135, 1, 'home.show', 1, '2020-12-04 06:03:26'),
(136, 1, 'home.show', 1, '2020-12-04 06:04:21'),
(137, 1, 'home.show', 1, '2020-12-04 06:05:05'),
(138, 1, 'home.show', 1, '2020-12-04 06:06:00'),
(139, 1, 'home.show', 1, '2020-12-04 06:07:21'),
(140, 1, 'home.show', 1, '2020-12-04 06:08:16'),
(141, 1, 'home.show', 1, '2020-12-04 06:09:27'),
(142, 1, 'home.show', 1, '2020-12-04 06:10:07'),
(143, 1, 'home.show', 1, '2020-12-04 06:11:11'),
(144, 1, 'home.show', 1, '2020-12-04 06:11:20'),
(145, 1, 'home.show', 1, '2020-12-04 06:12:21'),
(146, 1, 'home.show', 1, '2020-12-04 06:13:15'),
(147, 1, 'home.show', 1, '2020-12-04 06:16:11'),
(148, 1, 'home.show', 1, '2020-12-04 06:22:53'),
(149, 1, 'home.show', 1, '2020-12-04 06:23:04'),
(150, 1, 'home.show', 1, '2020-12-04 06:23:13'),
(151, 1, 'home.exportword', 1, '2020-12-04 06:23:17'),
(152, 1, 'home.exportword', 1, '2020-12-04 06:23:39'),
(153, 1, 'home.exportword', 1, '2020-12-04 06:23:47'),
(154, 1, 'home.exportword', 1, '2020-12-04 06:23:54'),
(155, 1, 'home.exportword', 1, '2020-12-04 06:23:57'),
(156, 1, 'home.exportword', 1, '2020-12-04 06:24:05'),
(157, 1, 'home.exportword', 1, '2020-12-04 06:24:11'),
(158, 1, 'home.show', 1, '2020-12-04 06:25:47'),
(159, 1, 'home.exportword', 1, '2020-12-04 06:25:51'),
(160, 1, 'home.exportword', 1, '2020-12-04 06:28:10'),
(161, 1, 'home.exportword', 1, '2020-12-04 06:33:44'),
(162, 1, 'home.show', 1, '2020-12-04 06:35:37'),
(163, 1, 'home.show', 1, '2020-12-04 06:35:53'),
(164, 1, 'home.show', 1, '2020-12-04 06:40:18'),
(165, 1, 'home.show', 1, '2020-12-04 06:41:06'),
(166, 1, 'home.show', 1, '2020-12-04 06:41:18'),
(167, 1, 'home.show', 1, '2020-12-04 06:41:27'),
(168, 1, 'home.show', 1, '2020-12-04 06:43:10'),
(169, 1, 'home.show', 1, '2020-12-04 06:44:51'),
(170, 1, 'home.exportword', 1, '2020-12-04 06:45:10'),
(171, 1, 'home.show', 1, '2020-12-04 06:46:05'),
(172, 1, 'home.show', 1, '2020-12-04 06:47:48'),
(173, 1, 'home.exportword', 1, '2020-12-04 06:47:54'),
(174, 1, 'home.show', 1, '2020-12-04 06:48:24'),
(175, 1, 'home.show', 1, '2020-12-04 06:48:35'),
(176, 1, 'home.exportword', 1, '2020-12-04 06:48:41'),
(177, 1, 'home.show', 1, '2020-12-04 06:55:18'),
(178, 1, 'auth.logout', 1, '2020-12-04 06:55:24'),
(179, 1, 'auth.login', 1, '2020-12-04 06:55:29'),
(180, 1, 'home.show', 1, '2020-12-04 06:55:29'),
(181, 1, 'home.show', 1, '2020-12-04 06:58:46'),
(182, 1, 'home.show', 1, '2020-12-04 06:58:51'),
(183, 1, 'home.show', 1, '2020-12-04 06:59:06'),
(184, 1, 'home.show', 1, '2020-12-04 07:01:04'),
(185, 1, 'home.show', 1, '2020-12-04 07:03:04'),
(186, 1, 'home.exportword', 1, '2020-12-04 07:03:21'),
(187, 1, 'home.show', 1, '2020-12-04 07:04:48'),
(188, 1, 'home.show', 1, '2020-12-04 07:05:16'),
(189, 1, 'home.exportword', 1, '2020-12-04 07:05:21'),
(190, 1, 'home.exportword', 1, '2020-12-04 07:06:49'),
(191, 1, 'home.show', 1, '2020-12-04 07:06:50'),
(192, 1, 'home.exportword', 1, '2020-12-04 07:06:55'),
(193, 1, 'auth.login', 1, '2020-12-04 07:07:28'),
(194, 1, 'home.show', 1, '2020-12-04 07:07:28'),
(195, 1, 'home.show', 1, '2020-12-04 07:15:21'),
(196, 1, 'home.exportword', 1, '2020-12-04 07:15:28'),
(197, 1, 'home.show', 1, '2020-12-04 07:15:57'),
(198, 1, 'home.exportword', 1, '2020-12-04 07:16:02'),
(199, 1, 'home.show', 1, '2020-12-04 07:17:04'),
(200, 1, 'home.show', 1, '2020-12-04 07:18:01'),
(201, 1, 'home.exportword', 1, '2020-12-04 07:18:04'),
(202, 1, 'home.exportword', 1, '2020-12-04 07:20:55'),
(203, 1, 'home.exportword', 1, '2020-12-04 07:21:14'),
(204, 1, 'home.exportword', 1, '2020-12-04 07:21:17'),
(205, 1, 'home.exportword', 1, '2020-12-04 07:21:45'),
(206, 1, 'home.show', 1, '2020-12-04 07:21:51'),
(207, 1, 'home.exportword', 1, '2020-12-04 07:21:56'),
(208, 1, 'home.show', 1, '2020-12-04 07:22:31'),
(209, 1, 'home.show', 1, '2020-12-04 07:22:39'),
(210, 1, 'home.exportword', 1, '2020-12-04 07:22:43'),
(211, 1, 'auth.login', 1, '2020-12-04 07:30:05'),
(212, 1, 'home.show', 1, '2020-12-04 07:30:05'),
(213, 1, 'home.show', 1, '2020-12-04 07:31:47'),
(214, 1, 'home.exportword', 1, '2020-12-04 07:33:24'),
(215, 1, 'home.exportword', 1, '2020-12-04 07:36:31'),
(216, 1, 'home.exportword', 1, '2020-12-04 07:36:33'),
(217, 1, 'home.exportword', 1, '2020-12-04 07:37:16'),
(218, 1, 'home.exportword', 1, '2020-12-04 07:37:22'),
(219, 1, 'home.exportword', 1, '2020-12-04 07:37:47'),
(220, 1, 'home.show', 1, '2020-12-04 07:38:15'),
(221, 1, 'home.show', 1, '2020-12-04 07:38:25'),
(222, 1, 'home.exportword', 1, '2020-12-04 07:38:29'),
(223, 1, 'home.show', 1, '2020-12-04 07:38:57'),
(224, 1, 'home.show', 1, '2020-12-04 07:39:04'),
(225, 1, 'home.show', 1, '2020-12-04 07:39:11'),
(226, 1, 'home.show', 1, '2020-12-04 07:42:51'),
(227, 1, 'home.show', 1, '2020-12-04 07:47:03'),
(228, 1, 'auth.logout', 1, '2020-12-04 07:49:04'),
(229, 1, 'auth.login', 1, '2020-12-04 07:51:53'),
(230, 1, 'home.show', 1, '2020-12-04 07:51:53'),
(231, 1, 'home.exportword', 1, '2020-12-04 07:52:04'),
(232, 1, 'home.exportword', 1, '2020-12-04 07:52:31'),
(233, 1, 'home.exportword', 1, '2020-12-04 07:52:41'),
(234, 1, 'home.exportword', 1, '2020-12-04 07:53:03'),
(235, 1, 'home.exportword', 1, '2020-12-04 07:53:18'),
(236, 1, 'home.exportword', 1, '2020-12-04 16:10:38'),
(237, 1, 'home.exportword', 1, '2020-12-04 16:11:31'),
(238, 1, 'home.show', 1, '2020-12-04 16:12:10'),
(239, 1, 'home.exportword', 1, '2020-12-04 16:12:15'),
(240, 1, 'home.show', 1, '2020-12-04 16:15:39'),
(241, 1, 'home.exportword', 1, '2020-12-04 16:15:51'),
(242, 1, 'home.exportword', 1, '2020-12-04 16:15:52'),
(243, 1, 'home.show', 1, '2020-12-04 16:16:05'),
(244, 1, 'home.exportword', 1, '2020-12-04 16:16:10'),
(245, 1, 'home.exportword', 1, '2020-12-04 16:16:11'),
(246, 1, 'home.show', 1, '2020-12-04 16:16:32'),
(247, 1, 'home.show', 1, '2020-12-04 16:16:41'),
(248, 1, 'auth.login', 1, '2020-12-23 19:55:17'),
(249, 1, 'home.show', 1, '2020-12-23 19:55:17'),
(250, 1, 'home.show', 1, '2020-12-23 19:55:24'),
(251, 1, 'mainapi.items', 1, '2020-12-23 19:55:56'),
(252, 1, 'api.listening', 1, '2020-12-23 19:57:12'),
(253, 1, 'api.listening', 1, '2020-12-23 20:00:31'),
(254, 1, 'api.listening', 1, '2020-12-23 20:01:49'),
(255, 1, 'api.listening', 1, '2020-12-23 20:04:18'),
(256, 1, 'api.listening', 1, '2020-12-23 20:05:00'),
(257, 1, 'api.listening', 1, '2020-12-23 20:05:54'),
(258, 1, 'api.listening', 1, '2020-12-23 20:09:34'),
(259, 1, 'home.show', 1, '2020-12-23 20:11:01'),
(260, 1, 'auth.logout', 1, '2020-12-23 20:11:06'),
(261, 1, 'auth.login', 1, '2020-12-23 20:12:14'),
(262, 1, 'home.show', 1, '2020-12-23 20:12:14'),
(263, 1, 'auth.logout', 1, '2020-12-23 20:12:30'),
(264, 1, 'auth.login', 1, '2020-12-23 20:12:41'),
(265, 1, 'home.show', 1, '2020-12-23 20:12:41'),
(266, 1, 'auth.logout', 1, '2020-12-23 20:13:49'),
(267, 1, 'auth.login', 1, '2020-12-23 20:14:01'),
(268, 1, 'home.show', 1, '2020-12-23 20:14:01'),
(269, 1, 'auth.logout', 1, '2020-12-23 20:14:03'),
(270, 1, 'auth.login', 1, '2020-12-23 20:14:16'),
(271, 1, 'home.show', 1, '2020-12-23 20:14:16'),
(272, 1, 'auth.logout', 1, '2020-12-23 20:14:30'),
(273, 1, 'auth.login', 1, '2021-04-04 06:59:55'),
(274, 1, 'home.show', 1, '2021-04-04 06:59:55'),
(275, 1, 'home.show', 1, '2021-04-04 07:00:43'),
(276, 1, 'home.show', 1, '2021-04-04 07:00:58'),
(277, 1, 'home.show', 1, '2021-04-04 07:02:32'),
(278, 1, 'home.show', 1, '2021-04-04 07:02:38'),
(279, 1, 'home.show', 1, '2021-04-04 07:03:07'),
(280, 1, 'home.show', 1, '2021-04-04 07:05:08'),
(281, 1, 'home.show', 1, '2021-04-04 07:05:21'),
(282, 1, 'home.show', 1, '2021-04-04 07:05:44'),
(283, 1, 'home.show', 1, '2021-04-04 07:06:27'),
(284, 1, 'home.show', 1, '2021-04-04 07:06:51'),
(285, 1, 'home.show', 1, '2021-04-04 07:07:45'),
(286, 1, 'home.show', 1, '2021-04-04 07:28:37'),
(287, 1, 'home.show', 1, '2021-04-04 07:28:45'),
(288, 1, 'home.show', 1, '2021-04-04 07:28:55'),
(289, 1, 'home.show', 1, '2021-04-04 07:31:22'),
(290, 1, 'home.show', 1, '2021-04-04 07:32:58'),
(291, 1, 'home.show', 1, '2021-04-04 07:34:26'),
(292, 1, 'home.show', 1, '2021-04-04 07:34:44'),
(293, 1, 'home.show', 1, '2021-04-04 07:36:18'),
(294, 1, 'home.show', 1, '2021-04-04 07:36:59'),
(295, 1, 'home.show', 1, '2021-04-04 07:37:06'),
(296, 1, 'home.show', 1, '2021-04-04 07:38:45'),
(297, 1, 'home.show', 1, '2021-04-04 07:40:08'),
(298, 1, 'home.show', 1, '2021-04-04 07:41:06'),
(299, 1, 'home.show', 1, '2021-04-04 07:41:36'),
(300, 1, 'listening.dashboard', 1, '2021-04-04 07:50:57'),
(301, 1, 'listening.dashboard', 1, '2021-04-04 07:51:56'),
(302, 1, 'listening.dashboard', 1, '2021-04-04 07:52:08'),
(303, 1, 'listening.dashboard', 1, '2021-04-04 07:52:20'),
(304, 1, 'home.show', 1, '2021-04-04 07:56:43'),
(305, 1, 'listening.dashboard', 1, '2021-04-04 07:56:48'),
(306, 1, 'listening.dashboard', 1, '2021-04-04 07:58:44'),
(307, 1, 'listening.dashboard', 1, '2021-04-04 07:58:48'),
(308, 1, 'listening.dashboard', 1, '2021-04-04 07:59:16'),
(309, 1, 'listening.dashboard', 1, '2021-04-04 07:59:41'),
(310, 1, 'auth.login', 1, '2021-04-04 09:39:52'),
(311, 1, 'home.show', 1, '2021-04-04 09:39:52'),
(312, 1, 'listening.dashboard', 1, '2021-04-04 09:39:56'),
(313, 1, 'listening.dashboard', 1, '2021-04-04 09:46:12'),
(314, 1, 'listening.dashboard', 1, '2021-04-04 09:48:09'),
(315, 1, 'listening.dashboard', 1, '2021-04-04 09:49:06'),
(316, 1, 'listening.dashboard', 1, '2021-04-04 09:49:30'),
(317, 1, 'listening.dashboard', 1, '2021-04-04 09:50:31'),
(318, 1, 'listening.dashboard', 1, '2021-04-04 12:49:51'),
(319, 1, 'listening.dashboard', 1, '2021-04-04 12:50:15'),
(320, 1, 'listening.dashboard', 1, '2021-04-04 12:55:49'),
(321, 1, 'listening.dashboard', 1, '2021-04-04 12:56:05'),
(322, 1, 'listening.dashboard', 1, '2021-04-04 12:56:30'),
(323, 1, 'listening.dashboard', 1, '2021-04-04 12:56:40'),
(324, 1, 'listening.dashboard', 1, '2021-04-04 13:04:16'),
(325, 1, 'listening.dashboard', 1, '2021-04-04 13:05:38'),
(326, 1, 'listening.dashboard', 1, '2021-04-04 13:06:53'),
(327, 1, 'listening.dashboard', 1, '2021-04-04 13:07:41'),
(328, 1, 'listening.dashboard', 1, '2021-04-04 13:08:32'),
(329, 1, 'listening.dashboard', 1, '2021-04-04 13:21:58'),
(330, 1, 'listening.dashboard', 1, '2021-04-04 13:22:25'),
(331, 1, 'listening.dashboard', 1, '2021-04-04 13:33:50'),
(332, 1, 'listening.dashboard', 1, '2021-04-04 13:37:46'),
(333, 1, 'listening.dashboard', 1, '2021-04-04 13:38:00'),
(334, 1, 'listening.dashboard', 1, '2021-04-04 13:38:09'),
(335, 1, 'listening.dashboard', 1, '2021-04-04 13:38:42'),
(336, 1, 'home.show', 1, '2021-04-04 13:39:13'),
(337, 1, 'listening.dashboard', 1, '2021-04-04 13:42:02'),
(338, 1, 'home.show', 1, '2021-04-04 14:59:09'),
(339, 1, 'home.show', 1, '2021-04-04 15:01:03'),
(340, 1, 'listening.dashboard', 1, '2021-04-04 15:02:52'),
(341, 1, 'home.show', 1, '2021-04-04 15:02:53'),
(342, 1, 'home.show', 1, '2021-04-04 15:03:19'),
(343, 1, 'home.show', 1, '2021-04-04 15:04:16'),
(344, 1, 'home.show', 1, '2021-04-04 15:04:33'),
(345, 1, 'home.show', 1, '2021-04-04 15:06:59'),
(346, 1, 'home.show', 1, '2021-04-04 15:07:19'),
(347, 1, 'home.show', 1, '2021-04-04 15:07:50'),
(348, 1, 'listening.dashboard', 1, '2021-04-04 15:09:02'),
(349, 1, 'listening.dashboard', 1, '2021-04-04 15:09:16'),
(350, 1, 'listening.dashboard', 1, '2021-04-04 15:09:49'),
(351, 1, 'listening.dashboard', 1, '2021-04-04 15:10:00'),
(352, 1, 'listening.dashboard', 1, '2021-04-04 15:10:03'),
(353, 1, 'listening.dashboard', 1, '2021-04-04 15:11:02'),
(354, 1, 'listening.dashboard', 1, '2021-04-04 15:11:21'),
(355, 1, 'listening.dashboard', 1, '2021-04-04 15:11:33'),
(356, 1, 'listening.dashboard', 1, '2021-04-04 15:11:52'),
(357, 1, 'listening.dashboard', 1, '2021-04-04 15:12:16'),
(358, 1, 'listening.dashboard', 1, '2021-04-04 15:13:07'),
(359, 1, 'listening.dashboard', 1, '2021-04-04 15:14:50'),
(360, 1, 'listening.dashboard', 1, '2021-04-04 15:15:14'),
(361, 1, 'listening.dashboard', 1, '2021-04-04 15:15:19'),
(362, 1, 'listening.dashboard', 1, '2021-04-04 15:15:55'),
(363, 1, 'listening.dashboard', 1, '2021-04-04 15:19:49'),
(364, 1, 'listening.dashboard', 1, '2021-04-04 15:20:37'),
(365, 1, 'listening.dashboard', 1, '2021-04-04 15:20:51'),
(366, 1, 'listening.dashboard', 1, '2021-04-04 15:21:28'),
(367, 1, 'listening.dashboard', 1, '2021-04-04 15:22:07'),
(368, 1, 'listening.dashboard', 1, '2021-04-04 15:25:05'),
(369, 1, 'listening.dashboard', 1, '2021-04-04 15:25:12'),
(370, 1, 'listening.dashboard', 1, '2021-04-04 15:26:18'),
(371, 1, 'listening.dashboard', 1, '2021-04-04 15:26:26'),
(372, 1, 'listening.dashboard', 1, '2021-04-04 15:27:12'),
(373, 1, 'listening.dashboard', 1, '2021-04-04 15:28:04'),
(374, 1, 'listening.dashboard', 1, '2021-04-04 15:28:25'),
(375, 1, 'listening.dashboard', 1, '2021-04-04 15:28:43'),
(376, 1, 'listening.dashboard', 1, '2021-04-04 15:28:48'),
(377, 1, 'listening.dashboard', 1, '2021-04-04 15:28:53'),
(378, 1, 'listening.dashboard', 1, '2021-04-04 15:29:19'),
(379, 1, 'listening.dashboard', 1, '2021-04-04 15:29:34'),
(380, 1, 'listening.dashboard', 1, '2021-04-04 15:30:02'),
(381, 1, 'listening.dashboard', 1, '2021-04-04 15:30:22'),
(382, 1, 'listening.dashboard', 1, '2021-04-04 15:31:10'),
(383, 1, 'listening.dashboard', 1, '2021-04-04 15:31:22'),
(384, 1, 'listening.dashboard', 1, '2021-04-04 15:32:12'),
(385, 1, 'listening.dashboard', 1, '2021-04-04 15:37:41'),
(386, 1, 'listening.weekform', 1, '2021-04-04 15:42:18'),
(387, 1, 'listening.weekform', 1, '2021-04-04 15:42:43'),
(388, 1, 'listening.weekform', 1, '2021-04-04 15:43:42'),
(389, 1, 'listening.weekform', 1, '2021-04-04 15:45:11'),
(390, 1, 'listening.weekform', 1, '2021-04-04 15:46:10'),
(391, 1, 'listening.dashboard', 1, '2021-04-04 15:46:32'),
(392, 1, 'listening.weekform', 1, '2021-04-04 15:47:30'),
(393, 1, 'listening.dashboard', 1, '2021-04-04 15:48:26'),
(394, 1, 'listening.weekform', 1, '2021-04-04 15:48:30'),
(395, 1, 'listening.weekform', 1, '2021-04-04 15:48:42'),
(396, 1, 'listening.weekform', 1, '2021-04-04 15:49:25'),
(397, 1, 'auth.login', 1, '2021-04-05 09:15:44'),
(398, 1, 'home.show', 1, '2021-04-05 09:15:44'),
(399, 1, 'home.show', 1, '2021-04-05 09:17:15'),
(400, 1, 'home.show', 1, '2021-04-05 09:17:40'),
(401, 1, 'home.show', 1, '2021-04-05 09:18:10'),
(402, 1, 'home.show', 1, '2021-04-05 09:28:11'),
(403, 1, 'listening.dashboard', 1, '2021-04-05 09:28:31'),
(404, 1, 'home.show', 1, '2021-04-05 09:28:46'),
(405, 1, 'home.show', 1, '2021-04-05 09:38:17'),
(406, 1, 'auth.login', 1, '2021-04-05 10:05:49'),
(407, 1, 'home.show', 1, '2021-04-05 10:05:49'),
(408, 1, 'home.show', 1, '2021-04-05 10:07:10'),
(409, 1, 'home.show', 1, '2021-04-05 10:11:08'),
(410, 1, 'home.show', 1, '2021-04-05 10:40:03'),
(411, 1, 'home.show', 1, '2021-04-05 10:41:09'),
(412, 1, 'home.show', 1, '2021-04-05 10:42:11'),
(413, 1, 'listening.dashboard', 1, '2021-04-05 10:48:10'),
(414, 1, 'listening.dashboard', 1, '2021-04-05 10:48:53'),
(415, 1, 'listening.dashboard', 1, '2021-04-05 10:48:57'),
(416, 1, 'listening.dashboard', 1, '2021-04-05 10:49:10'),
(417, 1, 'listening.dashboard', 1, '2021-04-05 10:49:25'),
(418, 1, 'listening.dashboard', 1, '2021-04-05 10:49:39'),
(419, 1, 'listening.dashboard', 1, '2021-04-05 10:49:47'),
(420, 1, 'listening.dashboard', 1, '2021-04-05 10:50:12'),
(421, 1, 'listening.dashboard', 1, '2021-04-05 10:50:24'),
(422, 1, 'listening.dashboard', 1, '2021-04-05 10:50:42'),
(423, 1, 'listening.dashboard', 1, '2021-04-05 11:02:00'),
(424, 1, 'listening.dashboard', 1, '2021-04-05 11:04:52'),
(425, 1, 'listening.dashboard', 1, '2021-04-05 11:05:05'),
(426, 1, 'listening.dashboard', 1, '2021-04-05 11:06:38'),
(427, 1, 'listening.dashboard', 1, '2021-04-05 11:06:47'),
(428, 1, 'listening.dashboard', 1, '2021-04-05 11:07:11'),
(429, 1, 'listening.dashboard', 1, '2021-04-05 11:07:21'),
(430, 1, 'listening.dashboard', 1, '2021-04-05 11:07:39'),
(431, 1, 'listening.dashboard', 1, '2021-04-05 11:07:40'),
(432, 1, 'listening.dashboard', 1, '2021-04-05 11:13:15'),
(433, 1, 'listening.dashboard', 1, '2021-04-05 11:13:18'),
(434, 1, 'listening.dashboard', 1, '2021-04-05 11:14:29'),
(435, 1, 'listening.dashboard', 1, '2021-04-05 11:14:50'),
(436, 1, 'listening.dashboard', 1, '2021-04-05 11:15:04'),
(437, 1, 'listening.dashboard', 1, '2021-04-05 11:15:26'),
(438, 1, 'listening.dashboard', 1, '2021-04-05 11:15:37'),
(439, 1, 'listening.dashboard', 1, '2021-04-05 11:17:01'),
(440, 1, 'listening.dashboard', 1, '2021-04-05 11:17:12'),
(441, 1, 'listening.dashboard', 1, '2021-04-05 11:18:12'),
(442, 1, 'listening.dashboard', 1, '2021-04-05 11:18:27'),
(443, 1, 'listening.dashboard', 1, '2021-04-05 11:19:20'),
(444, 1, 'listening.dashboard', 1, '2021-04-05 11:19:25'),
(445, 1, 'listening.dashboard', 1, '2021-04-05 11:19:35'),
(446, 1, 'listening.dashboard', 1, '2021-04-05 11:19:51'),
(447, 1, 'listening.dashboard', 1, '2021-04-05 11:20:12'),
(448, 1, 'listening.dashboard', 1, '2021-04-05 11:20:58'),
(449, 1, 'listening.dashboard', 1, '2021-04-05 11:21:17'),
(450, 1, 'listening.dashboard', 1, '2021-04-05 11:21:47'),
(451, 1, 'listening.dashboard', 1, '2021-04-05 11:21:56'),
(452, 1, 'listening.dashboard', 1, '2021-04-05 11:23:47'),
(453, 1, 'listening.dashboard', 1, '2021-04-05 11:24:05'),
(454, 1, 'listening.dashboard', 1, '2021-04-05 11:29:06'),
(455, 1, 'listening.dashboard', 1, '2021-04-05 11:29:21'),
(456, 1, 'listening.weekform', 1, '2021-04-05 11:29:41'),
(457, 1, 'listening.weekform', 1, '2021-04-05 11:30:16'),
(458, 1, 'listening.weekform', 1, '2021-04-05 11:30:32'),
(459, 1, 'listening.weekform', 1, '2021-04-05 11:30:52'),
(460, 1, 'listening.weekform', 1, '2021-04-05 11:31:23'),
(461, 1, 'listening.weekform', 1, '2021-04-05 11:33:02'),
(462, 1, 'listening.weekform', 1, '2021-04-05 11:33:22'),
(463, 1, 'listening.weekform', 1, '2021-04-05 11:33:31'),
(464, 1, 'listening.weekform', 1, '2021-04-05 11:33:39'),
(465, 1, 'listening.weekform', 1, '2021-04-05 11:33:48'),
(466, 1, 'listening.weekform', 1, '2021-04-05 11:36:18'),
(467, 1, 'listening.weekform', 1, '2021-04-05 11:36:28'),
(468, 1, 'listening.weekform', 1, '2021-04-05 11:36:44'),
(469, 1, 'listening.weekform', 1, '2021-04-05 11:36:54'),
(470, 1, 'listening.weekform', 1, '2021-04-05 11:37:01'),
(471, 1, 'listening.weekform', 1, '2021-04-05 11:45:05'),
(472, 1, 'listening.weekform', 1, '2021-04-05 11:45:45'),
(473, 1, 'listening.weekform', 1, '2021-04-05 11:46:07'),
(474, 1, 'listening.weekform', 1, '2021-04-05 11:46:13'),
(475, 1, 'listening.weekform', 1, '2021-04-05 11:46:16'),
(476, 1, 'listening.weekform', 1, '2021-04-05 11:47:22'),
(477, 1, 'listening.weekform', 1, '2021-04-05 11:47:29'),
(478, 1, 'listening.weekform', 1, '2021-04-05 11:47:55'),
(479, 1, 'listening.weekform', 1, '2021-04-05 11:48:20'),
(480, 1, 'listening.weekform', 1, '2021-04-05 11:48:28'),
(481, 1, 'listening.weekform', 1, '2021-04-05 11:48:38'),
(482, 1, 'listening.weekform', 1, '2021-04-05 11:48:46'),
(483, 1, 'listening.weekform', 1, '2021-04-05 11:49:01'),
(484, 1, 'listening.weekform', 1, '2021-04-05 11:49:08'),
(485, 1, 'listening.weekform', 1, '2021-04-05 11:51:03'),
(486, 1, 'listening.weekform', 1, '2021-04-05 11:51:13'),
(487, 1, 'listening.weekform', 1, '2021-04-05 11:51:21'),
(488, 1, 'listening.weekform', 1, '2021-04-05 11:51:28'),
(489, 1, 'listening.weekform', 1, '2021-04-05 11:51:33'),
(490, 1, 'listening.weekform', 1, '2021-04-05 11:51:41'),
(491, 1, 'listening.weekform', 1, '2021-04-05 11:51:52'),
(492, 1, 'listening.weekform', 1, '2021-04-05 11:52:01'),
(493, 1, 'listening.weekform', 1, '2021-04-05 11:52:06'),
(494, 1, 'listening.weekform', 1, '2021-04-05 11:53:00'),
(495, 1, 'listening.weekform', 1, '2021-04-05 11:53:10'),
(496, 1, 'listening.weekform', 1, '2021-04-05 11:53:17'),
(497, 1, 'listening.weekform', 1, '2021-04-05 11:56:06'),
(498, 1, 'listening.weekform', 1, '2021-04-05 11:56:48'),
(499, 1, 'listening.dashboard', 1, '2021-04-05 11:57:02'),
(500, 1, 'listening.weekform', 1, '2021-04-05 11:57:04'),
(501, 1, 'listening.weekform', 1, '2021-04-05 11:58:17'),
(502, 1, 'listening.weekform', 1, '2021-04-05 11:58:31'),
(503, 1, 'listening.weekform', 1, '2021-04-05 11:58:37'),
(504, 1, 'listening.weekform', 1, '2021-04-05 11:58:47'),
(505, 1, 'listening.weekform', 1, '2021-04-05 11:58:53'),
(506, 1, 'listening.weekform', 1, '2021-04-05 11:59:22'),
(507, 1, 'listening.weekform', 1, '2021-04-05 11:59:30'),
(508, 1, 'listening.weekform', 1, '2021-04-05 12:02:52'),
(509, 1, 'listening.weekform', 1, '2021-04-05 12:03:01'),
(510, 1, 'listening.weekform', 1, '2021-04-05 12:03:57'),
(511, 1, 'listening.weekform', 1, '2021-04-05 12:04:57'),
(512, 1, 'listening.weekform', 1, '2021-04-05 12:05:33'),
(513, 1, 'listening.weekform', 1, '2021-04-05 12:06:48'),
(514, 1, 'listening.weekform', 1, '2021-04-05 12:06:54'),
(515, 1, 'listening.weekform', 1, '2021-04-05 12:07:55'),
(516, 1, 'listening.weekform', 1, '2021-04-05 12:08:03'),
(517, 1, 'listening.weekform', 1, '2021-04-05 12:08:07'),
(518, 1, 'listening.weekform', 1, '2021-04-05 12:08:59'),
(519, 1, 'listening.dashboard', 1, '2021-04-05 12:09:00'),
(520, 1, 'listening.weekform', 1, '2021-04-05 12:09:02'),
(521, 1, 'listening.weekform', 1, '2021-04-05 12:09:07'),
(522, 1, 'listening.weekform', 1, '2021-04-05 12:09:40'),
(523, 1, 'listening.dashboard', 1, '2021-04-05 12:09:41'),
(524, 1, 'listening.weekform', 1, '2021-04-05 12:09:43'),
(525, 1, 'listening.weekform', 1, '2021-04-05 12:10:14'),
(526, 1, 'listening.weekform', 1, '2021-04-05 12:10:15'),
(527, 1, 'listening.weekform', 1, '2021-04-05 14:02:24'),
(528, 1, 'auth.login', 1, '2021-04-05 20:28:00'),
(529, 1, 'home.show', 1, '2021-04-05 20:28:00'),
(530, 1, 'listening.dashboard', 1, '2021-04-05 20:28:52'),
(531, 1, 'listening.dashboard', 1, '2021-04-05 20:31:51'),
(532, 1, 'listening.dashboard', 1, '2021-04-05 20:31:53'),
(533, 1, 'listening.dashboard', 1, '2021-04-05 20:33:50'),
(534, 1, 'listening.dashboard', 1, '2021-04-05 20:34:17'),
(535, 1, 'listening.weeknew', 1, '2021-04-05 20:36:33'),
(536, 1, 'listening.weeknew', 1, '2021-04-05 20:36:41'),
(537, 1, 'listening.weeknew', 1, '2021-04-05 20:41:59'),
(538, 1, 'listening.weeknew', 1, '2021-04-05 20:42:25'),
(539, 1, 'listening.weeknew', 1, '2021-04-05 20:42:26'),
(540, 1, 'listening.weeknew', 1, '2021-04-05 20:43:40'),
(541, 1, 'listening.weeknew', 1, '2021-04-05 20:44:53'),
(542, 1, 'listening.weeknew', 1, '2021-04-05 20:45:06'),
(543, 1, 'listening.weeknew', 1, '2021-04-05 20:45:12'),
(544, 1, 'listening.weeknew', 1, '2021-04-05 20:45:18'),
(545, 1, 'listening.weeknew', 1, '2021-04-05 20:45:20'),
(546, 1, 'listening.weeknew', 1, '2021-04-05 20:45:23'),
(547, 1, 'listening.weeknew', 1, '2021-04-05 20:45:24'),
(548, 1, 'listening.weeknew', 1, '2021-04-05 20:45:28'),
(549, 1, 'listening.weeknew', 1, '2021-04-05 20:45:38'),
(550, 1, 'listening.weeknew', 1, '2021-04-05 20:45:50'),
(551, 1, 'listening.weeknew', 1, '2021-04-05 20:46:07'),
(552, 1, 'listening.weeknew', 1, '2021-04-05 20:46:12'),
(553, 1, 'listening.weeknew', 1, '2021-04-05 20:46:14'),
(554, 1, 'listening.weeknew', 1, '2021-04-05 20:46:17'),
(555, 1, 'listening.weeknew', 1, '2021-04-05 20:46:21'),
(556, 1, 'listening.weeknew', 1, '2021-04-05 20:46:29'),
(557, 1, 'listening.weeknew', 1, '2021-04-05 20:46:36'),
(558, 1, 'listening.weeknew', 1, '2021-04-05 20:47:04'),
(559, 1, 'listening.weeknew', 1, '2021-04-05 20:47:10'),
(560, 1, 'listening.weeknew', 1, '2021-04-05 20:47:18'),
(561, 1, 'listening.weeknew', 1, '2021-04-05 20:47:20'),
(562, 1, 'listening.weeknew', 1, '2021-04-05 20:47:35'),
(563, 1, 'listening.weeknew', 1, '2021-04-05 20:47:40'),
(564, 1, 'listening.weeknew', 1, '2021-04-05 20:49:08'),
(565, 1, 'listening.weeknew', 1, '2021-04-05 20:49:45'),
(566, 1, 'listening.weeknew', 1, '2021-04-05 20:52:25'),
(567, 1, 'listening.weeknew', 1, '2021-04-05 20:54:09'),
(568, 1, 'listening.weeknew', 1, '2021-04-05 20:54:56'),
(569, 1, 'listening.weeknew', 1, '2021-04-05 20:54:57'),
(570, 1, 'auth.logout', 1, '2021-04-05 20:54:59'),
(571, 1, 'auth.login', 1, '2021-04-05 20:55:03'),
(572, 1, 'home.show', 1, '2021-04-05 20:55:03'),
(573, 1, 'listening.dashboard', 1, '2021-04-05 21:16:02'),
(574, 1, 'listening.weekform', 1, '2021-04-05 21:16:05'),
(575, 1, 'listening.weekform', 1, '2021-04-05 21:16:08'),
(576, 1, 'listening.weekform', 1, '2021-04-05 21:16:42'),
(577, 1, 'listening.weekform', 1, '2021-04-05 21:16:45'),
(578, 1, 'listening.weekform', 1, '2021-04-05 21:27:21'),
(579, 1, 'listening.dashboard', 1, '2021-04-05 21:27:26'),
(580, 1, 'listening.weeknew', 1, '2021-04-05 21:27:27'),
(581, 1, 'listening.weeknew', 1, '2021-04-05 21:27:42'),
(582, 1, 'listening.weeknew', 1, '2021-04-05 21:28:00'),
(583, 1, 'listening.weeknew', 1, '2021-04-05 21:28:27'),
(584, 1, 'listening.weeknew', 1, '2021-04-05 21:28:53'),
(585, 1, 'listening.weeknew', 1, '2021-04-05 21:29:31'),
(586, 1, 'listening.dashboard', 1, '2021-04-05 21:29:59'),
(587, 1, 'listening.weeknew', 1, '2021-04-05 21:33:58'),
(588, 1, 'listening.weeknew', 1, '2021-04-05 21:34:10'),
(589, 1, 'listening.weeknew', 1, '2021-04-05 21:34:24'),
(590, 1, 'listening.weeknew', 1, '2021-04-05 21:34:29'),
(591, 1, 'listening.weeknew', 1, '2021-04-06 00:27:23'),
(592, 1, 'auth.login', 1, '2021-04-06 00:27:37'),
(593, 1, 'home.show', 1, '2021-04-06 00:27:37'),
(594, 1, 'listening.dashboard', 1, '2021-04-06 00:27:48'),
(595, 1, 'listening.dashboard', 1, '2021-04-06 00:37:42'),
(596, 1, 'listening.dashboard', 1, '2021-04-06 01:48:18'),
(597, 1, 'listening.weeknew', 1, '2021-04-06 01:55:58'),
(598, 1, 'listening.weeknew', 1, '2021-04-06 01:56:22'),
(599, 1, 'listening.weeknew', 1, '2021-04-06 01:56:34'),
(600, 1, 'listening.weeknew', 1, '2021-04-06 01:56:38'),
(601, 1, 'listening.weeknew', 1, '2021-04-06 01:56:41'),
(602, 1, 'listening.weeknew', 1, '2021-04-06 01:58:01'),
(603, 1, 'listening.weeknew', 1, '2021-04-06 01:58:24'),
(604, 1, 'listening.weeknew', 1, '2021-04-06 01:58:39'),
(605, 1, 'listening.weeknew', 1, '2021-04-06 01:58:51'),
(606, 1, 'listening.weeknew', 1, '2021-04-06 01:59:32'),
(607, 1, 'listening.weeknew', 1, '2021-04-06 01:59:42'),
(608, 1, 'listening.weeknew', 1, '2021-04-06 01:59:48'),
(609, 1, 'listening.weeknew', 1, '2021-04-06 02:00:06'),
(610, 1, 'listening.weeknew', 1, '2021-04-06 02:00:25'),
(611, 1, 'listening.weeknew', 1, '2021-04-06 02:01:03'),
(612, 1, 'listening.weeknew', 1, '2021-04-06 02:01:26'),
(613, 1, 'listening.weeknew', 1, '2021-04-06 02:01:27'),
(614, 1, 'listening.weeknew', 1, '2021-04-06 02:01:31'),
(615, 1, 'listening.weeknew', 1, '2021-04-06 02:01:32'),
(616, 1, 'listening.weeknew', 1, '2021-04-06 02:02:59'),
(617, 1, 'listening.weeknew', 1, '2021-04-06 02:03:01'),
(618, 1, 'listening.weeknew', 1, '2021-04-06 02:03:10'),
(619, 1, 'listening.weeknew', 1, '2021-04-06 02:03:19'),
(620, 1, 'listening.weeknew', 1, '2021-04-06 02:03:22'),
(621, 1, 'listening.weeknew', 1, '2021-04-06 02:03:27'),
(622, 1, 'listening.weeknew', 1, '2021-04-06 02:04:00'),
(623, 1, 'listening.weeknew', 1, '2021-04-06 02:04:04'),
(624, 1, 'listening.weeknew', 1, '2021-04-06 02:04:46'),
(625, 1, 'listening.weeknew', 1, '2021-04-06 02:04:54'),
(626, 1, 'listening.weeknew', 1, '2021-04-06 02:04:59'),
(627, 1, 'listening.dashboard', 1, '2021-04-06 02:05:42'),
(628, 1, 'listening.weeknew', 1, '2021-04-06 02:06:04'),
(629, 1, 'listening.weeknew', 1, '2021-04-06 02:07:41'),
(630, 1, 'listening.weeknew', 1, '2021-04-06 03:48:17'),
(631, 1, 'listening.weeknew', 1, '2021-04-06 03:49:03'),
(632, 1, 'listening.weeknew', 1, '2021-04-06 03:49:46'),
(633, 1, 'listening.weeknew', 1, '2021-04-06 03:49:49'),
(634, 1, 'listening.weeknew', 1, '2021-04-06 03:50:43'),
(635, 1, 'listening.weeknew', 1, '2021-04-06 03:50:46'),
(636, 1, 'listening.dashboard', 1, '2021-04-06 03:51:27'),
(637, 1, 'listening.weeknew', 1, '2021-04-06 03:52:23'),
(638, 1, 'listening.weeknew', 1, '2021-04-06 03:52:28'),
(639, 1, 'listening.weeknew', 1, '2021-04-06 03:53:49'),
(640, 1, 'listening.weeknew', 1, '2021-04-06 03:53:55'),
(641, 1, 'listening.weeknew', 1, '2021-04-06 03:56:27'),
(642, 1, 'listening.weeknew', 1, '2021-04-06 03:56:54'),
(643, 1, 'listening.weeknew', 1, '2021-04-06 03:56:58'),
(644, 1, 'listening.weeknew', 1, '2021-04-06 03:57:06'),
(645, 1, 'listening.weeknew', 1, '2021-04-06 03:58:23'),
(646, 1, 'listening.weeknew', 1, '2021-04-06 03:58:37'),
(647, 1, 'listening.weeknew', 1, '2021-04-06 03:59:19'),
(648, 1, 'listening.weeknew', 1, '2021-04-06 04:00:48'),
(649, 1, 'listening.weeknew', 1, '2021-04-06 04:12:40'),
(650, 1, 'listening.weeknew', 1, '2021-04-06 04:12:59'),
(651, 1, 'listening.weeknew', 1, '2021-04-06 04:13:08'),
(652, 1, 'listening.weeknew', 1, '2021-04-06 04:13:47'),
(653, 1, 'listening.dashboard', 1, '2021-04-06 04:13:55'),
(654, 1, 'listening.dashboard', 1, '2021-04-06 04:14:10'),
(655, 1, 'listening.weeknew', 1, '2021-04-06 04:14:12'),
(656, 1, 'listening.weeknew', 1, '2021-04-06 04:14:17'),
(657, 1, 'listening.weeknew', 1, '2021-04-06 04:15:07'),
(658, 1, 'home.show', 1, '2021-04-06 04:15:12'),
(659, 1, 'listening.dashboard', 1, '2021-04-06 04:15:15'),
(660, 1, 'listening.weeknew', 1, '2021-04-06 04:15:17'),
(661, 1, 'listening.weeknew', 1, '2021-04-06 04:15:33'),
(662, 1, 'listening.weeknew', 1, '2021-04-06 04:16:09'),
(663, 1, 'listening.weeknew', 1, '2021-04-06 04:16:28'),
(664, 1, 'listening.weeknew', 1, '2021-04-06 04:17:41'),
(665, 1, 'listening.weeknew', 1, '2021-04-06 04:18:22'),
(666, 1, 'listening.weeknew', 1, '2021-04-06 04:19:41'),
(667, 1, 'listening.weeknew', 1, '2021-04-06 04:19:50'),
(668, 1, 'listening.weeknew', 1, '2021-04-06 04:20:01'),
(669, 1, 'home.show', 1, '2021-04-06 04:20:05'),
(670, 1, 'listening.dashboard', 1, '2021-04-06 04:20:06'),
(671, 1, 'listening.dashboard', 1, '2021-04-06 04:20:36'),
(672, 1, 'listening.dashboard', 1, '2021-04-06 04:22:28'),
(673, 1, 'listening.dashboard', 1, '2021-04-06 05:07:24'),
(674, 1, 'home.show', 1, '2021-04-06 05:07:26'),
(675, 1, 'listening.dashboard', 1, '2021-04-06 05:07:28'),
(676, 1, 'home.show', 1, '2021-04-06 05:07:29'),
(677, 1, 'listening.dashboard', 1, '2021-04-06 05:07:30'),
(678, 1, 'listening.dashboard', 1, '2021-04-06 05:08:09'),
(679, 1, 'listening.weeknew', 1, '2021-04-06 05:08:26'),
(680, 1, 'listening.dashboard', 1, '2021-04-06 05:08:29'),
(681, 1, 'listening.weekform', 1, '2021-04-06 05:09:50'),
(682, 1, 'listening.weekform', 1, '2021-04-06 05:10:23'),
(683, 1, 'listening.dashboard', 1, '2021-04-06 05:10:25'),
(684, 1, 'listening.dashboard', 1, '2021-04-06 05:10:31'),
(685, 1, 'listening.weeknew', 1, '2021-04-06 05:10:34'),
(686, 1, 'listening.dashboard', 1, '2021-04-06 05:10:37'),
(687, 1, 'listening.dashboard', 1, '2021-04-06 05:15:29'),
(688, 1, 'listening.dashboard', 1, '2021-04-06 05:16:13'),
(689, 1, 'listening.weekform', 1, '2021-04-06 05:16:58'),
(690, 1, 'listening.weekform', 1, '2021-04-06 05:17:06'),
(691, 1, 'listening.weekform', 1, '2021-04-06 05:24:24'),
(692, 1, 'listening.weekform', 1, '2021-04-06 05:31:24'),
(693, 1, 'listening.weekform', 1, '2021-04-06 05:31:32'),
(694, 1, 'listening.weekform', 1, '2021-04-06 05:32:34'),
(695, 1, 'listening.weekform', 1, '2021-04-06 05:32:44'),
(696, 1, 'listening.dashboard', 1, '2021-04-06 05:33:21'),
(697, 1, 'listening.weeknew', 1, '2021-04-06 05:33:22'),
(698, 1, 'listening.weekform', 1, '2021-04-06 05:33:28'),
(699, 1, 'listening.weekform', 1, '2021-04-06 05:34:33'),
(700, 1, 'listening.weekform', 1, '2021-04-06 05:34:38'),
(701, 1, 'listening.dashboard', 1, '2021-04-06 05:34:45'),
(702, 1, 'listening.weeknew', 1, '2021-04-06 05:34:49'),
(703, 1, 'listening.weekform', 1, '2021-04-06 05:34:52'),
(704, 1, 'listening.dashboard', 1, '2021-04-06 05:35:00'),
(705, 1, 'listening.weeknew', 1, '2021-04-06 05:36:46'),
(706, 1, 'listening.weeknew', 1, '2021-04-06 05:36:50'),
(707, 1, 'listening.weeknew', 1, '2021-04-06 05:37:14'),
(708, 1, 'listening.weeknew', 1, '2021-04-06 05:38:26'),
(709, 1, 'listening.weeknew', 1, '2021-04-06 05:38:45'),
(710, 1, 'listening.weeknew', 1, '2021-04-06 05:38:51'),
(711, 1, 'listening.weeknew', 1, '2021-04-06 05:43:18'),
(712, 1, 'listening.weeknew', 1, '2021-04-06 05:43:25'),
(713, 1, 'listening.weeknew', 1, '2021-04-06 05:44:12'),
(714, 1, 'listening.weeknew', 1, '2021-04-06 05:44:22'),
(715, 1, 'listening.weeknew', 1, '2021-04-06 05:44:32'),
(716, 1, 'listening.weeknew', 1, '2021-04-06 05:45:26'),
(717, 1, 'listening.weeknew', 1, '2021-04-06 05:45:40'),
(718, 1, 'listening.weeknew', 1, '2021-04-06 05:45:49'),
(719, 1, 'listening.weeknew', 1, '2021-04-06 05:46:02'),
(720, 1, 'listening.weeknew', 1, '2021-04-06 05:46:28'),
(721, 1, 'listening.weeknew', 1, '2021-04-06 05:50:11'),
(722, 1, 'listening.weeknew', 1, '2021-04-06 05:50:29'),
(723, 1, 'listening.dashboard', 1, '2021-04-06 05:50:29'),
(724, 1, 'listening.dashboard', 1, '2021-04-06 05:50:35'),
(725, 1, 'listening.dashboard', 1, '2021-04-06 05:52:04'),
(726, 1, 'listening.weekform', 1, '2021-04-06 05:52:14'),
(727, 1, 'listening.dashboard', 1, '2021-04-06 05:52:22'),
(728, 1, 'listening.weekform', 1, '2021-04-06 05:52:23'),
(729, 1, 'listening.weekform', 1, '2021-04-06 05:53:13'),
(730, 1, 'listening.dashboard', 1, '2021-04-06 05:53:18'),
(731, 1, 'listening.weekform', 1, '2021-04-06 05:53:20'),
(732, 1, 'listening.weekform', 1, '2021-04-06 05:54:11'),
(733, 1, 'listening.weekform', 1, '2021-04-06 05:56:05'),
(734, 1, 'listening.weekform', 1, '2021-04-06 05:56:44'),
(735, 1, 'listening.weekform', 1, '2021-04-06 05:59:12'),
(736, 1, 'listening.dashboard', 1, '2021-04-06 05:59:14'),
(737, 1, 'listening.dashboard', 1, '2021-04-06 05:59:52'),
(738, 1, 'listening.weekform', 1, '2021-04-06 05:59:55'),
(739, 1, 'listening.weekform', 1, '2021-04-06 06:00:59'),
(740, 1, 'listening.weekform', 1, '2021-04-06 06:01:03'),
(741, 1, 'listening.weekform', 1, '2021-04-06 06:01:16'),
(742, 1, 'listening.weekform', 1, '2021-04-06 06:02:27'),
(743, 1, 'listening.weekform', 1, '2021-04-06 06:04:37'),
(744, 1, 'listening.weeks', 1, '2021-04-06 06:04:51'),
(745, 1, 'listening.dashboard', 1, '2021-04-06 06:04:56'),
(746, 1, 'listening.weeknew', 1, '2021-04-06 06:05:03'),
(747, 1, 'listening.weeknew', 1, '2021-04-06 06:05:09'),
(748, 1, 'listening.weeknew', 1, '2021-04-06 06:05:38'),
(749, 1, 'listening.dashboard', 1, '2021-04-06 06:05:38'),
(750, 1, 'listening.weekform', 1, '2021-04-06 06:05:41'),
(751, 1, 'listening.weekform', 1, '2021-04-06 06:05:45'),
(752, 1, 'listening.weekform', 1, '2021-04-06 06:06:22'),
(753, 1, 'listening.dashboard', 1, '2021-04-06 06:06:22'),
(754, 1, 'listening.weekform', 1, '2021-04-06 06:06:25'),
(755, 1, 'listening.weekform', 1, '2021-04-06 06:06:30'),
(756, 1, 'listening.dashboard', 1, '2021-04-06 06:06:30'),
(757, 1, 'listening.weekform', 1, '2021-04-06 07:09:36'),
(758, 1, 'listening.weekform', 1, '2021-04-06 07:09:39'),
(759, 1, 'listening.dashboard', 1, '2021-04-06 07:09:39'),
(760, 1, 'listening.weekform', 1, '2021-04-06 07:10:17'),
(761, 1, 'listening.weekform', 1, '2021-04-06 07:10:21'),
(762, 1, 'listening.dashboard', 1, '2021-04-06 07:10:35'),
(763, 1, 'listening.weekform', 1, '2021-04-06 07:11:01'),
(764, 1, 'listening.weekform', 1, '2021-04-06 07:11:05'),
(765, 1, 'listening.weekform', 1, '2021-04-06 07:12:04'),
(766, 1, 'listening.weekform', 1, '2021-04-06 07:12:06'),
(767, 1, 'listening.weekform', 1, '2021-04-06 07:12:09'),
(768, 1, 'listening.weekform', 1, '2021-04-06 07:12:36'),
(769, 1, 'listening.dashboard', 1, '2021-04-06 07:12:36'),
(770, 1, 'listening.weekform', 1, '2021-04-06 07:12:43'),
(771, 1, 'listening.weekform', 1, '2021-04-06 07:12:47'),
(772, 1, 'listening.dashboard', 1, '2021-04-06 07:12:47'),
(773, 1, 'listening.dashboard', 1, '2021-04-06 07:14:46'),
(774, 1, 'listening.weekform', 1, '2021-04-06 07:14:49'),
(775, 1, 'listening.dashboard', 1, '2021-04-06 07:14:51'),
(776, 1, 'listening.dashboard', 1, '2021-04-06 07:15:04'),
(777, 1, 'listening.weekform', 1, '2021-04-06 07:15:08'),
(778, 1, 'listening.dashboard', 1, '2021-04-06 07:16:09'),
(779, 1, 'listening.weekform', 1, '2021-04-06 07:20:39'),
(780, 1, 'listening.weekform', 1, '2021-04-06 07:20:42'),
(781, 1, 'listening.weekform', 1, '2021-04-06 07:21:19'),
(782, 1, 'listening.dashboard', 1, '2021-04-06 07:21:19'),
(783, 1, 'listening.weekform', 1, '2021-04-06 07:22:34'),
(784, 1, 'listening.weekform', 1, '2021-04-06 07:22:37'),
(785, 1, 'listening.dashboard', 1, '2021-04-06 07:22:37'),
(786, 1, 'listening.weekform', 1, '2021-04-06 07:23:00'),
(787, 1, 'listening.weekform', 1, '2021-04-06 07:23:02'),
(788, 1, 'listening.dashboard', 1, '2021-04-06 07:23:02'),
(789, 1, 'listening.weekform', 1, '2021-04-06 07:23:09'),
(790, 1, 'listening.weekform', 1, '2021-04-06 07:23:11'),
(791, 1, 'listening.dashboard', 1, '2021-04-06 07:23:11'),
(792, 1, 'listening.weekform', 1, '2021-04-06 07:23:15'),
(793, 1, 'listening.dashboard', 1, '2021-04-06 07:23:38');

-- --------------------------------------------------------

--
-- Структура таблицы `slides`
--

CREATE TABLE `slides` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `url` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category` int NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `slides`
--

INSERT INTO `slides` (`id`, `user`, `url`, `date`, `category`, `status`) VALUES
(1, 1, 'https://i.picsum.photos/id/809/400/250.jpg?hmac=2f-6tfCMn0Xqf1ekbU_3yjVTiOttcDGgBw8me_BFdDw', '2020-12-06 00:55:42', 0, 1),
(2, 1, 'https://i.picsum.photos/id/545/400/250.jpg?hmac=dwV06WncX9F-q2Yw37l-ffGz1iH3Lt9aFX4ya5RSYiw', '2020-12-06 00:56:51', 0, 1),
(3, 1, 'https://i.picsum.photos/id/2/400/250.jpg?hmac=xQarGFhPP7UqhRdaEoqEBcWnlHuctr99TQdkB9fv3O8', '2020-12-06 00:56:51', 0, 1),
(4, 1, 'https://i.picsum.photos/id/1079/400/250.jpg?hmac=InqR2wOl1RhfBRCZIlImg1KQdZFCs3OUc_vzn3BQpBU', '2020-12-06 00:57:18', 0, 1),
(5, 1, 'https://i.picsum.photos/id/580/400/250.jpg?hmac=TGatCf-0jVubHnohnmf0vOr4-ySdVgiQ021ShH_L2KY', '2020-12-06 00:57:18', 0, 1),
(6, 1, 'https://i.picsum.photos/id/80/400/250.jpg?hmac=_SdiunS86ZDGpytlABJ7u16UWvd8UT-H01atgESh_D0', '2020-12-06 00:57:24', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tips`
--

CREATE TABLE `tips` (
  `id` int NOT NULL,
  `text` text NOT NULL,
  `user` int NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `tips`
--

INSERT INTO `tips` (`id`, `text`, `user`, `date`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, '2020-12-07 02:38:34');

-- --------------------------------------------------------

--
-- Структура таблицы `tips_user`
--

CREATE TABLE `tips_user` (
  `id` int NOT NULL,
  `user` int NOT NULL,
  `tips` int NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `tips_user`
--

INSERT INTO `tips_user` (`id`, `user`, `tips`, `status`, `date`) VALUES
(1, 1, 1, 0, '2020-12-07 02:51:10');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `modified`, `created`) VALUES
(1, 'admin', '$2y$10$MszeSZpsEJI93y19.4EXUe78rXAkWI5loFH5j9sDrgq5pFNBzqbre', 'lostov@hi.uz', NULL, '2020-12-03 03:38:33');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `GROUP.NAME` (`name`) USING BTREE;

--
-- Индексы таблицы `config_colors`
--
ALTER TABLE `config_colors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `listening`
--
ALTER TABLE `listening`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `listening_days`
--
ALTER TABLE `listening_days`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `listening_weeks`
--
ALTER TABLE `listening_weeks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tips`
--
ALTER TABLE `tips`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tips_user`
--
ALTER TABLE `tips_user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `config_colors`
--
ALTER TABLE `config_colors`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `items`
--
ALTER TABLE `items`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `listening`
--
ALTER TABLE `listening`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `listening_days`
--
ALTER TABLE `listening_days`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `listening_weeks`
--
ALTER TABLE `listening_weeks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=794;

--
-- AUTO_INCREMENT для таблицы `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `tips`
--
ALTER TABLE `tips`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `tips_user`
--
ALTER TABLE `tips_user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
