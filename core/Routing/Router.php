<?php

namespace Core\Routing;

use Core\Contracts\Routing\RouterInterface;
use Core\Http\Input;
use Core\Http\Request;
use Core\Http\Response;
use Core\Http\Session;

use Core\Exception\ExceptionHandler;

/**
 * Управление маршрутизацией
 */
class Router implements RouterInterface
{
    /**
     * @var Router
     */
    private static $instance;

    /**
     * URI
     *
     * @var string
     */
    private $uri = '';

    /**
     * Routes
     *
     * @var array
     */
    private $routes = [];

    /**
     *  Routerconstructor.
     */
    private function __construct()
    {
        Session::init();
        $this->setUri();
    }

    /**
     * Singleton
     *
     * @return mixed
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Установщик URI
     */
    private function setUri()
    {
        if (Input::hasGet('uri')) {
            $this->uri = ltrim(Input::get('uri'), '/');

            if (strpos(Request::getRequestUri(), '?uri=') !== false) {
                Response::redirect($this->uri, 301);
            }
        }
    }

    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Добавить маршрут
     *
     * @param string $path
     * @param string $action
     */
    public function add(string $path, string $action, array $params = [])
    {
        $this->routes[$path] = $action;
    }

    /**
     * Добавить маршрут группу
     *
     * @param string $key
     * @param string $values
     */
    public function group(string $key, array $values = [])
    {
        foreach ($values as $path => $action) {
            $this->routes[$key . $path] = $action;
        }
    }

    /**
     * Выполнить маршрутизацию
     *
     * @return mixed
     */
    public function run()
    {
        foreach ($this->routes as $path => $action) {
            if ($this->uri == $path) {
                $logType = 1;
                $logUser = Session::get('user');
                $logAction = strtolower(str_replace("::", ".", $action));
                if($logUser > 0) \App\Models\Log::add($logType, $logAction, $logUser);
                return $this->executeAction($action);
            }
        }

        return $this->executeError404();
    }

    /**
     * Выполнить действие
     *
     * @param string $action
     * @throws Exception Handler
     * @return mixed
     */
    private function executeAction(string $action)
    {
        list($controller, $method) = explode('::', $action);

        $class = '\App\Controllers\\'.ucfirst($controller).'Controller';

        if (!class_exists($class)) {
            throw new ExceptionHandler('Class "'.$class.'" not found.');
        }

        $controllerInstantiate = new $class();

        if (!method_exists($controllerInstantiate, $method)) {
            throw new ExceptionHandler('Method "'.$method.'" not found in '.$class.'.');
        }

        return call_user_func_array([new $controllerInstantiate, $method], []);
    }

    /**
     * Вернуть ошибку 404
     *
     * @return mixed
     */
    private function executeError404()
    {
        $error = new \App\Controllers\ErrorController();

        return $error->show404();
    }
}
