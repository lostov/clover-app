<?php

namespace Core\Routing;

use Core\Exception\ExceptionHandler;

class Helper
{
	private static $links = [];
	private static $menus = [];

	public static function getLink($link)
	{
		$link = array_search($link, self::$links);

		if (substr($link, 0, 1) != '/') {
			$link = '/' . $link;
		}

		return $link;
	}

	public static function addLink($link)
	{
		$arr = explode('@', $link);
		$key = $arr[0];
		$val = $arr[1];
		if (strlen($key) > 0 && strlen($val) > 0) {
			self::$links[$key] = $val;
		}
	}


	public static function addLinks($array)
	{
		self::$links = $array;
	}

	public static function getLinks()
	{
		return self::$links;
	}

	public static function importRoute($array)
	{
		self::$links = $array;
	}

	public static function importMenus($array)
	{
		self::$menus = $array;
	}

	public static function getMenus()
	{
		return self::$menus;
	}
}

?>