<?php

namespace Core\Support;
use Core\Routing\Helper AS RouteHelper;

class HTML
{

	private static $configBackgroundColor = 'black';
	private static $configBackgroundImage = __SITE__ . '/assets/img/sidebar-1.jpg';
	private static $configShowLogo = true;
	private static $configColor = 'rose';
	private static $configLogoIcon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24" fill="#fff"><path d="M12,11.18C15.3,8.18 17,6.64 17,4.69C17,3.19 15.75,2 14.25,2C13.39,2 12.57,2.36 12,3C11.43,2.36 10.61,2 9.69,2C8.19,2 7,3.25 7,4.75C7,6.64 8.7,8.18 12,11.18M11.18,12C8.18,8.7 6.64,7 4.69,7C3.19,7 2,8.25 2,9.75C2,10.61 2.36,11.43 3,12C2.36,12.57 2,13.39 2,14.31C2,15.81 3.25,17 4.75,17C6.64,17 8.18,15.3 11.18,12M12.83,12C15.82,15.3 17.36,17 19.31,17C20.81,17 22,15.75 22,14.25C22,13.39 21.64,12.57 21,12C21.64,11.43 22,10.61 22,9.69C22,8.19 20.75,7 19.25,7C17.36,7 15.82,8.7 12.83,12M12,12.82C8.7,15.82 7,17.36 7,19.31C7,20.81 8.25,22 9.75,22C10.61,22 11.43,21.64 12,21C12.57,21.64 13.39,22 14.31,22C15.81,22 17,20.75 17,19.25C17,17.36 15.3,15.82 12,12.82Z" /></svg>';
	private static $configLogoLabel = 'CloverApp';
	private static $configLogoUrl = '/';
	private static $configDevLabel = 'lostov';
	private static $configDevUrl = 'https://www.t.me/lostov';
	private static $configNavTitle = 'Dashboard';
	private static $configNavAccount = true;
	private static $configNavSettings = true;
	private static $configNavLogout = true;
	private static $configNavDivider = true;
	private static $configSidebarUsername = '';
	private static $configSidebarUsermail = '';
	private static $configSidebarUserlogo = __SITE__ . '/assets/img/faces/user.png';
	private static $configSidebarNavPage = '';
	private static $configColor5 = 'rose';

	public static function sidebar()
	{
		$bgcolor = self::get('configBackgroundColor');
		$bgimage = self::get('configBackgroundImage');
		$color = self::get('configColor');
		$sidebar = '';
		$sidebar .= '<div class="sidebar" data-color="'.$color.'" data-background-color="'.$bgcolor.'" data-image="'.$bgimage.'">';
		$sidebar .= HTML::sidebarLogo();
		$sidebar .= '	<div class="sidebar-wrapper">';
		$sidebar .= HTML::sidebarUser();
		$sidebar .= HTML::sidebarNav();
		$sidebar .= '	</div>';
		$sidebar .= '</div>';
		return $sidebar;
	}

	public static function sidebarLogo()
	{
		if (self::get('configShowLogo')) {
			$icon = self::get('configLogoIcon');
			$label = self::get('configLogoLabel');
			$url = self::get('configLogoUrl');
		}

		return
		<<<html
		    <div class="logo">
		      <a href="{$url}" class="simple-text logo-mini">
		        <i class="mi">
		          {$icon}
		        </i>
		      </a>
		      <a href="#" class="simple-text logo-normal">
		        {$label}
		      </a>
		    </div>
		html;
	}

	public static function sidebarUser()
	{
		$username = self::get('configSidebarUsername');
		$usermail = self::get('configSidebarUsermail');
		$userlogo = self::get('configSidebarUserlogo');
		return
		<<<html
			  <div class="user">
		        <div class="photo">
		          <img src="{$userlogo}" />
		        </div>
		        <div class="user-info">
		          <a data-toggle="collapse" href="#collapseExample" class="username">
		            <span>
		              {$username}[{$usermail}]
		              <b class="caret"></b>
		            </span>
		          </a>
		          <div class="collapse" id="collapseExample">
		            <ul class="nav">
		              <li class="nav-item">
		                <a class="nav-link" href="#">
		                  <span class="sidebar-mini"> MP </span>
		                  <span class="sidebar-normal"> My Profile </span>
		                </a>
		              </li>
		              <li class="nav-item">
		                <a class="nav-link" href="#">
		                  <span class="sidebar-mini"> EP </span>
		                  <span class="sidebar-normal"> Edit Profile </span>
		                </a>
		              </li>
		              <li class="nav-item">
		                <a class="nav-link" href="#">
		                  <span class="sidebar-mini"> S </span>
		                  <span class="sidebar-normal"> Settings </span>
		                </a>
		              </li>
		            </ul>
		          </div>
		        </div>
		      </div>
		html;
	}

	public static function sidebarNav()
	{
		$active = self::get('configSidebarNavPage');
		$ul = '';
		foreach (RouteHelper::getMenus() as $key => $menu) {
			if ($active == $menu['action']) {
				$li = ' active';
			} else {
				$li = '';
			}
			if ($menu['status'] === true) {
			$ul .=
				'<li class="nav-item'.$li.'">
		          <a class="nav-link" href="'.$menu['action'].'">
		            <i class="material-icons">'.$menu['icon'].'</i>
		            <p> '. $menu['title'] .' </p>
		          </a>
		        </li>';

			}
		}
		return
		<<<html
		      <ul class="nav">
		      	{$ul}
		      </ul>
		html;
	}

	public static function navbar()
	{
		$menu = '';
		if (self::get('configNavAccount')) {
			$link = RouteHelper::getLink('Settings::accountSettings');
			$menu .= '<a class="dropdown-item" href="'.$link.'">Account</a>';
		}
		if (self::get('configNavSettings')) {
			$link = RouteHelper::getLink('Settings::settings');
			$menu .= '<a class="dropdown-item" href="'.$link.'">Settings</a>';
		}
		if (self::get('configNavDivider')) {
			$menu .= '<div class="dropdown-divider"></div>';
		}
		if (self::get('configNavLogout')) {
			$link = RouteHelper::getLink('Auth::logout');
			$menu .= '<a class="dropdown-item" href="'.$link.'">Logout</a>';
		}
		$title = self::get('configNavTitle');
		return
		<<<html
		    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
		      <div class="container-fluid">
		        <div class="navbar-wrapper">
		          <div class="navbar-minimize">
		            <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
		              <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
		              <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
		            </button>
		          </div>
		          <a class="navbar-brand" href="javascript:;">{$title}</a>
		        </div>
		        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
		          <span class="sr-only">Toggle navigation</span>
		          <span class="navbar-toggler-icon icon-bar"></span>
		          <span class="navbar-toggler-icon icon-bar"></span>
		          <span class="navbar-toggler-icon icon-bar"></span>
		        </button>
		        <div class="collapse navbar-collapse justify-content-end">
		          <ul class="navbar-nav">
		            <li class="nav-item dropdown">
		              <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                <i class="material-icons">person</i>
		                <p class="d-lg-none d-md-block">
		                  Account
		                </p>
		              </a>
		              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
		                {$menu}
		              </div>
		            </li>
		          </ul>
		        </div>
		      </div>
		    </nav>
		html;
	}

	public static function set($key, $value)
	{
		self::${$key} = $value;
	}

	public static function get($key)
	{
		return self::${$key};
	}

	public static function footer()
	{
		$devLabel = self::get('configDevLabel');
		$devUrl = self::get('configDevUrl');
		return
		<<<html
		    <footer class="footer">
		      <div class="container-fluid">
		        <div class="copyright float-right">
		          &copy;
		          <script>
		            document.write(new Date().getFullYear())
		          </script>, made with <i class="material-icons">favorite</i> by
		          <a href="{$devUrl}" target="_blank">{$devLabel}</a> for a better web.
		        </div>
		      </div>
		    </footer>
		html;
	}
}

?>