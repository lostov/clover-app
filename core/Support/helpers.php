<?php

namespace {
	/**
	 * Путь к общей корневой папке
	 *
	 * @param string|null $file
	 * @return mixed
	 */
	function public_path(string $file = null)
	{
	    if ($file) {
	        return dirname(dirname(dirname(__FILE__))).'/www'.'/'.$file;
	    }

	    return dirname(dirname(dirname(__FILE__))).'/www';
	}

	/**
	 * Путь к корневой папке, содержащей все приложение
	 *
	 * @param string|null $file
	 * @return mixed
	 */
	function base_path(string $file = null)
	{
	    if ($file) {
	        return dirname(dirname(dirname(__FILE__))).'/'.$file;
	    }

	    return dirname(dirname(dirname(__FILE__)));
	}

	/**
	 * Загрузка нужного класса для вызова
	 */
	function import(string $classlink = null)
	{
		if (!is_null($classlink)) {
			$link = str_replace('.', '/', $classlink) . '.php';
			if (file_exists($link)) {
				require_once($link);
			}
		}
	}

	DB::$dbName = "cu84363_app";
	DB::$user = "cu84363_app";
	DB::$password = "cu84363_app";
	DB::$host = "localhost";
	define('DS', DIRECTORY_SEPARATOR);
	define('__ROOT__', dirname(__DIR__, 2));
	define('__LIBS__', __ROOT__ . DS . 'libs');
	define('__SITE__', ' ');
	define('__SITE_BASE__', ' /');
}

namespace Core\Support {
	class Factory
	{
		private static $password_algo = PASSWORD_BCRYPT;
		private static $password_option = ['cost' => 20];

		public static function password(string $password)
		{
			return password_hash($password, PASSWORD_BCRYPT);
		}

		public static function passwordCheck(string $password, string $hash = null)
		{
			return password_verify($password, $hash);
		}

		public static function getPasswordAlgo() : int
		{
			return self::$password_algo;
		}

		public static function setPasswordAlgo($algo) : void
		{
			self::$password_algo = $algo;
		}

		public static function getPasswordOption() : array
		{
			return self::$password_option;
		}

		public static function setPasswordOption(array $option)
		{
			self::$password_option = $option;
		}

		public static function getSession()
		{
			return new \Core\Http\Session();
		}
	}
}
