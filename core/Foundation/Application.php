<?php

namespace Core\Foundation;

use Core\Contracts\Routing\RouterInterface;

/**
 * Для создания приложения
 */
class Application
{
    /**
     * @var Интерфейс маршрутизатора
     */
    private $router;

    /**
     *  Конструктор приложения.
     *
     * @param Router Interface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * Загрузите список маршрутов и запустите Routing
     */
    public function run()
    {
        $this->router->run();
    }
}
