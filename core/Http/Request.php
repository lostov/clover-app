<?php

namespace Core\Http;

/**
 * Request
 */
class Request
{
    /**
     * @param string $method - Метод передан в параметре
     * @return bool - True если метод запроса равен методу, переданному в качестве параметра
     */
    public static function isMethod(string $method): bool
    {
        return $this->getMethod() === strtoupper($method);
    }

    /**
     * @return string - Метод, используемый для доступа к странице. «GET», «POST», «PUT», «PATCH», «DELETE» ...
     */
    public static function getMethod()
    {
        $methodPost = strtoupper(Input::post('_method'));

        if (Input::hasPost('_method') && (in_array($methodPost, ['PUT', 'PATCH', 'DELETE']))) {
            return $methodPost;
        }

        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return string - URI, предоставленный для доступа к этой странице
     */
    public static function getRequestUri()
    {
        return $_SERVER['REQUEST_URI'];
    }
}
