<?php

namespace Core\Http;

/**
 * Input
 */
class Input
{
    /**
     * Проверьте, что данные, отправленные в REQUEST, существуют
     *
     *  @param string $name
     *  @return bool
     */
    public static function hasRequest(string $name): bool
    {
        return array_key_exists($name, $_REQUEST);
    }

    /**
     * Если данные отправлены в REQUEST, и если это $name существует -> вернуть $_REQUEST ['name']
     *
     *  @param string $name
     *  @return array|null
     */
    public static function request(string $name, $value = null, string $type = null)
    {
        $request = isset($_REQUEST[$name]) && $_REQUEST[$name] != '' ? $_REQUEST[$name] : $value;
        if ( $type != null) {
            switch (strtolower($type)) {
                case 'bool':
                    settype($request, 'bool');
                    break;
                case 'int':
                    settype($request, 'int');
                    break;
                case 'float':
                case 'double':
                    settype($request, 'double');
                    break;
                case 'string':
                    settype($request, 'string');
                    break;
                case 'array':
                    settype($request, 'array');
                    break;
                case 'object':
                    settype($request, 'object');
                    break;
                case 'null':
                default:
                    settype($request, 'null');
                    break;
            }
        }
        return $request;
    }

    /**
     * Проверьте, что данные, отправленные в POST, существуют
     *
     *  @param string $name
     *  @return bool
     */
    public static function hasPost(string $name): bool
    {
        return array_key_exists($name, $_POST);
    }

    /**
     * Если данные отправлены в POST, и если это $name существует -> вернуть $_POST ['name']
     *
     *  @param string $name
     *  @return array|null
     */
    public static function post(string $name, $value = null, string $type = null)
    {
        $post = isset($_POST[$name]) && $_POST[$name] != '' ? $_POST[$name] : $value;
        if ( $type != null) {
            switch (strtolower($type)) {
                case 'bool':
                    settype($post, 'bool');
                    break;
                case 'int':
                    settype($post, 'int');
                    break;
                case 'float':
                case 'double':
                    settype($post, 'double');
                    break;
                case 'string':
                    settype($post, 'string');
                    break;
                case 'array':
                    settype($post, 'array');
                    break;
                case 'object':
                    settype($post, 'object');
                    break;
                case 'null':
                default:
                    settype($post, 'null');
                    break;
            }
        }
        return $post;
    }

    /**
     * Проверить что данные отправленные в GET существуют
     *
     *  @param string $name
     *  @return bool
     */
    public static function hasGet(string $name): bool
    {
        return array_key_exists($name, $_GET);
    }

    /**
     * Если данные отправлены в GET, и если это $name существует -> вернуть $_GET['name']
     *
     *  @param string $name
     *  @return array|null - Данные отправленные в GET
     */
    public static function get(string $name, $value = null, string $type = null)
    {
        $get = isset($_GET[$name]) && $_GET[$name] != '' ? $_GET[$name] : $value;
        if ( $type != null) {
            switch (strtolower($type)) {
                case 'bool':
                    settype($get, 'bool');
                    break;
                case 'int':
                case 'integer':
                    settype($get, 'int');
                    break;
                case 'float':
                case 'double':
                    settype($get, 'double');
                    break;
                case 'string':
                    settype($get, 'string');
                    break;
                case 'array':
                    settype($get, 'array');
                    break;
                case 'object':
                    settype($get, 'object');
                    break;
                case 'null':
                default:
                    settype($get, 'null');
                    break;
            }
        }
        return $get;
    }

    /**
     * Проверить что данные отправленные в FILES существуют
     *
     *  @param string $name
     *  @return bool
     */
    public static function hasFile(string $name): bool
    {
        return array_key_exists($name, $_FILES);
    }

    /**
     * Если данные отправлены в FILES, и если это $name существует -> вернуть $_FILES['name']
     *
     *  @param string $name
     *  @return array|null - Данные отправленные в GET
     */
    public static function file(string $name, $value = null, string $type = null)
    {
        $file = isset($_FILES[$name]) && $_FILES[$name] != '' ? $_FILES[$name] : $value;
        if ( $type != null) {
            switch (strtolower($type)) {
                case 'bool':
                    settype($file, 'bool');
                    break;
                case 'int':
                case 'integer':
                    settype($file, 'int');
                    break;
                case 'float':
                case 'double':
                    settype($file, 'double');
                    break;
                case 'string':
                    settype($file, 'string');
                    break;
                case 'array':
                    settype($file, 'array');
                    break;
                case 'object':
                    settype($file, 'object');
                    break;
                case 'null':
                default:
                    settype($file, 'null');
                    break;
            }
        }
        return $file;
    }

    /**
     * Если данные отправлены в FILE_GET_CONTENTS, и если это $name существует -> вернуть FILE_GET_CONTENTS
     *
     *  @param string $type
     *  @return Данные отправленные в GET
     */
    public static function input(string $type = '0', $bool = true)
    {
        $input = file_get_contents('php://input', $bool);
        if ( $type != null) {
            switch (strtolower($type)) {
                case 'bool':
                    settype($input, 'bool');
                    break;
                case 'int':
                case 'integer':
                    settype($input, 'int');
                    break;
                case 'float':
                case 'double':
                    settype($input, 'double');
                    break;
                case 0:
                case '0':
                case 'string':
                    settype($input, 'string');
                    break;
                case 'array':
                    settype($input, 'array');
                    break;
                case 'object':
                    settype($input, 'object');
                    break;
                case 'null':
                default:
                    settype($input, 'null');
                    break;
            }
        }
        return $input;
    }



}
