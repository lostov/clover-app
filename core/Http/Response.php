<?php

namespace Core\Http;

use Core\Exception\ExceptionHandler;

/**
 * Response
 */
class Response
{
    /**
     * Коды ответа HTTP
     */
    const STATUS_CODE = [
        // Information 1xx
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',

        // Successful 2xx
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        210 => 'Content Different',
        226 => 'IM Used',

        // Redirection 3xx
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        310 => 'Too many Redirects',

        // Client error 4xx
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',

        418 => 'I\'m a teapot',
        421 => 'Bad mapping / Misdirected Request',
        422 => 'Unprocessable entity',
        423 => 'Locked',
        424 => 'Method failure',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        451 => 'Unavailable For Legal Reasons',
        456 => 'Unrecoverable Error',
        499 => 'Client has closed connection',

        // Server error 5xx
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient storage',
        508 => 'Loop detected',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not extended',
        511 => 'Network authentication required',
        520 => 'Web server is returning an unknown error',
    ];

    /**
     * Все загаловки
     */
    protected static $headers = [];

    /**
     * Укажите заголовок HTTP для отображения представления
     *
     * @param string $content
     * @param string|null $type
     */
    public static function header(string $content, string $type = null)
    {
        if ($type) {
            header($content.': '.$type.'; charset=UTF-8');
        } else {
            header($content);
        }
    }
    
    /**
     * Вернёт все заголовки текущего запроса 
     * 
     * @return array $headers
     */
    public static function getAllHeaders()
    {
        $copy_server = array(
            'CONTENT_TYPE'   => 'Content-Type',
            'CONTENT_LENGTH' => 'Content-Length',
            'CONTENT_MD5'    => 'Content-Md5',
        );

        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) === 'HTTP_') {
                $key = substr($key, 5);
                if (!isset($copy_server[$key]) || !isset($_SERVER[$key])) {
                    $key = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', $key))));
                    self::$headers[$key] = $value;
                }
            } elseif (isset($copy_server[$key])) {
                self::$headers[$copy_server[$key]] = $value;
            }
        }

        if (!isset(self::$headers['Authorization'])) {
            if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
                self::$headers['Authorization'] = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
            } elseif (isset($_SERVER['PHP_AUTH_USER'])) {
                $basic_pass = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';
                self::$headers['Authorization'] = 'Basic ' . base64_encode($_SERVER['PHP_AUTH_USER'] . ':' . $basic_pass);
            } elseif (isset($_SERVER['PHP_AUTH_DIGEST'])) {
                self::$headers['Authorization'] = $_SERVER['PHP_AUTH_DIGEST'];
            }
        }


        return self::$headers;
    }

    /**
     * Получить нужного загаловки по выбраному параметра 
     * 
     * @param string $name нозвание
     * @param different $value значение по умолчание 
     * @param different $type тип значение по умолчание
     * @return different $header выбранные заголовка 
     */
    public static function get(string $name, $value = null, $type = null)
    {
        if (count((array)self::$headers) == 0) {
            self::getAllHeaders();
        }
        $header = isset(self::$headers[$name]) && self::$headers[$name] != '' ? self::$headers[$name] : $value;
        if ( $type != null) {
            switch (strtolower($type)) {
                case 'bool':
                    settype($header, 'bool');
                    break;
                case 'int':
                    settype($header, 'int');
                    break;
                case 'float':
                case 'double':
                    settype($header, 'double');
                    break;
                case 'string':
                    settype($header, 'string');
                    break;
                case 'array':
                    settype($header, 'array');
                    break;
                case 'object':
                    settype($header, 'object');
                    break;
                case 0:
                    settype($header, 'null');
                    break;
                default:
                    settype($header, 'string');
                    break;
            }
        }
        return $header;
    }

    /**
     * Перенаправить
     *
     * @param string $url - URL-адрес, куда перенаправить посетителя
     * @param null|int $httpResponseCodeParam - Код ответа HTTP
     */
    public static function redirect(string $url, $httpResponseCodeParam = null)
    {
        if ($httpResponseCodeParam) {
            if (array_key_exists($httpResponseCodeParam, self::STATUS_CODE)) {
                $httpResponseCode = $httpResponseCodeParam;

                header('Location: '.$url, true, $httpResponseCode);
            } else {
                new ExceptionHandler('Status code "'.$httpResponseCodeParam.'" not good.');
                
                header('Location: '.$url);
            }
        } else {
            header('Location: '.$url);
        }
        
        exit();
    }
}
