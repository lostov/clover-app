<?php

namespace Core\Controller;

use Core\Http\Response;

/**
 * Родительский контроллер
 */
abstract class BaseController
{
    /**
     * Чтобы можно было использовать макет, отличный от view по умолчанию
     *
     * @var string
     */
    private $layout;
    private $json;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->layout = 'site';
        $this->layoutApi = 'api';
    }

    /**
     * Возможно использование view, отличного от используемого по умолчанию
     *
     * @param string $layout
     */
    final protected function setLayout(string $layout)
    {
        $this->layout = $layout;
    }
    /**
     * Возможно использование view, отличного от используемого по умолчанию
     *
     * @param string $layout
     */
    final protected function setApiLayout(string $layout)
    {
        $this->layoutApi = $layout;
    }

    /**
     * Обратный view
     *
     * @param string $view - Просмотреть View для загрузки
     * @param array $data - Чтобы переключить любые данные в View
     */
    final protected function view(string $view, array $data = [])
    {
        if ($data) extract($data);
        $require = base_path().'/app/views/'.$view.'.php';
        if (file_exists($require)) {
            ob_start();
            require $require;
            $contentInLayout = ob_get_clean();
        }
        require base_path().'/app/views/layouts/'.$this->layout.'.php';
        exit();
    }

    final protected function json(array $data = [])
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }

    final protected function getMedia(array $headers=[])
    {
        $file = base_path() . "/source/music.mp3";
        // header('Content-type: audio/mpeg');
        // header('Content-length: ' . filesize($file));
        // header('Content-Disposition: attachment; filename="music.mp3"');
        // header("Content-Transfer-Encoding: binary");
        // header("Content-Type: audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3");
        set_time_limit(0);
        $filePath = $file;
        $bitrate = 128;
        $strContext=stream_context_create( array( 'http'=>array( 'method'=>'GET', 'header'=>"Accept-language: en\r\n" ) ) );
        header('Content-type: audio/mpeg');
        header ("Content-Transfer-Encoding: binary");
        header ("Pragma: no-cache");
        header ("icy-br: " . $bitrate);
        $fpOrigin=fopen($filePath, 'rb', false, $strContext);
        while(!feof($fpOrigin)){
            $buffer=fread($fpOrigin, 4096);
            echo $buffer;
            flush();
        }
        fclose($fpOrigin);
        exit();
    }

    /**
     * Укажите заголовок HTTP для отображения представления
     *
     * @param string $content
     * @param string|null $type
     */
    final protected function header(string $content, string $type = null)
    {
        Response::header($content, $type);
    }
}
