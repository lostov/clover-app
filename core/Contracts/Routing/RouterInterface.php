<?php

namespace Core\Contracts\Routing;

interface RouterInterface
{
    /**
     * Singleton
     *
     * @return mixed
     */
    public static function getInstance();

    /**
     * Добавить маршрут
     *
     * @param string $path
     * @param string $action
     */
    public function add(string $path, string $action);

    /**
     * Выполнить маршрутизацию
     *
     * @return mixed
     */
    public function run();
}
